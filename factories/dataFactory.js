app.factory('DataFactory', function() {
    let listUsers	 		= [];
    let listBets			= [];
    let listPositions 		= [];
    let listTeams 			= [];
    let listPlayers			= [];
    let loggedUser			= [];
    let listStadiums		= [];
    let listMatches			= [];
    let listExtraBets		= [];
    let listAllExtraBets    = [];
    let listByeWeeks		= [];
    let onlineUsersCheck	= [];
    let userBets			= [];
    let matchEvents			= [];
    let listTips			= [];
    let liveMatch			= [];
    let listWeeklyRanking   = [];
    let listRanking			= [];
    let listScorers			= [];
    let listOwnGoals		= [];
    let verifyPassword  	= false;
    let existingEmail		= false;
    let googleId;
    let googleUser;
    let googleProfile;
    let clientIP;
    let season;
    let session = '';

	return {
		setUsers : function (data) {
			listUsers = data;
		},
		setBets : function (data) {
			listBets = data;
		},
		setExtraBets : function (data) {
			listExtraBets = data;
		},
        setAllExtraBets : function (data) {
            listAllExtraBets = data;
        },
		setPositions : function (data) {
			listPositions = data;
		},
		setTeams : function (data) {
			listTeams = data;
		},
		setPlayers : function (data) {
			listPlayers = data;
		},
		setStadiums : function (data) {
			listStadiums = data;
		},
		setMatches : function (data) {
			for (let i = 0; i < data.length; i++) {
				data[i].date = moment(data[i].date).format();
			}
			listMatches = data;
		},
		setByeWeeks : function (data) {
			listByeWeeks = data;
		},
		setLoggedUser : function (data) {
			loggedUser = data;
		},
		setUserBets : function (data) {
			userBets = data;
		},
		setClientIP : function (data) {
			clientIP = data;
		},
		setVerifyPassword : function (data) {
			verifyPassword = data;
		},
		setExistingEmail : function (data) {
			existingEmail = data;
		},
		setOnlineUsersCheck : function (data) {
			onlineUsersCheck = data;
		},
		setGoogleUser : function (data) {
			googleUser = data;
		},
		setGoogleProfile : function (data) {
			googleProfile = data;
		},
		setMatchEvents : function (data) {
			matchEvents = data;
		},
		setTips : function (data) {
			listTips = data;
		},
		setLiveMatch : function (data) {
			liveMatch = data;
		},
		setRanking : function (data) {
			listRanking = data;
		},
		setWeeklyRanking : function (data) {
				listWeeklyRanking = data;    
		},
		setScorers : function (data) {
			listScorers = data;
		},
		setOwnGoals : function (data) {
			listOwnGoals = data;
		},
		setSeason : function (data) {
			season = data;
		},
		setSession : function (data) {
			session = data;
		},
		getInfo : function (data) {
			switch (data) {
				case "users":
					return listUsers;
				case "bets":
					return listBets;
				case "extraBets":
					return listExtraBets;
                case "allExtraBets":
                    return listAllExtraBets;
				case "teams":
					return listTeams;
				case "positions":
					return listPositions;
				case "players":
					return listPlayers;
				case "stadiums":
					return listStadiums;
				case "matches":
					return listMatches;
				case "byeWeeks":
					return listByeWeeks;
				case "loggedUser":
					return loggedUser;
				case "userBets":
					return userBets;
				case "clientIP":
					return clientIP;
				case "verifyPassword":
					return verifyPassword;
				case "existingEmail":
					return existingEmail;
				case "onlineUsersCheck":
					return onlineUsersCheck;
				case "googleUser":
					return googleUser;
				case "googleProfile":
					return googleProfile;
				case "matchEvents":
					return matchEvents;
				case "tips":
					return listTips;
				case "liveMatch":
					return liveMatch;
				case "ranking":
					return listRanking;
				case "weeklyRanking":
                    return listWeeklyRanking;
				case "scorers":
					return listScorers;
				case "ownGoals":
					return listOwnGoals;
				case "season":
					return season;
				case "session":
					return session;
			}
		}
	};
});
