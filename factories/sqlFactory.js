app.factory('SQLFactory', function($q, $http, DataFactory, ToasterService, config) {
	
	return {
    loadTeams : function () {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/getTeams.php?",
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("loadTeams not loaded correctly. " + thrownError);
                deferred.reject("loadTeams not loaded correctly.");
            },
            success: function(dataReturn){
                DataFactory.setTeams(dataReturn);
                deferred.resolve();
            }                
        });
        return deferred.promise;
    },

    loadMatches : function (week) {
        var deferred = $q.defer();
        var season = DataFactory.getInfo("season");

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/getMatches.php?season="+season+"&week="+week,
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("loadMatches not loaded correctly. " + thrownError);
                deferred.reject("loadMatches not loaded correctly.");
            },
            success: function(dataReturn){
                DataFactory.setMatches(dataReturn);
                deferred.resolve();
            }                
        });
        return deferred.promise;
    },
    loadBets : function (type, week) {
        var deferred = $q.defer();        
        var season = DataFactory.getInfo("season");

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/getBets.php?season="+season+"&week="+week+"&type="+type,
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("loadBets not loaded correctly. " + thrownError);
                deferred.reject("loadBets not loaded correctly.");
            },
            success: function(dataReturn){
                if (type == "regular")
                    DataFactory.setBets(dataReturn);
                else (type == "extras")
                    DataFactory.setAllExtraBets(dataReturn);
                
                deferred.resolve();
            }                
        });
        return deferred.promise;
    },
    loadUserBets : function (type) {
        var deferred = $q.defer();
        var season = DataFactory.getInfo("season");

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/getUserBets.php?type="+type+"&season="+season,
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("User bets not loaded correctly. " + thrownError);
                deferred.reject("User bets not loaded correctly.");
            },
            success: function(dataReturn){
                if (type == "regular")
                    DataFactory.setUserBets(dataReturn);
                else if (type == "extra")
                    DataFactory.setExtraBets(dataReturn);
                deferred.resolve();
            }
        });
        return deferred.promise;
    },
    loadAllSeasonsRecords : function () {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/getAllSeasonsRanking.php",
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Ranking not loaded correctly. " + thrownError);
                deferred.reject("Ranking not loaded correctly.");
            },
            success: function(dataReturn){
                deferred.resolve(dataReturn);
            }
        });
        return deferred.promise;
    },
    loadSingleSeasonsRecords : function (season) {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/getSingleSeasonRanking.php?season="+season,
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Ranking not loaded correctly. " + thrownError);
                deferred.reject("Ranking not loaded correctly.");
            },
            success: function(dataReturn){
                deferred.resolve(dataReturn);
            }
        });
        return deferred.promise;
    },
    loadRanking : function (week, season) {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/getRanking.php?week="+week+"&season="+season,
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Ranking not loaded correctly. " + thrownError);
                deferred.reject("Ranking not loaded correctly.");
            },
            success: function(dataReturn){
                if (season === config.season) {
                    if (week != 0) {
                        DataFactory.setWeeklyRanking(dataReturn);
                    } else {
                        DataFactory.setRanking(dataReturn);
                    }    
                }
                deferred.resolve(dataReturn);
            }
        });
        return deferred.promise;
    },
    loadUser : function () {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/getUser.php",
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("User not loaded correctly. " + thrownError);
                deferred.reject("User not loaded correctly.");
            },
            success: function(dataReturn){
                DataFactory.setLoggedUser(dataReturn);
                deferred.resolve();
            }
        });
        return deferred.promise;
    },
    loadByeWeeks : function (season) {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/getByeWeeks.php?season="+season,
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Bye weeks not loaded correctly. " + thrownError);
                deferred.reject("Bye weeks not loaded correctly.");
            },
            success: function(dataReturn){
                DataFactory.setByeWeeks(dataReturn);
                deferred.resolve();
            }
        });
        return deferred.promise;
    },
    manualLogin : function (login, pass, season) {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/login.php?type=manual&login="+login+"&pass="+pass+"&season="+season,
            dataType: "json", 
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Could not verify credentials on manual login. " + thrownError);
                deferred.reject("Could not verify credentials on manual login.");
            },
            success: function(dataReturn){
                if (dataReturn.length > 0) {
                    DataFactory.setLoggedUser(dataReturn);

                    clientLanguage = DataFactory.getInfo("clientLanguage");
                    ToasterService.printToast('success', 10);
                } else {
                    ToasterService.printToast('error', 11);
                }
                deferred.resolve();
            }
        });
        return deferred.promise;
    },
    cookiesLogin : function (cookie, id_user, season) {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/login.php?type=cookies&cookie="+cookie+"&id_user="+id_user+"&season="+season,
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Could not verify credentials on cookies login. " + thrownError);
                deferred.reject("Could not verify credentials on cookies login.");
            },
            success: function(dataReturn){
                const parsedDataReturn = JSON.parse(dataReturn);
                if (dataReturn.length > 0) {
                    DataFactory.setLoggedUser(parsedDataReturn);
                    clientLanguage = DataFactory.getInfo("clientLanguage");
                    ToasterService.printToast('success', 10);
                }
                deferred.resolve();
            }
        });
        return deferred.promise;
    },
    saveCookies : function (table, cookie) {
        var deferred = $q.defer();

        $.ajax({
            type: "POST",
            cache: false,
            url: "sql/updateLogin.php?type=cookies&cookie="+cookie,
//				dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Could not update cookies to user credentials. " + thrownError);
                deferred.reject("Could not update cookies to user credentials.");
            },
            success: function(dataReturn){
                deferred.resolve();
            }
        });
        return deferred.promise;			
    },
    deleteCookies : function (cookie) {
        var deferred = $q.defer();

        $.ajax({
            type: "POST",
            cache: false,
            url: "sql/delete.php?type=cookies&cookie="+cookie,
//				dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Could not delete cookies from user credentials. " + thrownError);
                deferred.reject("Could not delete cookies from user credentials.");
            },				
            success: function(dataReturn){
                deferred.resolve();					
            }
        });
        return deferred.promise;
    },
    destroySession : function () {

        $.ajax({
            type: "POST",
            cache: false,
            url: "sql/destroySession.php",
            success: function(dataReturn){
            }
        });
    },
    saveBets : function (type, id_match, id_bet) {
        var deferred = $q.defer();
        const season = DataFactory.getInfo("season");

        $.ajax({
            type: "POST",
            cache: false,
            url: "sql/saveBets.php?type="+type+"&id_match="+id_match+"&id_bet="+id_bet+"&season="+season,
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Could not save bets. " + thrownError);
                deferred.reject("Could not save bets.");
            },
            success: function(dataReturn){
                if (dataReturn.status == "success")
                    deferred.resolve();
                else 
                    deferred.reject(dataReturn.code);
            }
        });
        return deferred.promise;
    },
    verifyEmail : function (email) {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/verifyUser.php?type=email&email="+email,
            dataType: "json",              
            success: function(dataReturn){
                if (dataReturn.length > 0) {
                    deferred.reject(903);
                } else {
                    deferred.resolve();
                }
            }
        }).
        error(function(data, status, headers, config) {
            console.log("E-mail not verified.");
            deferred.reject("E-mail not verified.");
        });
        return deferred.promise;

    },
    verifyName : function (name) {
        var deferred = $q.defer();

        $.ajax({
            type: "GET",
            cache: false,
            url: "sql/verifyUser.php?type=name&name="+name,
            dataType: "json",              
            success: function(dataReturn){
                if (dataReturn.length > 0) {
                    deferred.reject(904);
                } else {
                    deferred.resolve();
                }
            }
        }).
        error(function(data, status, headers, config) {
            console.log("Name not verified.");
            deferred.reject("Name not verified.");
        });
        return deferred.promise;

    },
    saveUsers : function (email, password, user, season) {
        var deferred = $q.defer();
        
        const color = encodeURIComponent("#000000");
        const icon = "fas fa-dot-circle";

        $.ajax({
            type: "POST",
            cache: false,
            url: "sql/saveUsers.php?email="+email+"&password="+password+"&user="+user+"&season="+season+"&color="+color+"&icon="+icon,
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Could not save user. " + thrownError);
                deferred.reject("Could not save user.");
            },
            success: function(dataReturn){
                deferred.resolve();
            }
        });
        return deferred.promise;
    },
    updateUser : function (fullname, name, login, newpass, pass) {
        var deferred = $q.defer();

        $.ajax({
            type: "POST",
            cache: false,
            url: "sql/updateUser.php?type=account&fullname="+fullname+"&name="+name+"&login="+login+"&newpass="+newpass+"&pass="+pass,
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("User not updated correctly. " + thrownError);
                deferred.reject(911);
            },
            success: function(dataReturn){
                if (dataReturn == 0)
                    deferred.reject(905);
                else 
                    deferred.resolve();
            }
        });
        return deferred.promise;
    },
    updateUserProfile : function (icon, color) {
        var deferred = $q.defer();
        $.ajax({
            type: "POST",
            cache: false,
            url: "sql/updateUser.php?type=profile&icon="+icon+"&color="+encodeURIComponent(color),
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("User not updated correctly. " + thrownError);
                deferred.reject(911);
            },
            success: function(dataReturn){
                deferred.resolve();
            }
        });
        return deferred.promise;

    },
    updateOnlineTimestamp : function () {
        var deferred = $q.defer();
        $.ajax({
            type: "POST",
            cache: false,
            url: "sql/updateUser.php?type=online",
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("User not updated correctly. " + thrownError);
                deferred.reject(911);
            },
            success: function(dataReturn){
                deferred.resolve();
            }
        })
        return deferred.promise;
	}
    };
});
