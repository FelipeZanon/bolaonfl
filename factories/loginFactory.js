app.factory('LoginFactory', function($q, UtilitiesService, DataFactory, SQLFactory) {
	
	function saveCookiesToBrowser(cookieNumber, id_user, expirationDays)
	{
		const expireDate = new Date();
		expireDate.setTime(expireDate.getTime() + (expirationDays*24*60*60*1000));
		const expires = "expires="+ expireDate.toUTCString();
		document.cookie = "bolaoCopa2018" + "=" + cookieNumber + "-" + id_user + "; " + expires;
	};

	return {
        manualLogin : function (login, pass)
		{
			var deferred = $q.defer();
            const season = DataFactory.getInfo("season");

			SQLFactory.manualLogin(login, pass, season).then(function(){
                UtilitiesService.renewCookies();
				deferred.resolve();
			});
			return deferred.promise;
		},
		cookiesLogin : function ()
		{
			var deferred = $q.defer();
            const season = DataFactory.getInfo("season");

            const bolaoCookies = UtilitiesService.returnBrowserCookies();
			
			if (bolaoCookies) {
				allCookies = bolaoCookies.split('-');
				const singleCookie = allCookies[0].trim().substring(13);
				const id_user = allCookies[1].trim();
                
				SQLFactory.cookiesLogin(singleCookie, id_user, season).then(function(){
					loggedUser = DataFactory.getInfo("loggedUser");
					if (loggedUser.length > 0) {
						SQLFactory.deleteCookies(singleCookie, id_user);
					}
	
					deferred.resolve();
				});
			}
			return deferred.promise;
		},
		signup : function (email, password, user) {
			var deferred = $q.defer();
            
            SQLFactory.verifyEmail(email).then(function(response){
                //Verified that e-mail is not already on DB
                SQLFactory.verifyName(user).then(function(response){
                    const season = DataFactory.getInfo("season");
                    SQLFactory.saveUsers(email, password, user, season).then(function(){                    
                        deferred.resolve();
                    }, function (error) {
                        deferred.reject(error);
                    });
                }, function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
           
            }, function(error) {
                //Problem with e-mail verification (probably e-mail already exists in DB)
                console.log(error);
                deferred.reject(error);
            });
			return deferred.promise;
		},
		updateUser : function (fullname, name, login, newpass, pass) {
			var deferred = $q.defer();
            const loggedUser = DataFactory.getInfo("loggedUser");
            
            if (login && login != loggedUser[0].login) {
                SQLFactory.verifyEmail(login).then(function(response){
                    if (name && name != loggedUser[0].name) {
                        SQLFactory.verifyName(name).then(function(response){
                            SQLFactory.updateUser(fullname, name, login, newpass, pass).then(function(){
                                deferred.resolve();
                            }, function(error) {
                                console.log(error);
                                deferred.reject(error);
                            });
                            
                        }, function(error) {
                            console.log(error);
                            deferred.reject(error);
                        });
                    } else {
                        SQLFactory.updateUser(fullname, name, login, newpass, pass).then(function(){
                            deferred.resolve();
                        }, function(error) {
                            console.log(error);
                            deferred.reject(error);
                        });
                    }                 
                }, function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            } else if (name && name != loggedUser[0].name) {
                SQLFactory.verifyName(name).then(function(response){
                    SQLFactory.updateUser(fullname, name, login, newpass, pass).then(function(){
                        deferred.resolve();
                    }, function(error) {
                        console.log(error);
                        deferred.reject(error);
                    });
                }, function(error) {
                    console.log(error);
                    deferred.reject(error);
                });

            } else {
                SQLFactory.updateUser(fullname, name, login, newpass, pass).then(function(){
                    deferred.resolve();
                }, function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            }
            return deferred.promise;
		},
        updateUserProfile : function (icon, color) {
			var deferred = $q.defer();
            SQLFactory.updateUserProfile(icon, color).then(function(response){
                deferred.resolve();
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            })
            return deferred.promise;
        }
	};
});