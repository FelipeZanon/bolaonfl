var app = angular.module('bolaoApp', ['ngRoute']);

app.config(function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
	$routeProvider
		.when('/tabela/', {
		templateUrl: 'views/table.php',
		controller: 'tableController'
	   })
		.when('/apostar/', {
		templateUrl: 'views/bets.php',
		controller: 'betsController'
	})
		.when('/apostarExtra/', {
		templateUrl: 'views/extraBets.php',
		controller: 'betsController'
	})
		.when('/regras/', {
		templateUrl: 'views/rules.php',
		controller: 'rulesController'
	})
		.when('/ranking/', {
		templateUrl: 'views/ranking.php',
		controller: 'tableController'
	})
		.when('/records/', {
		templateUrl: 'views/records.php',
		controller: 'recordsController',
	})
		.when('/records/:year', {
		templateUrl: 'views/table.php',
		controller: 'tableController',
//        resolve: {
//            // I will cause a 1 second delay
//            delay: function($q, $timeout) {
//            var delay = $q.defer();
//            $timeout(delay.resolve, 1000);
//            return delay.promise;
//            }
//        }

	})
		.when('/estatisticas/', {
		templateUrl: 'views/stats.php',
		controller: 'statsController'
	})
	.otherwise({ redirectTo: '/tabela/' });
});


app.controller('mainController', function($scope, $q, $http, $location, $filter, $window, $interval, $rootScope, $route, $routeParams, $location,
                                           DataFactory, SQLFactory, LoginFactory, //Factories
                                           GetInfoService, SaveInfoService, LoginService, //Services
                                           config, sidebarButtons, seasons, weeks, icons, iconColors) { //Constants provider
	if ($location.path().indexOf("tabela") != -1) {
        $scope.activeController = 'table';
    } else if ($location.path().indexOf("apostarExtra") != -1) {
        $scope.activeController = 'extraBets';
    } else if ($location.path().indexOf("apostar") != -1) {
        $scope.activeController = 'bets';
    } else if ($location.path().indexOf("regras") != -1) {
        $scope.activeController = 'rules';
    } else if ($location.path().indexOf("teams") != -1) {
        $scope.activeController = 'teams';
    } else if ($location.path().indexOf("estatisticas") != -1) {
        $scope.activeController = 'stats';
    } else if ($location.path().indexOf("records") != -1) {
        $scope.activeController = 'records';
    }

	//else if ($location.path().indexOf("cadastro") != -1)
	//	$scope.activeController = 'table';
    
    $scope.$on('$routeChangeSuccess', function() {
        if ($location.path().indexOf("tabela") != -1) {
            $scope.activeMenu = 'tabela';
        } else if ($location.path().indexOf("apostarExtra") != -1) {
            $scope.activeMenu = 'apostarExtra';
        } else if ($location.path().indexOf("apostar") != -1) {
            $scope.activeMenu = 'apostar';
        } else if ($location.path().indexOf("regras") != -1) {
            $scope.activeMenu = 'regras';
        } else if ($location.path().indexOf("teams") != -1) {
            $scope.activeMenu = 'teams';
        } else if ($location.path().indexOf("estatisticas") != -1) {
            $scope.activeMenu = 'estatisticas';
        } else if ($location.path().indexOf("ranking") != -1) {
            $scope.activeMenu = 'ranking';
        } else if ($location.path().indexOf("records") != -1) {
            $scope.activeMenu = 'records';
        }
    
        if ($location.path().indexOf("records") != -1 && $routeParams) {
            //set new season = $routeParams
            //broadcast season change so all data can be gathered again
            
        }
//        $scope.$route = $route;
//        $scope.$location = $location.path();
//        $scope.$routeParams = $routeParams;
    });

    
    $scope.loggedUser = [];
    $scope.listMatches = [];
    $scope.listBets = [];
    $scope.googleUser = [];
    $scope.googleProfile = [];
    $scope.listPositions = [];
    $scope.listTeams = [];
    $scope.listStadiums = [];
    $scope.listPlayers = [];
    $scope.listExtraBets = [];
    $scope.listRanking = [];

    $scope.openedSidebar = true;
    $scope.rankingType = "general";
    $scope.preferencesTab = 1;
    $scope.iconsLimit = 80;
    $scope.iconsStart = 0;
    $scope.iconsCurrentPage = 1;
    $scope.loginTab = 1;
    $scope.chosenColor = "#000";    
    
	$(document).ready(function(){
        $scope.season = config.season;
        $scope.season_start = config.season_start;
        $scope.sidebarButtons = sidebarButtons;
        $scope.weeks = weeks;
        $scope.seasons = seasons;
        $scope.icons = icons;
        $scope.iconsPages = Math.ceil($scope.icons.length/$scope.iconsLimit);                
        $scope.iconColors = iconColors;
        
        $scope.loadingLogin = 1;
        $scope.loadingRanking = 1;
        $scope.loadingMatches = 1;

        DataFactory.setSeason($scope.season);
        $(this).scrollTop(0);
        updateNow();
        updateRound();
        $('#modalLoginMobile').modal();
        $('#modalPreferencesMobile').modal();
        $('#modalSignup').modal();
        $('#modalPreferences').modal();        
        $('.sidenav').sidenav();
        GetInfoService.byeWeeks().then(function() {
            $scope.byeWeeks = DataFactory.getInfo("byeWeeks");
        });
        GetInfoService.teams();
        GetInfoService.matches(0).then(function() {
            GetInfoService.bets("regular", 0);
        });
        
        $scope.loadingRanking = true;
        GetInfoService.ranking(0);
        GetInfoService.ranking($scope.selectedRound);
	});
    
    //1sec
	$interval(updateNow, 1000, 0, true);
	//2min
	$interval(updateOnlineTimestamp, 120000, 0, true);
    //1min
	$interval(getBets, 60000, 0, true);
    //30sec
	$interval(getResults, 30000, 0, true);

    function updateNow () {
        $scope.now = moment().format();
        $scope.nowTimestamp = moment().format("X");
        if (moment().format("hh") == "00" && 
            moment().format("mm") == "01" && 
            moment().weekday() == "3") //00h01 of a wednesday
            updateRound();
    }

    function updateRound () {
        for (let i = 0; i < $scope.weeks.length; i++) {
            if ($scope.nowTimestamp < $scope.weeks[i].timestamp) {
                $scope.selectedRound = $scope.weeks[i].number; //Depends on time + tabs
                $scope.currentRound = $scope.weeks[i].number; //Dependes only on time - doesn't change
                return;
            }
            //If no week was selected, season is over
            $scope.selectedRound = 1;
            $scope.currentRound = 1;
        }
    }

    function updateOnlineTimestamp () {
        SaveInfoService.onlineTimestamp();
    }
    function getResults () {
        if ($scope.activeController == "table") {
            GetInfoService.matches($scope.currentRound).then(function() {
                insertBetsOnMatches($scope.currentRound);
            });
        }
        GetInfoService.ranking(0);
        GetInfoService.ranking($scope.selectedRound);            
    }
    
    function getBets () {
        if (moment().format("mm") % 5 == 0) {
            GetInfoService.bets("regular", $scope.currentRound);
        }
    }

    $scope.expandMenu = function (button) {
        if (button == "Menu") {
            $scope.openedSidebar = !$scope.openedSidebar;
        } else if (button == "Login") {
            $scope.openedSidebar = true;
        } else if (button == "Tabela") {
            getResults();
        }
    }
    
    $scope.cleanForm = function () {
        $scope.emailSignup = "";
        $scope.passwordSignup = "";
        $scope.userSignup = "";
    }
    
    $scope.signup = function () {
        LoginService.signup($scope.emailSignup, $scope.passwordSignup, $scope.userSignup);
    }
    
    $scope.manualLogin = function () {
        LoginService.manualLogin($scope.emailLogin, $scope.passwordLogin);
    }
    
    $scope.logout = function () {
        LoginService.logout();
    }
    
    $scope.updateUserAccount = function () {
        LoginService.updateAccount($scope.newFullname, $scope.newName, $scope.newLogin, $scope.newPassword, $scope.passwordConfirm);
    }
    
    $scope.updateUserProfile = function (icon, color) {
        LoginService.updateProfile(icon, color);
    }
   
    $scope.$on("teamsSet", function () {
        $scope.listTeams = DataFactory.getInfo("teams");
    }); 
    $scope.$on("matchesSet", function () {
        $scope.lastGetResults = $scope.nowTimestamp;
        
        if ($scope.listMatches.length == 0) {
            $scope.listMatches = DataFactory.getInfo("matches");
        } else {
            const newResults = DataFactory.getInfo("matches");
            for (let i = 0; i < $scope.listMatches.length; i++) {
                if ($scope.listMatches[i].week == $scope.currentRound) {
                    for (let j = 0; j < newResults.length; j++) {
                        if ($scope.listMatches[i].id == newResults[j].id) {
                            $scope.listMatches[i] = newResults[j];
                        }
                    }                    
                }
            }
        }
        
        //Login being called after matchesSet to avoid assync problems. UserBets was being called before matches were set.
        //Inside LoginService is being verified if there's already a logged user.        
        LoginService.cookiesLogin(); 
    });
    $scope.$on("betsSet", function () {
        $scope.listBets = DataFactory.getInfo("bets");
        insertBetsOnMatches(0);
        $scope.loadingMatches = 0;
    });
    $scope.$on("userExtraBetsSet", function () {
        $scope.listExtraBets = DataFactory.getInfo("extraBets");
    });

    $scope.$on("rankingSet", function () {
        $scope.listRanking = DataFactory.getInfo("ranking");
        $scope.listWeeklyRanking = DataFactory.getInfo("weeklyRanking");
        $scope.loadingRanking = false;
    });
    
    $scope.$on("userLoggedIn", function () {
        SaveInfoService.onlineTimestamp();
        $scope.loggedUser = DataFactory.getInfo("loggedUser");
        $scope.emailLogin = "";
        $scope.passwordLogin = "";
    });
    $scope.$on("userLoggedOut", function () {
        $scope.loggedUser = DataFactory.getInfo("loggedUser");
    });
    $scope.$on("userSignup", function () {
        $scope.loadingRanking = true;
        GetInfoService.ranking(0);
        GetInfoService.ranking($scope.selectedRound);
        $('#modalSignup').modal('close');
        $('#modalLoginMobile').modal('close');
    });    
    $scope.$on("userUpdate", function () {
        GetInfoService.user().then(function(){
            
            $scope.loggedUser = DataFactory.getInfo("loggedUser");

            $scope.newPass = null;
            $scope.passwordConfirm = null;

            $scope.editFullname = false;
            $scope.editName = false;
            $scope.editLogin = false;
            $scope.editPassword = false;
            GetInfoService.ranking(0);
            GetInfoService.ranking($scope.selectedRound);
            
            if ($scope.preferencesTab == 1)
                $('#modalPreferences').modal('close');
        }); 
    });

    
    function insertBetsOnMatches (week) {
        for (let i = 0; i < $scope.listMatches.length; i++) {
            $scope.listMatches[i].bets = [];
            for (let j = 0; j < $scope.listBets.length; j++) {
                if (week == 0 || $scope.listMatches[i].week == week) {
                    if ($scope.listMatches[i].id == $scope.listBets[j].id_match) {
                        $scope.listMatches[i].bets.push($scope.listBets[j]);
                    }
                }
            }
        }
    }
    
    $scope.isExtraBet = function (id, conference, division) {
        conference = conference.toLowerCase();
        if (division != undefined) {
            division = "_" + division.toLowerCase();
        } else {
            division = '';
        }
        
        for (let i = 0; i < $scope.listExtraBets.length; i++) {
            if ($scope.listExtraBets[i].id_team == id && $scope.listExtraBets[i].description == conference+division) {
                return true;
            }
        }
        return false;
    }
    
    $scope.selectRound = function (round) {
        if (round >= 0 && round <= 21) {
            $scope.selectedRound = round;
            
            if ($scope.activeController != "records")
                GetInfoService.ranking($scope.selectedRound);
        }
    }
    
    $scope.colorCode = function (match) {
        
        const home_points = match.home_points;
        const away_points = match.away_points;
        let userBet = null;
        
        if (match.bets && $scope.loggedUser.length > 0) {
            for (let i = 0; i < match.bets.length; i++) {
                if (match.bets[i].id_match == match.id && match.bets[i].id_user == $scope.loggedUser[0].id) {
                    userBet = match.bets[i].id_bet;
                }
            }
        }
        
        if (userBet != null) {
            if (userBet == 0) {
                if (away_points - home_points > 7)
                    return "green-text";
                else if (away_points - home_points > 0)
                    return "blue-text";
            } else if (userBet == 1) {
                if (away_points - home_points > 7 || away_points - home_points == 0)
                    return "blue-text";
                else if (away_points - home_points > 0)
                    return "green-text";
            } else if (userBet == 2) {
                if (home_points - away_points > 7 || away_points - home_points == 0)
                    return "blue-text";
                else if (home_points - away_points > 0)
                    return "green-text";
            } else if (userBet == 3) {
                if (home_points - away_points > 7)
                    return "green-text";
                else if (home_points - away_points > 0)
                    return "blue-text";
            }
        }
        
        if (match.status.toLowerCase() == "final" || match.status.toLowerCase() == "final/ot")
            return "grey-text";
        else return "black-text";
    }
    
    $scope.Math = window.Math;
    
    $scope.isNaN = function (value) {
		return isNaN(value);
	};
});