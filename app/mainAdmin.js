var app = angular.module('bolaoAdminApp', []);

app.controller('adminController', function($scope, $q, $http, $location, $window, DataFactory, SQLFactory, LoginFactory) {
	$(document ).ready(function(){
		$(".button-collapse").sideNav();
		LoginFactory.loginByCookies().then(function(){
			$scope.loggedUser = DataFactory.getInfo("loggedUser");
			if ($scope.loggedUser.length > 0)
				LoginFactory.renewCookies();
		});
		refreshData();
	});
		
		
	$scope.listTeamsInsert    = [{prefix:'', name:''}];
	$scope.listPlayersInsert  = [{name:'', number:'', position:'', team:''}];
	$scope.listStadiumsInsert = [{name:'', city:'', capacity:'', geo_latitude:'', geo_longitude:''}];
	$scope.listMatchesInsert  = [{date:'', home_team:'', away_team:'', stadium:''}];
	
	function refreshData () {
		SQLFactory.loadInfo("positions").then(function(){
			$scope.listPositions = DataFactory.getInfo("positions");
		});
		SQLFactory.loadInfo("teams").then(function(){
			$scope.listTeams = DataFactory.getInfo("teams");
		});
		SQLFactory.loadInfo("stadiums").then(function(){
			$scope.listStadiums = DataFactory.getInfo("stadiums");
		});
		SQLFactory.loadInfo("players").then(function(){
			$scope.listPlayers = DataFactory.getInfo("players");
		});	
		SQLFactory.loadInfo("matches").then(function(){
			$scope.listMatches = DataFactory.getInfo("matches");
		});	
	}
	
	$scope.updateInternalPage = function (data) {
		$('#tooltippedAdd').tooltip({delay: 50});
		$('#tooltippedSave').tooltip({delay: 50});
		$('#tooltippedRemove').tooltip({delay: 50});

		$scope.internalAdminMenu = data;
	
		if (data == "players" || data == "matches") {
			setTimeout(updateSelects, 50);
	
			SQLFactory.loadInfo("teams").then(function(){
				$scope.listTeams = DataFactory.getInfo("teams");
			});	
		}
	};
	
	$scope.deleteInfo = function () {
		var i;
		var deleteArray = [];
			if ($scope.internalAdminMenu == "teams") {
				for (i = 0; i < $scope.listTeams.length; i++) {
					if ($scope.listTeams[i].remove)
						deleteArray.push($scope.listTeams[i]);
				}
			} else if ($scope.internalAdminMenu == "players") {
				for (i = 0; i < $scope.listPlayers.length; i++) {
					if ($scope.listPlayers[i].remove)
						deleteArray.push($scope.listPlayers[i]);
				}
			} else if ($scope.internalAdminMenu == "stadiums") {
				for (i = 0; i < $scope.listStadiums.length; i++) {
					if ($scope.listStadiums[i].remove)
						deleteArray.push($scope.listStadiums[i]);
				}
			} else if ($scope.internalAdminMenu == "matches") {
				for (i = 0; i < $scope.listMatches.length; i++) {
					if ($scope.listMatches[i].remove)
						deleteArray.push($scope.listMatches[i]);
				}
			}

			console.log("oi");

			SQLFactory.deleteInfo($scope.internalAdminMenu, deleteArray).then(function() {
				console.log("Deletando...");
				refreshData();
			});

	};

	$scope.insertInfo = function () {
		var arrayLength, i;
		if ($scope.internalAdminMenu == "teams") {
			arrayLength = $scope.listTeamsInsert.length;
			for (i = 0; i < arrayLength; i++) {
				if ($scope.listTeamsInsert[i].prefix == "" || $scope.listTeamsInsert[i].name == "") {
					if ($scope.listTeamsInsert[i].prefix == "" && $scope.listTeamsInsert[i].name == "") {
						$scope.listTeamsInsert.splice(i, 1);
						
						i--;
						arrayLength--;
						
						if ($scope.listTeamsInsert.length == 0)
							return;				 
					} else {
						Materialize.toast('Um ou mais campos estão vazios!', 4000);
					}
				}
			}
			SQLFactory.saveInfo($scope.internalAdminMenu, $scope.listTeamsInsert).then(function() {
				console.log("Salvando...");
				$scope.listTeamsInsert = [{prefix:'', name:''}];
			});
		} else if ($scope.internalAdminMenu == "players") {
			arrayLength = $scope.listPlayersInsert.length;
			for (i = 0; i < arrayLength; i++) {
				if ($scope.listPlayersInsert[i].name == "" || $scope.listPlayersInsert[i].number == "" ||
				$scope.listPlayersInsert[i].id_position == "" || $scope.listPlayersInsert[i].id_team == "") {
					if ($scope.listPlayersInsert[i].name == "" && $scope.listPlayersInsert[i].number == "" &&
					!$scope.listPlayersInsert[i].id_position && !$scope.listPlayersInsert[i].id_team) {
						$scope.listPlayersInsert.splice(i, 1);
					
						i--;
						arrayLength--;
						
						if ($scope.listPlayersInsert.length == 0)
							return;
						
					} else {
						Materialize.toast('Um ou mais campos estão vazios!', 4000);
					}
				}
			}
			SQLFactory.saveInfo($scope.internalAdminMenu, $scope.listPlayersInsert).then(function() {
				console.log("Salvando...");
				$scope.listPlayersInsert = [{name:'', number:'', position:'', team:''}];
			});
		} else if ($scope.internalAdminMenu == "stadiums") {
			arrayLength = $scope.listStadiumsInsert.length;
			for (i = 0; i < arrayLength; i++) {
				if ($scope.listStadiumsInsert[i].name == "" || $scope.listStadiumsInsert[i].city == "" ||
				$scope.listStadiumsInsert[i].capacity == "" || $scope.listStadiumsInsert[i].geo_latitude == "" ||
				$scope.listStadiumsInsert[i].geo_longitude == "") {
					if ($scope.listStadiumsInsert[i].name == "" && $scope.listStadiumsInsert[i].city == "" &&
					!$scope.listStadiumsInsert[i].capacity && $scope.listStadiumsInsert[i].geo_latitude == "" &&
					$scope.listStadiumsInsert[i].geo_longitude == "") {
						$scope.listStadiumsInsert.splice(i, 1);
						
						i--;
						arrayLength--;
						
						if ($scope.listStadiumsInsert.length == 0)
							return;						
					} else {
						Materialize.toast('Um ou mais campos estão vazios!', 4000);
					}
				}
			}
			SQLFactory.saveInfo($scope.internalAdminMenu, $scope.listStadiumsInsert).then(function() {
				console.log("Salvando...");
				$scope.listStadiumsInsert = [{name:'', city:'', capacity:'', geo_latitude:'', geo_longitude:''}];
			});
		} else if ($scope.internalAdminMenu == "matches") {
			arrayLength = $scope.listMatchesInsert.length;
			for (i = 0; i < arrayLength; i++) {
					if ($scope.listMatchesInsert[i].date == "" || !$scope.listMatchesInsert[i].id_home ||
					!$scope.listMatchesInsert[i].id_visitor || !$scope.listMatchesInsert[i].id_stadium) {
						if ($scope.listMatchesInsert[i].date == "" && !$scope.listMatchesInsert[i].id_home &&
						!$scope.listMatchesInsert[i].id_visitor && !$scope.listMatchesInsert[i].id_stadium) {
							$scope.listMatchesInsert.splice(i, 1);
							
							i--;
							arrayLength--;
							
							if ($scope.listMatchesInsert.length == 0)
								return;
						} else
							Materialize.toast('Um ou mais campos estão vazios!', 4000);
					} else {
						$scope.listMatchesInsert[i].date = moment($scope.listMatchesInsert[i].date).format('YYYY-MM-DD');
						$scope.listMatchesInsert[i].date = $scope.listMatchesInsert[i].date + ' ' + $scope.listMatchesInsert[i].time + ':00';
					}
			}
			
			if ($scope.listMatchesInsert.length > 0 ) {
				SQLFactory.saveInfo($scope.internalAdminMenu, $scope.listMatchesInsert).then(function() {
					console.log("Salvando...");
					$scope.listMatchesInsert.splice(0, $scope.listMatchesInsert.length);
					$scope.addLine();
				});
			}
		}
	};

	$scope.addLine = function () {
		switch ($scope.internalAdminMenu) {
		case "teams":
			$scope.listTeamsInsert.push({prefix:"", name:""});
			break;
		case "players":
			$scope.listPlayersInsert.push({name:'', number:'', position:'', team:''});
			setTimeout(updateSelects, 50);
			break;
		case "stadiums":
			$scope.listStadiumsInsert.push({name:'', city:'', capacity:'', geo_latitude:'', geo_longitude:''});
			break;
		case "matches":
			$scope.listMatchesInsert.push({date:'', home_team:'', away_team:'', stadium:''});
			setTimeout(updateSelects, 50);
			break;			
		}
	};

	$scope.clearLines = function () {
		switch ($scope.internalAdminMenu) {
		case "teams":
			$scope.listTeamsInsert = [{prefix:'', name:''}];
			break;
		case "players":
			$scope.listPlayersInsert = [{name:'', number:'', position:'', team:''}];
			setTimeout(updateSelects, 50);
			break;
		case "stadiums":
			$scope.listStadiumsInsert  = [{name:'', city:'', capacity:'', geo_latitude:'', geo_longitude:''}];
			break;
		case "matches":
			$scope.listMatchesInsert = [{date:'', home_team:'', away_team:'', stadium:''}];
			setTimeout(updateSelects, 50);
			break;
		}
	};
	
	$scope.updateSelectsTimeout = function() {
		setTimeout(updateSelects, 50);
	};

	function updateSelects () {
		var i;
		if ($scope.internalAdminMenu == "players") {
			for (i = 0; i < $scope.listPlayersInsert.length; i++) {
				$('#positionsSelect'+i).material_select();
				$('#teamsSelect'+i).material_select();
			}
		} else if ($scope.internalAdminMenu == "matches") {
			for (i = 0; i < $scope.listMatchesInsert.length; i++) {
				$('#matchDate'+i).pickadate({
					selectMonths: true, // Creates a dropdown to control month
					selectYears: 4, // Creates a dropdown of 15 years to control year,
					today: 'Today',
					clear: 'Clear',
					close: 'Ok',
					closeOnSelect: false // Close upon selecting a date,
				});
				$('#matchTime'+i).pickatime({
					default: '16:00', // Set default time: 'now', '1:30AM', '16:30'
					fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
					twelvehour: false, // Use AM/PM or 24-hour format
					donetext: 'OK', // text for done-button
					cleartext: 'Clear', // text for clear-button
					canceltext: 'Cancel', // Text for cancel-button
					autoclose: false, // automatic close timepicker
					ampmclickable: false, // make AM PM clickable
				});

				$('#teamsSelectVisitor'+i).material_select();
				$('#teamsSelectHome'+i).material_select();
				$('#stadium'+i).material_select();
			}
		}
	}

	$scope.login = function () {
		LoginFactory.login($scope.userLogin, LoginFactory.encryptSHA($scope.userPass), $scope.userRemember).then(function(){
			$scope.loggedUser = DataFactory.getInfo("loggedUser");
		});
	};
});