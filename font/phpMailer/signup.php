<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'src/Exception.php';
require 'src/PHPMailer.php';
require 'src/SMTP.php';

$email = $_POST['email'];
$name = $_POST['name'];

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

try {
    //Server settings
	$mail->CharSet = 'UTF-8';
	$mail->Encoding = "base64";
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'homie.mail.dreamhost.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'bolao@motta.ml';                 // SMTP username
    $mail->Password = 'x7gfsLrG';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('bolao@motta.ml', 'Bolão Copa do Mundo 2018');
    $mail->addAddress($email);										// Add a recipient
    //$mail->addAddress('ngm.motta@gmail.com', 'Nelson Gimenez da Motta');	// Name is optional
    $mail->addReplyTo('bolao@motta.ml', 'Bolão Copa do Mundo 2018');
    //$mail->addCC('lipeaspiracao@gmail.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Cadastro realizado com sucesso';
    $mail->Body    =	
		'<!-- HTML Codes by Quackit.com -->
		<!DOCTYPE html>
		<title>Text Example</title>
		<style>
		div.container {
		background-color: #ffffff;
		}
		div.container p {
		font-family: Arial;
		font-size: 14px;
		font-style: italic;
		font-weight: normal;
		text-decoration: none;
		text-transform: none;
		color: #000000;
		background-color: #ffffff;
		}
		</style>
		
		<div class="container">
		<p>Ol�, '.$name.',</p>
		<p>Seja bem-vindo ao <a href="https://www.motta.ml/bolao2018">Bol�o Copa do Mundo 2018</a>!<br>Estamos muito felizes em t�-lo conosco.</p>
		<p>Seu login j� est� liberado, mas algumas �reas do site estar�o inacess�veis, pois sua conta ainda est� em an�lise.<br>Seu acesso ser� totalmente liberado assim que identificarmos o pagamento.</p>
		<p>
		Para facilitar o pagamento online e evitar que os participantes tenham que arcar com o valor do DOC em uma transfer�ncia, disponibilizamos o pagamento via PayPal. O valor da inscri��o nesse meio de pagamento � ajustado para <b>R$53,26</b>, para cobrir as taxas cobradas pelo PayPal (4,99% + R$0,60 fixo por transa��o).<br>
		<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=ngm%2emotta%40gmail%2ecom&lc=BR&item_name=Inscri%c3%a7%c3%a3o%20%2d%20Bol%c3%a3o%20da%20Copa%20do%20Mundo%202018&amount=53%2e26&currency_code=BRL&button_subtype=services&bn=PP%2dBuyNowBF%3abtn_paynowCC_LG%2egif%3aNonHosted"><img src="https://www.motta.ml/bolao2018/img/paypal.png" alt=""></a><br>
		<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=ngm%2emotta%40gmail%2ecom&lc=BR&item_name=Inscri%c3%a7%c3%a3o%20%2d%20Bol%c3%a3o%20da%20Copa%20do%20Mundo%202018&amount=53%2e26&currency_code=BRL&button_subtype=services&bn=PP%2dBuyNowBF%3abtn_paynowCC_LG%2egif%3aNonHosted">Pagar com PayPal</a></p>
		<p>
		Caso prefira realizar transfer�ncia ou dep�sito do valor da inscri��o (<b>R$50,00</b>), utilize os dados a seguir:<br>
		<b>Banco</b>: Bradesco<br>
		<b>Ag�ncia</b>: 7917<br>
		<b>Conta</b>: 15762-7<br>
		<b>CPF</b>: 395.825.028-94<br>
		Nelson Gimenez da Motta</p>
		<p>Enviar o comprovante de transfer�ncia ou dep�sito por um dos meios abaixo:<br>
		<b>E-mail</b>: bolao@motta.ml<br>
		<b>Skype</b>: nelson-motta<br>
		<b>WhatsApp | Telegram</b>: (11) 96603-1088<br>
		</p>
		<p>Os mesmos contatos acima podem ser usados para combinar algum outro meio de pagamento.</p>
		<p>A data limite para recebimento do pagamento � <b>13/06/2018</b>, �s <b>23h59</b>.</p>
		<p>Se n�o foi voc� que se cadastrou, favor entrar em contato com o administrador do Bol�o em bolao@motta.ml.</p>
		<p>Obrigado!</p>
		</div>';
	
    $mail->AltBody =
		'Ol�, '.$name.',
		Seja bem-vindo ao Bol�o Copa do Mundo 2018!
		Estamos muito felizes em t�-lo conosco.
		
		Seu login j� est� liberado, mas algumas �reas do site estar�o inacess�veis, pois sua conta ainda est� em an�lise.
		Seu acesso ser� totalmente liberado assim que identificarmos o pagamento.
		
		Para facilitar o pagamento online e evitar que os participantes tenham que arcar com o valor do DOC em uma transfer�ncia, disponibilizamos o pagamento via PayPal. O valor da inscri��o nesse meio de pagamento � ajustado para R$53,26, para cobrir as taxas cobradas pelo PayPal (4,99% + R$0,60 fixo por transa��o). Para pagar com PayPal, use esse link:
		https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=ngm%2emotta%40gmail%2ecom&lc=BR&item_name=Inscri%c3%a7%c3%a3o%20%2d%20Bol%c3%a3o%20da%20Copa%20do%20Mundo%202018&amount=53%2e26&currency_code=BRL&button_subtype=services&bn=PP%2dBuyNowBF%3abtn_paynowCC_LG%2egif%3aNonHosted
		
		Caso prefira realizar transfer�ncia ou dep�sito do valor da inscri��o (R$50,00), utilize os dados a seguir:
		Banco: Bradesco
		Ag�ncia: 7917
		Conta: 15762-7
		CPF<: 395.825.028-94
		Nelson Gimenez da Motta
		
		Enviar o comprovante de transfer�ncia ou dep�sito por um dos meios abaixo:
		E-mail: bolao@motta.ml
		Skype: nelson-motta
		WhatsApp | Telegram: (11) 96603-1088
		
		Os mesmos contatos acima podem ser usados para combinar algum outro meio de pagamento.
		A data limite para recebimento do pagamento � 13/06/2018, �s 23h59.
		Se n�o foi voc� que se cadastrou, favor entrar em contato com o administrador do Bol�o em bolao@motta.ml.
		Obrigado!';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
	echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

?>