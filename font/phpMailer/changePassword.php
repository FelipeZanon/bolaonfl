<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'src/Exception.php';
require 'src/PHPMailer.php';
require 'src/SMTP.php';

$email = $_POST['email'];

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

try {
    //Server settings
	$mail->CharSet = 'UTF-8';
	$mail->Encoding = "base64";
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'homie.mail.dreamhost.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'bolao@motta.ml';                 // SMTP username
    $mail->Password = 'x7gfsLrG';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('bolao@motta.ml', 'Bolão Copa do Mundo 2018');
    $mail->addAddress($email);										// Add a recipient
    //$mail->addAddress('ngm.motta@gmail.com', 'Nelson Gimenez da Motta');	// Name is optional
    $mail->addReplyTo('bolao@motta.ml', 'Bolão Copa do Mundo 2018');
    //$mail->addCC('lipeaspiracao@gmail.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Senha Alterada';
    $mail->Body    =	
		'<!-- HTML Codes by Quackit.com -->
		<!DOCTYPE html>
		<title>Text Example</title>
		<style>
		div.container {
		background-color: #ffffff;
		}
		div.container p {
		font-family: Arial;
		font-size: 14px;
		font-style: italic;
		font-weight: normal;
		text-decoration: none;
		text-transform: none;
		color: #000000;
		background-color: #ffffff;
		}
		</style>
		
		<div class="container">
		<p>Olá,</p>
		<p></p>
		<p>Você está recebendo esse e-mail porque sua senha foi alterada no <a href="https://www.motta.ml/bolao2018">Bolão Copa do Mundo 2018</a>.</p>
		<p></p>
		<p>Por segurança, você foi desconectado de todos os outros aparelhos que usou para fazer login no site anteriormente.</p>
		<p>Se não foi você que alterou sua senha, favor entrar em contato urgentemente com o administrador do Bolão em bolao@motta.ml.</p>
		<p></p>
		<p>Obrigado!</p>
		</div>';
	
    $mail->AltBody = 'Olá, você está recebendo esse e-mail porque sua senha foi alterada no Bolão Copa do Mundo 2018.
			Por segurança, você foi desconectado de todos os outros aparelhos que usou para fazer login no site anteriormente. Se não foi você que fez o pedido de reset da sua senha, favor entrar em contato urgentemente com o administrador do Bolão em bolao@motta.ml.
			Obrigado!';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
	echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

?>