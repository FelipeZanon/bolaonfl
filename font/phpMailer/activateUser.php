<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'src/Exception.php';
require 'src/PHPMailer.php';
require 'src/SMTP.php';

$email = $_POST['email'];
$name = $_POST['name'];

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

try {
    //Server settings
	$mail->CharSet = 'UTF-8';
	$mail->Encoding = "base64";
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'homie.mail.dreamhost.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'bolao@motta.ml';                 // SMTP username
    $mail->Password = 'x7gfsLrG';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('bolao@motta.ml', 'Bolão Copa do Mundo 2018');
    $mail->addAddress($email);										// Add a recipient
    //$mail->addAddress('ngm.motta@gmail.com', 'Nelson Gimenez da Motta');	// Name is optional
    $mail->addReplyTo('bolao@motta.ml', 'Bolão Copa do Mundo 2018');
    //$mail->addCC('lipeaspiracao@gmail.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Sua conta foi ativada';
    $mail->Body    =	
		'<!-- HTML Codes by Quackit.com -->
		<!DOCTYPE html>
		<title>Text Example</title>
		<style>
		div.container {
		background-color: #ffffff;
		}
		div.container p {
		font-family: Arial;
		font-size: 14px;
		font-style: italic;
		font-weight: normal;
		text-decoration: none;
		text-transform: none;
		color: #000000;
		background-color: #ffffff;
		}
		</style>
		
		<div class="container">
		<p>Olá, '.$name.',</p>
		<p></p>
		<p>Identificamos seu pagamento para o <a href="https://www.motta.ml/bolao2018">Bolão Copa do Mundo 2018</a>!</p>
		<p></p>
		<p>A partir de agora, você já tem acesso a todas as áreas do site e poderá <a href="https://www.motta.ml/bolao2018/apostar">apostar</a> em todos os jogos da fase de grupos, além das <a href="https://www.motta.ml/bolao2018/extras">apostas extras</a>!<br>Veja aqui um <a href="https://www.motta.ml/bolao2018/tutorial">tutorial</a> explicando o funcionamento do bolão.</p>
		<p></p>
		<p>Se tiver alguma dúvida com relação ao funcionamento do site, favor entrar em contato com o administrador do Bolão em bolao@motta.ml.</p>
		<p></p>
		<p>Boa sorte!</p>
		</div>';
	
    $mail->AltBody = 'Olá, '.$name.',
	
		Identificamos seu pagamento para o Bolão Copa do Mundo 2018!
		
		A partir de agora, você já tem acesso a todas as áreas do site e poderá apostar em todos os jogos da fase de grupos, além das apostas extras!
		Veja aqui um tutorial explicando o funcionamento do bolão: https://www.motta.ml/bolao2018/tutorial
		
		Se tiver alguma dúvida com relação ao funcionamento do site, favor entrar em contato com o administrador do Bolão em bolao@motta.ml.
		
		Boa sorte!';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
	echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

?>