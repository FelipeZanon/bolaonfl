<?php
require_once 'google-api-php-client-2.2.1/vendor/autoload.php';
$CLIENT_ID = "380684870392-ifj99tcljeocvi6igjhk58oo35m9crkv.apps.googleusercontent.com";
// Get $id_token via HTTPS POST.

$id_token = $_GET['id_token'];

$client = new Google_Client(['client_id' => $CLIENT_ID]);  // Specify the CLIENT_ID of the app that accesses the backend
$payload = $client->verifyIdToken($id_token);
if ($payload) {
	$userid = $payload['sub'];
	echo $userid;
} else {
	// Invalid ID token
	echo "false";  
}

?>