app.directive('passwordEval', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, mCtrl) {
      function myValidation(value) {
          if (value.length < 6)
              mCtrl.$setValidity('passLength', false);
          else 
              mCtrl.$setValidity('passLength', true);
          
          return value;
      }
      mCtrl.$parsers.push(myValidation);
    }
  };
});
