<!doctype html>
<html ng-app="bolaoApp">
	<header>
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
<!--		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119267009-1"></script>-->
<!--
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
		  
			gtag('config', 'UA-119267009-1');
		</script>
-->

		
		<title>Bolão NFL 2020/21</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="google-signin-client_id" content="380684870392-ifj99tcljeocvi6igjhk58oo35m9crkv.apps.googleusercontent.com">
		
		<meta property="og:url" content="https://www.felipe.zanon.tk/bolaonfl/"/>
		<meta property="og:title" content="Bolão NFL 2020/21"/>
		<meta property="og:image" content="https://www.felipe.zanon.tk/bolaonfl/img/nfl_misc/NFL.gif"/>
		<meta property="og:description" content="Bolão da temporada 2018/19 da NFL" />
		<meta property="og:type" content="website"/>

		<link rel="shortcut icon" href="img/favicon.png?123456"/>

		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script async src="https://code.jquery.com/ui/jquery-ui-git.js"></script>		
		<script src="materialize/js/materialize.js"></script>		

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<link type="text/css" rel="stylesheet" href="materialize/css/materialize.css"  media="screen,projection"/>
		<link type="text/css" rel="stylesheet" href="css/main.css">

	</header>
	<main>
		<div class="row">
			<br>
			<div class="col s12 m6 offset-m3 center-align"><a href="https://www.motta.ml/bolao2018"><img src="img/error404.gif" style="max-width: 100%;"></a></div>
			<div class="col s12 center-align" style="font-size: 18px;">
				<b>ERRO 404</b><br><br>
				Opa, parece que você digitou alguma coisa errada e nosso ETzinho decolou.<br>
				<a href="https://www.motta.ml/bolao2018">Clique aqui</a> para retornar ao Bolão.
			</div>
		</div>
	</main>
</html>
