<?php
@session_start();
include("sql.php");	

$id_user = $_SESSION['user_id'];
$type = $_GET['type'];

if ($type == 'account') {
    if (empty($_GET['fullname']) || !isset($_GET['fullname']) || $_GET['fullname'] == "undefined")
        $fullname = NULL;
    else 
        $fullname = $_GET['fullname'];

    if (empty($_GET['name']) || !isset($_GET['name']) || $_GET['name'] == "undefined")
        $name = NULL;
    else 
        $name = $_GET['name'];

    if (empty($_GET['login']) || !isset($_GET['login']) || $_GET['login'] == "undefined")
        $login = NULL;
    else 
        $login = $_GET['login'];

    if (empty($_GET['pass']) || !isset($_GET['pass']) || $_GET['pass'] == "undefined")
        $pass = NULL;
    else 
        $pass = $_GET['pass'];

    if (empty($_GET['newpass']) || !isset($_GET['newpass']) || $_GET['newpass'] == "undefined") {
        $query = $conn->prepare("UPDATE users 
                                SET name =  COALESCE(?, name),
                                full_name = COALESCE(?, full_name), 
                                login = COALESCE(?, login) 
                                WHERE id = ? AND password = ?");

        $query->bind_param("sssss", $name, $fullname, $login, $id_user, $pass);

    } else {
        $newpass = $_GET['newpass'];
        $query = $conn->prepare("UPDATE users 
                                SET name =  COALESCE(?, name),
                                full_name = COALESCE(?, full_name), 
                                login = COALESCE(?, login),
                                password = COALESCE (?, password)
                                WHERE id = ? AND password = ?");

        $query->bind_param("ssssss", $name, $fullname, $login, $newpass, $id_user, $pass);
    }
} else if ($type == 'profile') {
    if (empty($_GET['icon']) || !isset($_GET['icon']) || $_GET['icon'] == "undefined")
        $icon = NULL;
    else 
        $icon = $_GET['icon'];
    
    if (empty($_GET['color']) || !isset($_GET['color']) || $_GET['color'] == "undefined")
        $color = NULL;
    else 
        $color = $_GET['color'];

    $query = $conn->prepare("INSERT INTO users_icon (id_user, icon, color)
                            VALUES (?, ?, ?)
                            ON DUPLICATE KEY UPDATE
                            icon = COALESCE(?, icon),
                            color= COALESCE(?, color)");
                            
    $query->bind_param("sssss", $id_user, $icon, $color, $icon, $color);

} else if ($type == 'online') {
    $query = $conn->prepare("INSERT INTO users_online (id_user)
                            VALUES (?)
                            ON DUPLICATE KEY UPDATE
                            timestamp = NOW()");
    $query->bind_param("s", $id_user);    
}


if ($query->execute() === false)
    die('execute() failed: ' . htmlspecialchars($query->error));

echo mysqli_affected_rows($conn);


$query->close();
$conn->close();
