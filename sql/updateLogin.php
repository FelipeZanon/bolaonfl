<?php
@session_start();
include("sql.php");	

$id_user = $_SESSION['user_id'];
$type = $_GET['type'];

print $id_user;

if ($type == "cookies") {
    
    $query = $conn->prepare("INSERT INTO cookies (id_user, cookie) VALUES (?, ?)");
    
    $cookie =   $_GET['cookie'];
    $query->bind_param("ss", $id_user, $cookie);
}

if ($query->execute() === false)
    die('execute() failed: ' . htmlspecialchars($query->error));


$query->close();
$conn->close();
