<?php
	include("sql.php");	
	$season = $_GET['season'];

	$query = $conn->prepare("SELECT SQL_NO_CACHE teams_byes.id_team, teams_byes.bye_week,
													teams.name, teams.alias, teams.code
													FROM teams_byes
													INNER JOIN teams ON teams.id = teams_byes.id_team
													WHERE teams_byes.id_season = ?");

	$query->bind_param("s", $season);


	if ($query->execute() === false) {
		die('execute() failed: ' . htmlspecialchars($query->error));
	}
	
	$result = $query->get_result();
	
	$rows = array();
	while($singleRow = mysqli_fetch_assoc($result)) {
		$rows[] = array_map('utf8_encode', $singleRow);
	}
	$jsonRows = json_encode($rows, JSON_NUMERIC_CHECK);
	print $jsonRows;

	$query->close();
	$conn->close();
?>