<?php
include("sql.php");

$email = $_GET['email'];
$password = $_GET['password'];
$user = $_GET['user'];
$season = $_GET['season'];
$icon = $_GET['icon'];;
$color = $_GET['color'];

$query = $conn->prepare("INSERT INTO users (login, password, name) VALUES (?, ?, ?)");
$query->bind_param("sss", $email, $password, $user);

if ($query->execute() === false)
    die('execute() failed: ' . htmlspecialchars($query->error));

$user_id = $query->insert_id;

$query = $conn->prepare("INSERT INTO users_season (id_user, id_season) VALUES (?, ?)");
$query->bind_param("ss", $user_id, $season);

if ($query->execute() === false)
    die('execute() failed: ' . htmlspecialchars($query->error));

$query = $conn->prepare("INSERT INTO users_icon (id_user, icon, color) VALUES (?, ?, ?)");
$query->bind_param("sss", $user_id, $icon, $color);

if ($query->execute() === false)
    die('execute() failed: ' . htmlspecialchars($query->error));

$query->close();
$conn->close();
?>