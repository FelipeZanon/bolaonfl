<?php
    include("sql.php");

    $data_info = "";
    $rows = 0;

    if (!isset($_GET['home_team']) || 
        !isset($_GET['home_team_score']) || 
        !isset($_GET['away_team']) || 
        !isset($_GET['away_team_score']) || 
        !isset($_GET['status']) ||
        !isset($_GET['key'])) {

        $query_status = "error";
        $data_info = "Missing arguments (home_team, home_team_score, away_team, away_team_score, status, key)";

    } else {

        if (!isset($_GET['possession']))
            $possession = "home";
        else
            $possession = $_GET['possession'];

        $home_team = $_GET['home_team'];
        $home_team_score = $_GET['home_team_score'];
        $away_team = $_GET['away_team'];
        $away_team_score = $_GET['away_team_score'];
        $status = $_GET['status'];
        $key = $_GET['key'];
        $id_season = 8;

        if ($key == "5ZOi5erX3Mdx1ojaiO5MMf2ZEv1CkCRp" || $key == "0u7obXRSxdOniJK5lvUnk06V9MZJ1fYC") {
            
            if ($key == "0u7obXRSxdOniJK5lvUnk06V9MZJ1fYC") {
                $query = $conn->prepare("UPDATE matches
                                        SET away_points = ?,
                                        home_points = ?,
                                        status = ?,
                                        possession = ?
                                        WHERE id_away_team = (
                                            SELECT id 
                                            FROM teams
                                            WHERE code = ?
                                        )
                                        AND id_home_team = (
                                            SELECT id 
                                            FROM teams
                                            WHERE code = ?
                                        )
                                        AND week >= 18
                                        AND id_season = ?;");

            } else {
                $query = $conn->prepare("UPDATE matches
                                        SET away_points = ?,
                                        home_points = ?,
                                        status = ?,
                                        possession = ?
                                        WHERE id_away_team = (
                                            SELECT id 
                                            FROM teams
                                            WHERE code = ?
                                        )
                                        AND id_home_team = (
                                            SELECT id 
                                            FROM teams
                                            WHERE code = ?
                                        )
                                        AND week <= 17
                                        AND id_season = ?;");
            }

            $query->bind_param("sssssss", $away_team_score, $home_team_score, $status, $possession, $away_team, $home_team, $id_season);

            if ($query->execute() === false)
                die('execute() failed: ' . htmlspecialchars($query->error));

            $match_id = $query->insert_id;
            $rows = $query->affected_rows;
            if ($rows > 0) {
                $data_info = array (
                    "match_id"=>$match_id,
                    "away_team"=>$away_team,
                    "away_team_score"=>$away_team_score,
                    "home_team_score"=>$home_team_score,
                    "home_team"=>$home_team,
                    "season"=>$id_season,
                    "status"=>$status,
                    "possession"=>$possession
                );
            }
            $query_status = "success";

            $query->close();
            $conn->close();
        } else {
            $query_status = "error";
            $data_info = "Wrong key.";
        }


    }



    $data_return = array(
        "status"=>$query_status,
        "rows affected"=>$rows,
        "data"=>$data_info
    );

    $data_json = json_encode($data_return, JSON_NUMERIC_CHECK);
    print $data_json;
?>