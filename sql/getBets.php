<?php
    include("sql.php");
    $season = $_GET['season'];
    $type = $_GET['type'];

    if ($type == 'regular') {
        $week = $_GET['week'];
        if ($week != 0) {        
            $query = $conn->prepare("SELECT bets.id, bets.id_match, bets.id_bet, bets.id_user,
                                    matches.timestamp, matches.week, users.name AS name,
                                    users_icon.color AS color, users_icon.icon AS icon
                                    FROM bets
                                    INNER JOIN matches 		ON matches.id = bets.id_match
                                    INNER JOIN users		ON users.id = bets.id_user
                                    LEFT JOIN users_icon    ON users.id = users_icon.id_user
                                    WHERE matches.timestamp <= UNIX_TIMESTAMP()
                                    AND matches.week = ?
                                    AND matches.id_season = ?
                                    AND bets.timestamp = (
                                    SELECT MAX(b2.timestamp) FROM bets AS b2 WHERE b2.id_match = bets.id_match AND b2.id_user = bets.id_user)
                                    GROUP BY bets.id_match, bets.id_user");
            $query->bind_param("ss", $week, $season);
        } else {
            $query = $conn->prepare("SELECT bets.id, bets.id_match, bets.id_bet, bets.id_user,
                                    matches.timestamp, matches.week, users.name AS name,
                                    users_icon.color AS color, users_icon.icon AS icon
                                    FROM bets
                                    INNER JOIN matches 		ON matches.id = bets.id_match
                                    INNER JOIN users		ON users.id = bets.id_user
                                    LEFT JOIN users_icon    ON users.id = users_icon.id_user
                                    WHERE matches.timestamp <= UNIX_TIMESTAMP()
                                    AND matches.id_season = ?
                                    AND bets.timestamp = (
                                    SELECT MAX(b2.timestamp) FROM bets AS b2 WHERE b2.id_match = bets.id_match AND b2.id_user = bets.id_user)
                                    GROUP BY bets.id_match, bets.id_user");
            $query->bind_param("s", $season);
        }
        
        if ($query->execute() === false) {
            die('execute() failed: ' . htmlspecialchars($query->error));
        }

        $result = $query->get_result();

        $rows = array();
        while($singleRow = mysqli_fetch_assoc($result)) {
            $rows[] = array_map('utf8_encode', $singleRow);
        }
        $jsonRows = json_encode($rows, JSON_NUMERIC_CHECK);
        print $jsonRows;
    } else if ($type == 'extras') {
        $query = $conn->prepare("SELECT COUNT(*) AS TOTAL 
                                FROM matches 
                                WHERE timestamp <= UNIX_TIMESTAMP()
                                AND week >= 1
                                AND id_season = ?");
        $query->bind_param("s", $season);
        
        if ($query->execute() === false) {
            die('execute() failed: ' . htmlspecialchars($query->error));
        } else {
            $result = $query->get_result();

            $rows = array();
            while($singleRow = mysqli_fetch_assoc($result)) {
                $rows[] = array_map('utf8_encode', $singleRow);
            }
            $totalRows = $rows[0]['TOTAL'];
            
            if ($totalRows == 0) {                
                die(json_encode(array('status' => 'error', 'message' => 'Season not started.')));
            } else {
                //query to select all non-wild card
                $query = $conn->prepare("SELECT b1.id, b1.id_user, b1.id_type, b1.timestamp, b1.id_team, 
                                        users_icon.icon, users_icon.color,
                                        extra_bets_types.description,
                                        users.name
                                        FROM extra_bets b1
                                        LEFT JOIN users_icon
                                        ON users_icon.id_user = b1.id_user
                                        LEFT JOIN extra_bets_types
                                        ON extra_bets_types.id = b1.id_type
                                        LEFT JOIN users
                                        ON users.id = b1.id_user
                                        WHERE b1.id_season = ?
                                        AND b1.timestamp = (
                                            SELECT MAX(timestamp) 
                                            FROM extra_bets b2 
                                            WHERE b2.id_season = b1.id_season
                                            AND b2.id_user = b1.id_user
                                            AND b2.id_type = b1.id_type
                                            GROUP BY b2.id_type
                                        )
                                        AND b1.id_type != 12
                                        AND b1.id_type != 13
                                        GROUP BY b1.id_type, b1.id_user
                                        ORDER BY b1.timestamp DESC");

                $query->bind_param("s", $season);

                if ($query->execute() === false) {
                    die('execute() failed: ' . htmlspecialchars($query->error));
                }

                $result = $query->get_result();	
                $rows = array();
                while($singleRow = mysqli_fetch_assoc($result)) {
                    $rows[] = array_map('utf8_encode', $singleRow);
                }
                $eb_json = json_encode($rows, JSON_NUMERIC_CHECK);
                $eb = $rows;

                //query to select all wild card
                $query = $conn->prepare("SELECT extra_bets.id, extra_bets.id_user, extra_bets.id_type, extra_bets.timestamp,
                                        users_icon.icon, users_icon.color,
                                        extra_bets_types.description,
                                        users.name,
                                        substring_index(group_concat(id_team ORDER BY timestamp DESC SEPARATOR ','), ',', 3) as bets
                                        FROM extra_bets
                                        LEFT JOIN users_icon
                                        ON users_icon.id_user = extra_bets.id_user
                                        LEFT JOIN extra_bets_types
                                        ON extra_bets_types.id = extra_bets.id_type
                                        LEFT JOIN users
                                        ON users.id = extra_bets.id_user
                                        WHERE id_season = ?
                                        AND id_type = 12
                                        GROUP BY id_user

                                        UNION 

                                        SELECT extra_bets.id, extra_bets.id_user, extra_bets.id_type, extra_bets.timestamp,
                                        users_icon.icon, users_icon.color,
                                        extra_bets_types.description,
                                        users.name,
                                        substring_index(group_concat(id_team ORDER BY timestamp DESC SEPARATOR ','), ',', 3) as bets
                                        FROM extra_bets 
                                        LEFT JOIN users_icon
                                        ON users_icon.id_user = extra_bets.id_user
                                        LEFT JOIN extra_bets_types
                                        ON extra_bets_types.id = extra_bets.id_type
                                        LEFT JOIN users
                                        ON users.id = extra_bets.id_user

                                        WHERE id_season = ?
                                        AND id_type = 13
                                        GROUP BY id_user");

                $query->bind_param("ss", $season, $season);

                if ($query->execute() === false) {
                    die('execute() failed: ' . htmlspecialchars($query->error));
                }

                $result = $query->get_result();	
                $rows = array();
                while($singleRow = mysqli_fetch_assoc($result)) {
                    $rows[] = array_map('utf8_encode', $singleRow);
                }
                $wc_json = json_encode($rows, JSON_NUMERIC_CHECK);
                $wc = $rows;

                for ($i = 0; $i < sizeof($wc); $i++) {
                    $array = explode(',', $wc[$i]['bets']);
                    for ($j = 0; $j < sizeof($array); $j++) {
                        $single_bet = array(
                            "id"=>$wc[$i]['id'],
                            "id_user"=>$wc[$i]['id_user'],
                            "id_type"=>$wc[$i]['id_type'],
                            "timestamp"=>$wc[$i]['timestamp'],
                            "id_team"=>$array[$j],
                            "icon"=>$wc[$i]['icon'],
                            "color"=>$wc[$i]['color'],
                            "description"=>$wc[$i]['description'],
                            "name"=>$wc[$i]['name']
                        );

                        array_push($eb, $single_bet);
                    }
                }

                $extra_bets = json_encode($eb, JSON_NUMERIC_CHECK);
                printf($extra_bets);
            }
        }
    }


    $query->close();
    $conn->close();