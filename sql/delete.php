<?php
@session_start();
include("sql.php");	

$id_user = $_SESSION['user_id'];
$type = $_GET['type'];

if ($type == "cookies") {
    
    $query = $conn->prepare("DELETE FROM cookies WHERE id_user = ? AND cookie = ?;");
    
    $cookie =   $_GET['cookie'];

    $query->bind_param("ss", $id_user, $cookie);        
}

if ($query->execute() === false)
    die('execute() failed: ' . htmlspecialchars($query->error));


$query->close();
$conn->close();
