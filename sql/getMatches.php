<?php
    include("sql.php");
    $data_info = "";
    $rows = 0;

    if (!isset($_GET['season']) || !isset($_GET['week'])) {
        $query_status = "error";
        $data_info = "Missing arguments (season, week)";
    }
  	$season = $_GET['season'];
    $week = $_GET['week'];

    if ($week != 0) {
        $query = $conn->prepare("SELECT SQL_NO_CACHE matches.id, matches.timestamp, matches.week, matches.away_points, matches.home_points, matches.status, matches.possession,
                                teamsHome.name AS team_home, teamsHome.alias AS team_home_alias, teamsHome.id AS id_team_home, teamsHome.code AS team_home_code, 
                                teamsAway.name AS team_away, teamsAway.alias AS team_away_alias, teamsAway.id AS id_team_away, teamsAway.code AS team_away_code
                                FROM matches
                                INNER JOIN teams as teamsHome 		ON matches.id_home_team=teamsHome.id
                                INNER JOIN teams as teamsAway 		ON matches.id_away_team=teamsAway.id
                                WHERE matches.id_season = ?
                                AND matches.week = ?
                                ORDER BY matches.timestamp ASC");
        $query->bind_param("ss", $season, $week);
    } else {
        $query = $conn->prepare("SELECT SQL_NO_CACHE matches.id, matches.timestamp, matches.week, matches.away_points, matches.home_points, matches.status, matches.possession,
                                teamsHome.name AS team_home, teamsHome.alias AS team_home_alias, teamsHome.id AS id_team_home, teamsHome.code AS team_home_code, 
                                teamsAway.name AS team_away, teamsAway.alias AS team_away_alias, teamsAway.id AS id_team_away, teamsAway.code AS team_away_code
                                FROM matches
                                INNER JOIN teams as teamsHome 		ON matches.id_home_team=teamsHome.id
                                INNER JOIN teams as teamsAway 		ON matches.id_away_team=teamsAway.id
                                WHERE matches.id_season = ?
                                ORDER BY matches.timestamp ASC");
        $query->bind_param("s", $season);
    }

	if ($query->execute() === false) {
		die('execute() failed: ' . htmlspecialchars($query->error));
	}
	
	$result = $query->get_result();
	
	$rows = array();
	while($singleRow = mysqli_fetch_assoc($result)) {
		$rows[] = array_map('utf8_encode', $singleRow);
	}
	$jsonRows = json_encode($rows, JSON_NUMERIC_CHECK);
	print $jsonRows;

    $query->close();
    $conn->close();
?>