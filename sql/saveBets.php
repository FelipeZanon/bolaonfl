<?php
@session_start();
if(!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])) {
   die(json_encode(array('message' => 'Stop trying to fuck the system.', 'code' => 666)));
} else {
    $id_user = $_SESSION['user_id'];
}

include("sql.php");
$type = $_GET['type'];
$season = $_GET['season'];
$id_match = $_GET['id_match'];

if ($type === "regular") {
    $query = $conn->prepare("SELECT COUNT(*) AS TOTAL 
                            FROM matches 
                            WHERE timestamp <= UNIX_TIMESTAMP() 
                            AND id_season = ?
                            AND id = ?");
    $query->bind_param("ss", $season, $id_match);
} else if($type === "extra") {
    $query = $conn->prepare("SELECT COUNT(*) AS TOTAL 
                            FROM matches 
                            WHERE timestamp <= UNIX_TIMESTAMP()
                            AND week >= 1
                            AND id_season = ?");
    // TO DO: change week back to 1 after sunday 16h
    $query->bind_param("s", $season);
}

if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
} else {
    $result = $query->get_result();

    $rows = array();
    while($singleRow = mysqli_fetch_assoc($result)) {
        $rows[] = array_map('utf8_encode', $singleRow);
    }
    $totalRows = $rows[0]['TOTAL'];

    if ($totalRows == 0) {   
        if ($type == "regular") {    
            $query = $conn->prepare("INSERT INTO bets (id_match, id_user, id_bet) VALUES (?, ?, ?)");
            
            $id_bet = $_GET['id_bet'];
            $query->bind_param("sss", $id_match, $id_user, $id_bet);
        } else if ($type == "extra") {
            $query = $conn->prepare("INSERT INTO extra_bets (id_season, id_user, id_team, id_type) SELECT ?, ?, ?, id FROM extra_bets_types WHERE description = ?");
            
            $id_bet = $_GET['id_bet'];
            $query->bind_param("ssss", $season, $id_user, $id_bet, $id_match);
        }        
        if ($query->execute() === false)
            die('execute() failed: ' . htmlspecialchars($query->error));
    } else {
        die(json_encode(array('status' => 'error', 'message' => 'Game/Season already started.', 'code' => 101)));
    }    
}

$query->close();
$conn->close();
die(json_encode(array('status' => 'success', 'message' => 'Bet successfully saved.', 'code' => 100)));
?>
