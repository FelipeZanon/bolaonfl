<?php
    include("sql.php");

    $type = $_GET['type'];
    if ($type == 'email') {
        $email = $_GET['email'];
        $query = $conn->prepare("SELECT SQL_NO_CACHE name FROM users WHERE login = ?");
        $query->bind_param("s", $email);
    } else if ($type == 'name') {
        $name = $_GET['name'];
        $query = $conn->prepare("SELECT SQL_NO_CACHE name FROM users WHERE name = ?");
        $query->bind_param("s", $name);
    }


    if ($query->execute() === false) {
        die('execute() failed: ' . htmlspecialchars($query->error));
    }
	
    $result = $query->get_result();

    $rows = array();
    while($singleRow = mysqli_fetch_assoc($result)) {
        $rows[] = array_map('utf8_encode', $singleRow);
    }
    $jsonRows = json_encode($rows, JSON_NUMERIC_CHECK);
    print $jsonRows;

    $query->close();
    $conn->close();
?>