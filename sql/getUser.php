<?php
@session_start();
include("sql.php");	

$id_user = $_SESSION['user_id'];

$query = $conn->prepare("SELECT SQL_NO_CACHE users.id, users.login, users.name, users.full_name,
                        users_icon.icon, users_icon.color
                        FROM users
                        LEFT JOIN users_icon ON users.id = users_icon.id_user
                        WHERE users.id = ?");

$query->bind_param("s", $id_user);

if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();

$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$jsonRows = json_encode($rows, JSON_NUMERIC_CHECK);
print $jsonRows;