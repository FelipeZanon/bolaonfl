<?php
@session_start();
include("sql.php");	

$season = $_GET['season'];
$type = $_GET['type'];

if ($type == "manual") {
    $query = $conn->prepare("SELECT SQL_NO_CACHE users.id, users.login, users.name, users.full_name,
                            users_icon.icon, users_icon.color
                            FROM users
                            INNER JOIN users_season ON users.id = users_season.id_user 
                            AND users_season.id_season = ?
                            LEFT JOIN users_icon ON users.id = users_icon.id_user
                            WHERE users.login = ? 
                            AND users.password = ?");
    
    $login = $_GET['login'];
    $pass = $_GET['pass'];
    
    $query->bind_param("sss", $season, $login, $pass);
} else if ($type == "cookies") {
    $query = $conn->prepare("SELECT SQL_NO_CACHE users.id, users.login, users.name, users.full_name,
                            users_icon.icon, users_icon.color
                            FROM cookies
                            INNER JOIN users ON users.id=cookies.id_user 
                            AND cookies.id_user = ?
                            INNER JOIN users_season ON users.id = users_season.id_user 
                            AND users_season.id_season = ?
                            LEFT JOIN users_icon ON users.id = users_icon.id_user
                            WHERE cookies.cookie = ?");
    
    $cookie = $_GET['cookie'];
    $id_user = $_GET['id_user'];
    
//    echo $season;
//    echo $cookie;
//    echo $id_user;

    $query->bind_param("sss", $id_user, $season, $cookie);
}

if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();

$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$jsonRows = json_encode($rows, JSON_NUMERIC_CHECK);

if (count($rows) > 0) {
    $_SESSION['user_id'] = $rows[0]['id'];
    print $jsonRows;
}
?>