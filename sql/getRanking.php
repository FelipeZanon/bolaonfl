<?php
function returnPoints ($match, $bet) {
    if ($match['away_points'] - $match['home_points'] > 0) { //away team won
        if ($match['away_points'] - $match['home_points'] > 7) { //away team won by more than 7 points (easy win)
            if ($bet['id_bet'] == 0)
                return 10;
            else if ($bet['id_bet'] == 1)
                return 5;
            else return 0;
        } else if ($match['away_points'] - $match['home_points'] <= 7) { //away team won by 7 points or less (hard win)
            if ($bet['id_bet'] == 0)
                return 5;
            else if ($bet['id_bet'] == 1)
                return 10;
            else return 0;
        }
    } else if ($match['home_points'] - $match['away_points'] > 0) { //home team won
        if ($match['home_points'] - $match['away_points'] > 7) { //home team won by more than 7 points (easy win)
            if ($bet['id_bet'] == 3)
                return 10;
            else if ($bet['id_bet'] == 2)
                return 5;
            else return 0;
        } else if ($match['home_points'] - $match['away_points'] <= 7) { //home team won by 7 points or less (hard win)
            if ($bet['id_bet'] == 2)
                return 10;
            else if ($bet['id_bet'] == 3)
                return 5;
            else return 0;
        }
    } else { //draw
        if ($bet['id_bet'] == 1 || $bet['id_bet'] == 2) {
            return 5;
        }
    }
    return 0;
};

include("sql.php");
header('Content-Type: application/json');

if (empty($_GET['season']) || !isset($_GET['season'])) {
    $season = 6;
} else {
    $season = $_GET['season'];
}

if (empty($_GET['week']) || !isset($_GET['week'])) {
    $week = 0;
    $week_info = "season";
} else {
    $week = $_GET['week'];
    $week_info = $week;
}

$totalPossiblePoints = 0;
$totalPossiblePointsAccumulated = 0;

$current_season = 2012 + $season;
$minified_season = 12 + $season;

$season_info = ($current_season) . "/" . ($current_season + 1);
$season_info_mini = $minified_season . "/" . ($minified_season + 1);

//Pegar todos os usuários
$query = $conn->prepare("SELECT SQL_NO_CACHE users.id, users.name, 
                        users_icon.icon, users_icon.color,
                        (CASE WHEN UNIX_TIMESTAMP(users_online.timestamp) >= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 2 MINUTE)) THEN TRUE ELSE FALSE END) AS online,
                        0 AS points, 0 AS tens, 0 AS fives, 0 AS percentage, 0 AS extra_points, 0 AS position, 0 AS bets_number, 
                        0 AS points_with_extras, 0 AS accumulated_points, 0 AS accumulated_tens, 0 AS accumulated_fives, 0 AS accumulated_percentage
                        FROM users
                        INNER JOIN users_season ON users.id = users_season.id_user
                        LEFT JOIN users_icon ON users.id = users_icon.id_user
                        LEFT JOIN users_online ON users.id = users_online.id_user
                        WHERE users_season.id_season = ?");

$query->bind_param("s", $season);
    
if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();

$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$users_json = json_encode($rows, JSON_NUMERIC_CHECK);
$users = $rows;

//Pegar todas as partidas
if ($week != 0) {
    
    $query = $conn->prepare("SELECT SQL_NO_CACHE matches.id, matches.timestamp, matches.week, matches.away_points, matches.home_points, matches.status, 
                            teamsHome.name AS team_home, teamsHome.alias AS team_home_alias, teamsHome.id AS id_team_home, 
                            teamsAway.name AS team_away, teamsAway.alias AS team_away_alias, teamsAway.id AS id_team_away
                            FROM matches
                            INNER JOIN teams as teamsHome 		ON matches.id_home_team=teamsHome.id
                            INNER JOIN teams as teamsAway 		ON matches.id_away_team=teamsAway.id
                            WHERE matches.id_season = ?
                            AND matches.week <= ?
                            ORDER BY matches.timestamp ASC");    

    $query->bind_param("ss", $season, $week);

} else {
    $query = $conn->prepare("SELECT SQL_NO_CACHE matches.id, matches.timestamp, matches.week, matches.away_points, matches.home_points, matches.status, 
                            teamsHome.name AS team_home, teamsHome.alias AS team_home_alias, teamsHome.id AS id_team_home, 
                            teamsAway.name AS team_away, teamsAway.alias AS team_away_alias, teamsAway.id AS id_team_away
                            FROM matches
                            INNER JOIN teams as teamsHome 		ON matches.id_home_team=teamsHome.id
                            INNER JOIN teams as teamsAway 		ON matches.id_away_team=teamsAway.id
                            WHERE matches.id_season = ?
                            ORDER BY matches.timestamp ASC");
    $query->bind_param("s", $season);
}
	
	
if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();

$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$matches_json = json_encode($rows, JSON_NUMERIC_CHECK);
$matches = $rows;


//Pegar todas as apostas
if ($week != 0) {
    $query = $conn->prepare("SELECT bets.id, bets.id_match, bets.id_bet, bets.id_user,
                                matches.timestamp, matches.week, users.name AS name,
                                users_icon.color AS color, users_icon.icon AS icon
                                FROM bets
                                INNER JOIN matches 		ON matches.id = bets.id_match
                                INNER JOIN users		ON users.id = bets.id_user
                                LEFT JOIN users_icon    ON users.id = users_icon.id_user
                                WHERE matches.timestamp <= UNIX_TIMESTAMP()
                                AND matches.id_season = ?
                                AND matches.week <= ?
                                AND bets.timestamp = (
                                SELECT MAX(b2.timestamp) FROM bets AS b2 WHERE b2.id_match = bets.id_match AND b2.id_user = bets.id_user)
                                GROUP BY bets.id_match, bets.id_user");

    $query->bind_param("ss", $season, $week);

} else {
    $query = $conn->prepare("SELECT bets.id, bets.id_match, bets.id_bet, bets.id_user,
                                matches.timestamp, matches.week, users.name AS name,
                                users_icon.color AS color, users_icon.icon AS icon
                                FROM bets
                                INNER JOIN matches 		ON matches.id = bets.id_match
                                INNER JOIN users		ON users.id = bets.id_user
                                LEFT JOIN users_icon    ON users.id = users_icon.id_user
                                WHERE matches.timestamp <= UNIX_TIMESTAMP()
                                AND matches.id_season = ?
                                AND bets.timestamp = (
                                SELECT MAX(b2.timestamp) FROM bets AS b2 WHERE b2.id_match = bets.id_match AND b2.id_user = bets.id_user)
                                GROUP BY bets.id_match, bets.id_user");
    $query->bind_param("s", $season);
}


if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();	
$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$bets_json = json_encode($rows, JSON_NUMERIC_CHECK);
$bets = $rows;

$matches_number = 0;
for ($i = 0; $i < sizeof($matches); $i++) {
    
    
    $match = $matches[$i];
    if ($match['week'] <= 17)
        $matchMultiplier = 1;
    else if ($match['week'] <= 19)
        $matchMultiplier = 2;
    else if ($match['week'] == 20)
        $matchMultiplier = 4;
    else if ($match['week'] == 21)
        $matchMultiplier = 8;
    
    if ($match['week'] == $week || $week == 0) {
        $matches_number++;
        $totalPossiblePoints = $totalPossiblePoints + (10 * $matchMultiplier);
    }
    
    $totalPossiblePointsAccumulated = $totalPossiblePointsAccumulated + (10 * $matchMultiplier);

    for ($j = 0; $j < sizeof($bets); $j++) {
        $bet = $bets[$j];
        if ($match['id'] === $bet['id_match']) {
            $match_point = returnPoints($match, $bet);
            for ($k = 0; $k < sizeof($users); $k++) {
                if ($bet['id_user'] == $users[$k]['id']) {
                    if ($match['week'] == $week || $week == 0) 
                        $users[$k]['bets_number'] = $users[$k]['bets_number'] + 1;
                    
                    if ($match_point == 10) {
                        if ($match['week'] == $week || $week == 0)
                            $users[$k]['tens']++;
                        
                        $users[$k]['accumulated_tens']++;
                    } else if ($match_point == 5) {
                        if ($match['week'] == $week || $week == 0)
                            $users[$k]['fives']++;
                        
                        $users[$k]['accumulated_fives']++;
                    }
                    
                    if ($match['week'] == $week || $week == 0)
                        $users[$k]['points'] = $users[$k]['points'] + ($match_point * $matchMultiplier);
                    
                    $users[$k]['accumulated_points'] = $users[$k]['accumulated_points'] + ($match_point * $matchMultiplier);
                    
                }
            }
        }
    }
    for ($l = 0; $l < sizeof($users); $l++) {
        $users[$l]['points_with_extras'] = $users[$l]['points'];
        if ($match['week'] == $week || $week == 0)
            $users[$l]['percentage'] = ($users[$l]['points'] / $totalPossiblePoints) * 100;
        
        $users[$l]['accumulated_percentage'] = ($users[$l]['accumulated_points'] / $totalPossiblePointsAccumulated) * 100;
    }

}

//Pegar extras corretas
$query = $conn->prepare("SELECT *
                            FROM extra_bets_results
                            WHERE id_season = ?");
$query->bind_param("s", $season);

if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();	
$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$correct_bets_json = json_encode($rows, JSON_NUMERIC_CHECK);
$correct_bets = $rows;

//query to select all non-wild card extra bets
$query = $conn->prepare("SELECT *
                            FROM extra_bets b1
                            WHERE b1.id_season = ?
                            AND b1.timestamp = (
                                SELECT MAX(timestamp) 
                                FROM extra_bets b2 
                                WHERE b2.id_season = b1.id_season
                                AND b2.id_user = b1.id_user
                                AND b2.id_type = b1.id_type
                                GROUP BY b2.id_type
                            )
                            AND b1.id_type != 12
                            AND b1.id_type != 13
                            GROUP BY b1.id_type, b1.id_user
                            ORDER BY b1.timestamp DESC");

$query->bind_param("s", $season);

if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();	
$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$eb_json = json_encode($rows, JSON_NUMERIC_CHECK);
$eb = $rows;

if ($season <= 7){
    
    //query to select all wild card extra bets
    $query = $conn->prepare("SELECT *, 
                            substring_index(group_concat(id_team ORDER BY timestamp DESC SEPARATOR ','), ',', 2) as bets
                            FROM extra_bets 
                            WHERE id_season = ?
                            AND id_type = 12
                            GROUP BY id_user
    
                            UNION 
    
                            SELECT *, 
                            substring_index(group_concat(id_team ORDER BY timestamp DESC SEPARATOR ','), ',', 2) as bets
                            FROM extra_bets 
                            WHERE id_season = ?
                            AND id_type = 13
                            GROUP BY id_user");
    
    $query->bind_param("ss", $season, $season);
    
    if ($query->execute() === false) {
        die('execute() failed: ' . htmlspecialchars($query->error));
    }
    
    $result = $query->get_result();	
    $rows = array();
    while($singleRow = mysqli_fetch_assoc($result)) {
        $rows[] = array_map('utf8_encode', $singleRow);
    }
    $wc_json = json_encode($rows, JSON_NUMERIC_CHECK);
    $wc = $rows;
    
    for ($i = 0; $i < sizeof($correct_bets); $i++) {
        for ($j = 0; $j < sizeof($wc); $j++) {
            list($bet1, $bet2) = explode(',', $wc[$j]['bets']);
            if ($correct_bets[$i]['id_type'] == $wc[$j]['id_type'] &&
               ($correct_bets[$i]['id_team'] == $bet1 || $correct_bets[$i]['id_team'] == $bet2)) {
                for ($k = 0; $k < sizeof($users); $k++) {
                    if ($users[$k]['id'] == $wc[$j]['id_user']) {
                        $users[$k]['extra_points'] = $users[$k]['extra_points'] + 10;
                        $users[$k]['points_with_extras'] = $users[$k]['points_with_extras'] + 10;
                    }
                }
            }
        }
        
        for ($l = 0; $l < sizeof($eb); $l++) {
            if ($correct_bets[$i]['id_type'] == $eb[$l]['id_type'] && $correct_bets[$i]['id_team'] == $eb[$l]['id_team']) {
    
                for ($k = 0; $k < sizeof($users); $k++) {
                    if ($users[$k]['id'] == $eb[$l]['id_user']) {
                        if ($correct_bets[$i]['id_type'] == 3 || 
                            $correct_bets[$i]['id_type'] == 4 || 
                            $correct_bets[$i]['id_type'] == 5 || 
                            $correct_bets[$i]['id_type'] == 6 || 
                            $correct_bets[$i]['id_type'] == 8 || 
                            $correct_bets[$i]['id_type'] == 9 || 
                            $correct_bets[$i]['id_type'] == 10 || 
                            $correct_bets[$i]['id_type'] == 11) {
                            $bet_value = 20;
                        } else if ($correct_bets[$i]['id_type'] == 2 || 
                                 $correct_bets[$i]['id_type'] == 7) {
                            $bet_value = 50;
                        } else if ($correct_bets[$i]['id_type'] == 1) {
                            $bet_value = 100;
                        }
    
                        $users[$k]['extra_points'] = $users[$k]['extra_points'] + $bet_value;
                        $users[$k]['points_with_extras'] = $users[$k]['points_with_extras'] + $bet_value;
                    }
                }
            }
        }
    }

} else {
    
    //query to select all wild card extra bets
    $query = $conn->prepare("SELECT *, 
                            substring_index(group_concat(id_team ORDER BY timestamp DESC SEPARATOR ','), ',', 3) as bets
                            FROM extra_bets 
                            WHERE id_season = ?
                            AND id_type = 12
                            GROUP BY id_user
    
                            UNION 
    
                            SELECT *, 
                            substring_index(group_concat(id_team ORDER BY timestamp DESC SEPARATOR ','), ',', 3) as bets
                            FROM extra_bets 
                            WHERE id_season = ?
                            AND id_type = 13
                            GROUP BY id_user");
    
    $query->bind_param("ss", $season, $season);
    
    if ($query->execute() === false) {
        die('execute() failed: ' . htmlspecialchars($query->error));
    }
    
    $result = $query->get_result();	
    $rows = array();
    while($singleRow = mysqli_fetch_assoc($result)) {
        $rows[] = array_map('utf8_encode', $singleRow);
    }
    $wc_json = json_encode($rows, JSON_NUMERIC_CHECK);
    $wc = $rows;
    
    for ($i = 0; $i < sizeof($correct_bets); $i++) {
        for ($j = 0; $j < sizeof($wc); $j++) {
            list($bet1, $bet2, $bet3) = explode(',', $wc[$j]['bets']);
            if ($correct_bets[$i]['id_type'] == $wc[$j]['id_type'] &&
               ($correct_bets[$i]['id_team'] == $bet1 || $correct_bets[$i]['id_team'] == $bet2 || $correct_bets[$i]['id_team'] == $bet3)) {
                for ($k = 0; $k < sizeof($users); $k++) {
                    if ($users[$k]['id'] == $wc[$j]['id_user']) {
                        $users[$k]['extra_points'] = $users[$k]['extra_points'] + 10;
                        $users[$k]['points_with_extras'] = $users[$k]['points_with_extras'] + 10;
                    }
                }
            }
        }
    
        for ($l = 0; $l < sizeof($eb); $l++) {
            if ($correct_bets[$i]['id_type'] == $eb[$l]['id_type'] && $correct_bets[$i]['id_team'] == $eb[$l]['id_team']) {
    
                for ($k = 0; $k < sizeof($users); $k++) {
                    if ($users[$k]['id'] == $eb[$l]['id_user']) {
                        if ($correct_bets[$i]['id_type'] == 3 || 
                            $correct_bets[$i]['id_type'] == 4 || 
                            $correct_bets[$i]['id_type'] == 5 || 
                            $correct_bets[$i]['id_type'] == 6 || 
                            $correct_bets[$i]['id_type'] == 8 || 
                            $correct_bets[$i]['id_type'] == 9 || 
                            $correct_bets[$i]['id_type'] == 10 || 
                            $correct_bets[$i]['id_type'] == 11) {
                            $bet_value = 20;
                        } else if ($correct_bets[$i]['id_type'] == 2 || 
                                 $correct_bets[$i]['id_type'] == 7) {
                            $bet_value = 50;
                        } else if ($correct_bets[$i]['id_type'] == 1) {
                            $bet_value = 100;
                        }
    
                        $users[$k]['extra_points'] = $users[$k]['extra_points'] + $bet_value;
                        $users[$k]['points_with_extras'] = $users[$k]['points_with_extras'] + $bet_value;
                    }
                }
            }
        }
    }
    
}

    

foreach ($users as $key => $row) {
    $points[$key]  = $row['points'];
    $points_with_extras[$key]  = $row['points_with_extras'];
    $tens[$key] = $row['tens'];
    $fives[$key] = $row['fives'];
    $name[$key] = $row['name'];
    $name_lowercase = array_map('strtolower', $name);
}

if ($week == 0)
    array_multisort($points_with_extras, SORT_DESC, $tens, SORT_DESC, $fives, SORT_DESC, $name_lowercase, SORT_ASC, $users);
else
    array_multisort($points, SORT_DESC, $tens, SORT_DESC, $fives, SORT_DESC, $name_lowercase, SORT_ASC, $users);

for ($l = 0; $l < sizeof($users); $l++) {
    if ($l == 0) {
        $users[$l]['position'] = $l + 1;
        $lastUsedPosition = 1;
    } else {
        if ($week == 0)
            $tiebreaker = 'points_with_extras';
        else
            $tiebreaker = 'points';
        
        if ($users[$l][$tiebreaker] == $users[$l-1][$tiebreaker] && $users[$l]['tens'] == $users[$l-1]['tens'] &&
            $users[$l]['fives'] == $users[$l-1]['fives']) {
            $users[$l]['position'] = $lastUsedPosition;
        } else {
            $users[$l]['position'] = $l + 1;
            $lastUsedPosition = $l + 1;
        }
    }			
}

$info = array(
    "season_id"=>$season,
    "season"=>$season_info,
    "season_mini"=>$season_info_mini,
    "week"=>$week_info,
    "matches"=>$matches_number
);

array_unshift($users, $info);
$users_json = json_encode($users, JSON_NUMERIC_CHECK);
print $users_json;
?>
