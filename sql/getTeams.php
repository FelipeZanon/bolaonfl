<?php
	include("sql.php");	

	$query = $conn->prepare("SELECT SQL_NO_CACHE *
													FROM teams");

	if ($query->execute() === false) {
		die('execute() failed: ' . htmlspecialchars($query->error));
	}
	
	$result = $query->get_result();
	
	$rows = array();
	while($singleRow = mysqli_fetch_assoc($result)) {
		$rows[] = array_map('utf8_encode', $singleRow);
	}
	$jsonRows = json_encode($rows, JSON_NUMERIC_CHECK);
	print $jsonRows;

	$query->close();
	$conn->close();
?>