<?php
@session_start();
$id_user = $_SESSION['user_id'];

include("sql.php");
$type = $_GET['type'];
$season = $_GET['season'];

if ($type == "regular") {
    $query = $conn->prepare("SELECT bets.id, bets.id_match, bets.id_bet,
                            matches.timestamp, matches.week, users.name AS name,
                            users_icon.color AS color, users_icon.icon AS icon
                            FROM bets
                            INNER JOIN matches 		ON matches.id = bets.id_match
                            INNER JOIN users		ON users.id = bets.id_user
                            LEFT JOIN users_icon    ON users.id = users_icon.id_user
                            WHERE bets.id_user = ?
                            AND matches.id_season = ?
                            AND bets.timestamp = (
                            SELECT MAX(b2.timestamp) FROM bets AS b2 WHERE b2.id_match = bets.id_match AND b2.id_user = bets.id_user)
                            GROUP BY bets.id_match, bets.id_user");
    
    $query->bind_param("ss", $id_user, $season);    
} else if ($type == "extra") {
    $query = $conn->prepare("(SELECT b1.id, b1.id_type, b1.id_team,
                            extra_bets_types.description, teams.conference, teams.division
                            FROM extra_bets b1
                            INNER JOIN teams ON b1.id_team = teams.id
                            INNER JOIN extra_bets_types ON b1.id_type = extra_bets_types.id
                            WHERE b1.id_user = ?
                            AND b1.id_season = ? 
                            AND b1.id_type = 12
                            ORDER BY b1.timestamp DESC
                            LIMIT 3)
                            
                            UNION
                            
                            (SELECT b1.id, b1.id_type, b1.id_team,
                            extra_bets_types.description, teams.conference, teams.division
                            FROM extra_bets b1
                            INNER JOIN teams ON b1.id_team = teams.id
                            INNER JOIN extra_bets_types ON b1.id_type = extra_bets_types.id
                            WHERE b1.id_user = ?
                            AND b1.id_season = ? 
                            AND b1.id_type = 13
                            ORDER BY b1.timestamp DESC
                            LIMIT 3)
                            
                            UNION
                            
                            (SELECT b1.id, b1.id_type, b1.id_team, 
                            extra_bets_types.description, teams.conference, teams.division
                                FROM extra_bets b1
                                INNER JOIN teams ON b1.id_team = teams.id
                                INNER JOIN extra_bets_types ON b1.id_type = extra_bets_types.id
                                WHERE b1.id_user = ?
                                AND b1.timestamp = (
                                    SELECT MAX(timestamp) 
                                    FROM extra_bets b2 
                                    WHERE b2.id_season = b1.id_season
                                    AND b2.id_user = b1.id_user
                                    AND b2.id_type = b1.id_type
                                    GROUP BY b2.id_type
                                )
                                AND b1.id_type != 12
                                AND b1.id_type != 13
                                AND b1.id_season = ?
                                GROUP BY b1.id_type, b1.id_user
                                ORDER BY b1.timestamp DESC);");
    $query->bind_param("ssssss", $id_user, $season, $id_user, $season, $id_user, $season);    
}


if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();

$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$jsonRows = json_encode($rows, JSON_NUMERIC_CHECK);
print $jsonRows;


?>