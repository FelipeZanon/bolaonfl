<?php
function returnPoints ($match, $bet) {
    if ($match['away_points'] - $match['home_points'] > 0) { //away team won
        if ($match['away_points'] - $match['home_points'] > 7) { //away team won by more than 7 points (easy win)
            if ($bet['id_bet'] == 0)
                return 10;
            else if ($bet['id_bet'] == 1)
                return 5;
            else return 0;
        } else if ($match['away_points'] - $match['home_points'] <= 7) { //away team won by 7 points or less (hard win)
            if ($bet['id_bet'] == 0)
                return 5;
            else if ($bet['id_bet'] == 1)
                return 10;
            else return 0;
        }
    } else if ($match['home_points'] - $match['away_points'] > 0) { //home team won
        if ($match['home_points'] - $match['away_points'] > 7) { //home team won by more than 7 points (easy win)
            if ($bet['id_bet'] == 3)
                return 10;
            else if ($bet['id_bet'] == 2)
                return 5;
            else return 0;
        } else if ($match['home_points'] - $match['away_points'] <= 7) { //home team won by 7 points or less (hard win)
            if ($bet['id_bet'] == 2)
                return 10;
            else if ($bet['id_bet'] == 3)
                return 5;
            else return 0;
        }
    } else { //draw
        if ($bet['id_bet'] == 1 || $bet['id_bet'] == 2) {
            return 5;
        }
    }
    return 0;
};

include("sql.php");
header('Content-Type: application/json');

if (empty($_GET['season']) || !isset($_GET['season'])) {
    $season = 6;
} else {
    $season = $_GET['season'];
}

//Pegar todos os usuários
$query = $conn->prepare("SELECT SQL_NO_CACHE users.id, users.name, 
                        users_icon.icon, users_icon.color,
                        users_season.id AS season_id,
                        0 AS points, 0 AS tens, 0 AS fives, 0 AS percentage, 0 AS extra_points, 0 AS position, 0 AS bets_number, 
                        0 AS points_with_extras, 0 AS accumulated_points, 0 AS accumulated_tens, 0 AS accumulated_fives, 0 AS accumulated_percentage
                        FROM users
                        INNER JOIN users_season ON users.id = users_season.id_user AND users_season.id_season = ?
                        LEFT JOIN users_icon ON users.id = users_icon.id_user
                        LEFT JOIN users_online ON users.id = users_online.id_user");

$query->bind_param("s", $season);

if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();

$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$all_users_json = json_encode($rows, JSON_NUMERIC_CHECK);
$all_users = $rows;

for ($i = 0; $i < 21; $i++) {
    $weekly_ranking[$i] = [];
}

//Pegar todas as partidas
$query = $conn->prepare("SELECT SQL_NO_CACHE matches.id, matches.timestamp, matches.week, matches.away_points, matches.home_points, matches.status, 
                        teamsHome.name AS team_home, teamsHome.alias AS team_home_alias, teamsHome.id AS id_team_home, 
                        teamsAway.name AS team_away, teamsAway.alias AS team_away_alias, teamsAway.id AS id_team_away
                        FROM matches
                        INNER JOIN teams as teamsHome 		ON matches.id_home_team=teamsHome.id
                        INNER JOIN teams as teamsAway 		ON matches.id_away_team=teamsAway.id
                        WHERE matches.id_season = ?
                        ORDER BY matches.timestamp ASC");

$query->bind_param("s", $season);

if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();

$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$matches_json = json_encode($rows, JSON_NUMERIC_CHECK);
$matches = $rows;

//Pegar todas as apostas
$query = $conn->prepare("SELECT bets.id, bets.id_match, bets.id_bet, bets.id_user,
                            matches.timestamp, matches.week, users.name AS name,
                            users_icon.color AS color, users_icon.icon AS icon
                            FROM bets
                            INNER JOIN matches 		ON matches.id = bets.id_match
                            INNER JOIN users		ON users.id = bets.id_user
                            LEFT JOIN users_icon    ON users.id = users_icon.id_user
                            WHERE matches.timestamp <= UNIX_TIMESTAMP()
                            AND matches.id_season = ?
                            AND bets.timestamp = (
                            SELECT MAX(b2.timestamp) FROM bets AS b2 WHERE b2.id_match = bets.id_match AND b2.id_user = bets.id_user)
                            GROUP BY bets.id_match, bets.id_user");
$query->bind_param("s", $season);

if ($query->execute() === false) {
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$result = $query->get_result();	
$rows = array();
while($singleRow = mysqli_fetch_assoc($result)) {
    $rows[] = array_map('utf8_encode', $singleRow);
}
$bets_json = json_encode($rows, JSON_NUMERIC_CHECK);
$bets = $rows;

$totalPossiblePointsAccumulated = 0;
$matches_number_accumulated = 0;
$season_ranking = [];

for ($count = 0; $count < 21; $count++) {
    $users = $all_users;
    $matches_number = 0;
    $totalPossiblePoints = 0;

    for ($i = 0; $i < sizeof($matches); $i++) {
        $match = $matches[$i];

        if ($match['week'] <= 17)
            $matchMultiplier = 1;
        else if ($match['week'] <= 19)
            $matchMultiplier = 2;
        else if ($match['week'] == 20)
            $matchMultiplier = 4;
        else if ($match['week'] == 21)
            $matchMultiplier = 8;

        if(($count + 1) == $match['week']) {
            $matches_number++;
            $totalPossiblePoints = $totalPossiblePoints + (10 * $matchMultiplier);
            $totalPossiblePointsAccumulated = $totalPossiblePointsAccumulated + (10 * $matchMultiplier);

            for ($j = 0; $j < sizeof($bets); $j++) {
                $bet = $bets[$j];
                if ($match['id'] === $bet['id_match']) {
                    $match_point = returnPoints($match, $bet);
                    for ($k = 0; $k < sizeof($users); $k++) {
                        if ($bet['id_user'] == $users[$k]['id']) {
                            $users[$k]['bets_number'] = $users[$k]['bets_number'] + 1;
                            
                            if ($match_point == 10) {
                                $users[$k]['tens']++;
                                $all_users[$k]['accumulated_tens']++;
                            } else if ($match_point == 5) {
                                $users[$k]['fives']++;
                                $all_users[$k]['accumulated_fives']++;
                            }
                            $users[$k]['points'] = $users[$k]['points'] + ($match_point * $matchMultiplier);                        
                            $all_users[$k]['accumulated_points'] = $all_users[$k]['accumulated_points'] + ($match_point * $matchMultiplier);
                        }
                    }
                }
            }
        }
    }

    $matches_number_accumulated += $matches_number;

    for ($l = 0; $l < sizeof($users); $l++) {
        $users[$l]['accumulated_tens'] = $all_users[$l]['accumulated_tens'];
        $users[$l]['accumulated_fives'] = $all_users[$l]['accumulated_fives'];
        $users[$l]['accumulated_points'] = $all_users[$l]['accumulated_points'];

        if ($totalPossiblePoints == 0)
            $users[$l]['percentage'] = 0;
        else 
            $users[$l]['percentage'] = ($users[$l]['points'] / $totalPossiblePoints) * 100;
        
        if ($totalPossiblePointsAccumulated == 0)
            $users[$l]['accumulated_percentage'] = 0;
        else
            $users[$l]['accumulated_percentage'] = ($all_users[$l]['accumulated_points'] / $totalPossiblePointsAccumulated) * 100;
    }

    $current_season = 2012 + $season;
    $minified_season = 12 + $season;

    $season_info = ($current_season) . "/" . ($current_season + 1);
    $season_info_mini = $minified_season . "/" . ($minified_season + 1);

    $info = array(
        "season_id"=>$season,
        "season"=>$season_info,
        "season_mini"=>$season_info_mini,
        "week"=>$count+1,
        "matches"=>$matches_number,
        "accumulated_matches"=>$matches_number_accumulated
    );

    array_unshift($users, $info);
    array_push($season_ranking, $users);
}

$season_ranking_json = json_encode($season_ranking, JSON_NUMERIC_CHECK);
print $season_ranking_json;

?>