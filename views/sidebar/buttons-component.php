<div id="buttons-container"
       class="animation"
       ng-class="!openedSidebar ? 'containers-closed' : ''">
    <div id="button" class="tooltip">
        <a class="waves-effect waves-light btn-large btn-flat blue-grey lighten-5 black-text"
           ng-click="expandMenu('Menu'); openedDropdown = false"
           href="">
            <span>Menu</span>
            <i class="icon-class fas fa-bars right"></i>

        </a>
        <span ng-show="!openedSidebar" class="tooltiptext tooltipRight">Menu</span>
    </div>
    <div id="button"
        class="tooltip" 
        ng-repeat="button in sidebarButtons">
        <a class="waves-effect waves-light btn-large btn-flat black-text"
        	ng-class="activeMenu == button.link ? 'active-button' : 'blue-grey lighten-5'"
            ng-show="button.text != 'Menu' && button.text != 'Login'" 
            ng-click="expandMenu(button.text)"
            href="#/{{button.link}}/">
            <span>{{button.text}}</span>
            <i class="icon-class right {{button.icon}}"></i>
        </a>
        <span ng-show="!openedSidebar" class="tooltiptext tooltipRight">{{button.text}}</span>
    </div>
    <div id="button"
        class="tooltip" 
        ng-show="loggedUser.length > 0">
        <a class="waves-effect waves-light btn-large btn-flat white green-text"
           ng-click="expandMenu('Login'); openedDropdown = !openedDropdown"           
           href="">
            <span ng-show="loggedUser.length == 0">Login</span>
            <span ng-show="loggedUser.length > 0">{{loggedUser[0].name}}</span>
            <i class="icon-class fas fa-user right"></i>
        </a>
        <span ng-show="!openedSidebar" class="tooltiptext tooltipRight">{{loggedUser[0].name}}</span>
    </div>
    <div id="dropdownLogin" 
         class="animation"
         ng-show="loggedUser.length > 0"
         ng-class="openedDropdown ? 'opened-dropdown' : ''">
        <ul class="noPadding noMargin">
            <a class="modal-trigger"
               href="#modalPreferences">
                <li class="center-align dropdown-item"
                    ng-show="loggedUser.length > 0"
                    openedDropdown = false>
                    Preferências
                </li>
            </a>
            <li class="divider" tabindex="-1"></li>
            <li class="center-align dropdown-item"
                ng-show="loggedUser.length > 0"
                ng-click="logout(); openedDropdown = false">
                Logout
            </li>
            <li class="divider" tabindex="-1"></li>
            <li class="center-align dropdown-item"
                ng-show="loggedUser.length > 0"
                ng-click="openedDropdown = false">
                Fechar
            </li>
        </ul>
    </div>
</div>