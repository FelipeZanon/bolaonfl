<div id="login-section"
     class="animation"
     ng-class="{'containers-closed login-section-closed':!openedSidebar}">
    <div class="row">
        <span ng-show="loggedUser.length == 0">
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" 
                               type="text" 
                               class="validate nfl-white"
                               ng-model="emailLogin"
                               ng-keyup="$event.keyCode == 13 && manualLogin()"
                               Placeholder="E-mail"
                               autocomplete="email"
                               autofocus>
                    </div>
                    <div class="input-field col s12 center-align">
                        <input id="password" 
                               type="password" 
                               class="validate nfl-white"
                               ng-model="passwordLogin"
                               ng-keyup="$event.keyCode == 13 && manualLogin()"
                               Placeholder="Password"
                               autocomplete="current-password">
                    </div>

                    <div class="col s6 center-align tooltip">
                        <a class="waves-effect waves-light btn-small btn-flat blue-text blue-grey lighten-5 modal-trigger"
                           style="width: 100%;"
                           href="#modalSignup">
                            <i class="small-icon-class fas fa-user-plus"></i>
                        </a>
                        <span class="tooltiptext">Registre-se!</span>
                    </div>
                    <div class="col s6 center-align tooltip">
                        <a class="waves-effect waves-light btn-small btn-flat green-text blue-grey lighten-5"
                           style="width: 100%;"
                           ng-click="manualLogin()">
                            <i class="small-icon-class fas fa-sign-in-alt"></i>
                        </a>
                        <span class="tooltiptext">Login</span>
                    </div>
                </div>
            </form>
        </span>
        <span ng-show="loggedUser.length > 0">
            <p class="col s12 center-align tooltip">
                <a href="https://t.me/joinchat/CjcJM0rzBojNCFWv_eJkCA" target="_blank"><i class="fab fa-telegram fa-3x white-text"></i></a>
                <span class="tooltiptext">Clique para entrar no nosso grupo do Telegram</span>
            </p>
            
        </span>
    </div>
</div>

