<ul id="mobile-sidebar"
    class="sidenav nfl-dark-blue"
    style="width: 250px !important">
    <li ng-class="activeMenu == button.link ? 'active-button' : 'blue-grey lighten-5'"
        ng-repeat="button in sidebarButtons">
        <a class=" black-text sidenav-close" 
           href="#/{{button.link}}/">
            {{button.text}} <i class="icon-class {{button.icon}} right"></i>
        </a>
    </li>
    <li ng-class="activeMenu == 'ranking' ? 'active-button' : 'blue-grey lighten-5'">
        <a class="black-text sidenav-close" 
           href="#/ranking/">
            Ranking <i class="icon-class fas fa-medal right"></i>
        </a>
    </li>
    <li class="blue-grey lighten-5" ng-show="loggedUser.length > 0">
        <a class="green-text text-darken-3 sidenav-close"
           ng-click="logout()">
            Logout <i class="icon-class fas fa-sign-out-alt right green-text"></i>
        </a>
    </li>
    <p class="center-align tooltip"
       style="width: 100%"
       ng-show="loggedUser.length > 0">
        <a href="https://t.me/joinchat/ByAyH0rzBoj_asurTcDNYQ" target="_blank"><i class="fab fa-telegram fa-3x white-text"></i></a>
        <span class="tooltiptext">Clique para entrar no nosso grupo do Telegram</span>
    </p>

</ul>