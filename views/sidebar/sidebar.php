<div id="sidebar" 
     class="nfl-dark-blue animation"
     ng-class="{'sidebar-closed':!openedSidebar}">
    <div id="sidebar-container"
         ng-class="{'sidebar-closed':!openedSidebar}">
        <?php require("views/sidebar/buttons-component.php"); ?>
        <?php require("views/sidebar/login-section.php"); ?>
        
<!--        Modal include-->
        <?php require("views/modals/signup.php"); ?>
        <?php require("views/modals/preferences.php"); ?>
    </div>
</div>