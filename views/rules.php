<div class="page-header rules-header col s12 cyan-text">
    Regras <i class="icon-class fas fa-tasks"></i>
</div>
<div class="row noPadding noMargin">
    <div class="col s12 noPadding noMargin">
        <ul id="tabsRules" class="tabs tabs-fixed-width">
            <li class="tab" ng-click="selectedRules = 1">
                <a ng-class="{'activeRules' : selectedRules == 1}" href="">Inscrição</a>
            </li>
            <li class="tab" ng-click="selectedRules = 2">
                <a ng-class="{'activeRules' : selectedRules == 2}" href="">Pontuação</a>
            </li>
            <li class="tab" ng-click="selectedRules = 3">
                <a ng-class="{'activeRules' : selectedRules == 3}" href="">Apostas</a>
            </li>
            <li class="tab" ng-click="selectedRules = 4">
                <a ng-class="{'activeRules' : selectedRules == 4}" href="">Extras</a>
            </li>
            <li class="tab" ng-click="selectedRules = 5">
                <a ng-class="{'activeRules' : selectedRules == 5}" href="">Premiação</a>
            </li>
        </ul>
    </div>
</div>

<div id="content" class="col s12" ng-show="selectedRules == 1">
	<?php require("rules/inscricao.php"); ?>
</div>
<div id="content" class="col s12" ng-show="selectedRules == 2">
	<?php require("rules/pontuacao.php"); ?>
</div>
<div id="content" class="col s12" ng-show="selectedRules == 3">
	<?php require("rules/apostas.php"); ?>
</div>
<div id="content" class="col s12" ng-show="selectedRules == 4">
	<?php require("rules/extras.php"); ?>
</div>
<div id="content" class="col s12" ng-show="selectedRules == 5">
	<?php require("rules/premiacao.php"); ?>
</div>