<div class="page-header extras-header col s12 orange-text text-darken-3">
    Apostas Extras <i class="icon-class fas fa-plus-square"></i>
</div>
<div id="sidebarTitle" class="row">
    <div class="col s12">
        <ul id="tabsExtras" 
            class="tabs">
            <li class="tab col s4">
                <a href="" 
                    ng-class="extrasType == 'AFC' ? 'active-round' : ''"
                    ng-click="extrasType = 'AFC'">
                    <img ng-src="img/nfl_misc/AFC.gif" class="img-valign nfl-logo">
                </a>
            </li>
            <li class="tab col s4">
                <a href="" 
                    ng-class="extrasType == 'NFC' ? 'active-round' : ''"
                    ng-click="extrasType = 'NFC'">
                    <img ng-src="img/nfl_misc/NFC.gif" class="img-valign nfl-logo">
                </a>
            </li>
            <li class="tab col s4">
                <a href="" 
                    ng-class="extrasType == 'Superbowl' ? 'active-round' : ''"
                    ng-click="extrasType = 'Superbowl'">
                    <img ng-src="img/nfl_misc/SB.png" class="img-valign nfl-logo">
                </a>
            </li>
        </ul>
    </div>
</div>

<div ng-show="(loggedUser.length > 0 && nowTimestamp < season_start) || nowTimestamp >= season_start">
	<span ng-show="extrasType != 'Superbowl'">
        <div class="col l6 s12"
             style="border-right: 1px solid lightgrey">
            <p class="center-align"><b>{{extrasType}} East</b></p>
            <div class="center-align noPadding">
                <span class="col s12 noPadding" ng-if="nowTimestamp >= season_start">
                    <div class="extras-bets noPadding shadowed left-align"
                         style="margin:5px;"
                         ng-repeat="team in listTeams | filter:{conference:extrasType}:true | filter:{division:'East'}:true | orderBy:['name']">
                        <div>
                            <div class="left waves-effect waves-light btn-large extras-button noPadding noLink"
                               ng-class="isExtraBet(team.id, extrasType, team.division) ? 'grey lighten-1' : 'grey lighten-3'">
                                <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                            </div>
                        </div>
                        <div class="extra-bets-content">
                            <span class="tooltip" 
                                  ng-repeat="icon in listAllExtraBets | extraBetsFilter:extrasType+'_East' | filter:{id_team:team.id}:true">
                                <i style="color: {{icon.color}}; padding:2px;"
                                   ng-class="icon.icon"></i>
                                <span class="tooltiptext">{{icon.name}}</span>
                            </span>
                            
                        </div>
                    </div>
                </span>
                <span class="col s12 noPadding" ng-if="nowTimestamp < season_start">
                    <a class="center-align waves-effect waves-light btn-large extras-button noPadding"
                       ng-class="isExtraBet(team.id, extrasType, team.division) ? 'grey lighten-1' : 'grey lighten-3'"
                       style="margin:10px;"
                       href=""
                       ng-repeat="team in listTeams | filter:{conference:extrasType}:true | filter:{division:'East'}:true | orderBy:['name']"
                       ng-click="saveExtraBet(team.id, extrasType, team.division)">
                        <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                    </a>
                </span>

            </div>
        </div>
        <div class="col l6 s12">
            <p class="center-align"><b>{{extrasType}} North</b></p>
            <div class="center-align noPadding">
                <span class="col s12 noPadding" ng-if="nowTimestamp >= season_start">
                    <div class="extras-bets noPadding shadowed left-align"
                         style="margin:5px;"
                         ng-repeat="team in listTeams | filter:{conference:extrasType}:true | filter:{division:'North'}:true | orderBy:['name']">
                        <div>
                            <div class="left waves-effect waves-light btn-large extras-button noPadding noLink"
                               ng-class="isExtraBet(team.id, extrasType, team.division) ? 'grey lighten-1' : 'grey lighten-3'">
                                <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                            </div>
                        </div>
                        <div class="extra-bets-content">
                            <span class="tooltip" 
                                  ng-repeat="icon in listAllExtraBets | extraBetsFilter:extrasType+'_North' | filter:{id_team:team.id}:true">
                                <i style="color: {{icon.color}}; padding:2px;"
                                   ng-class="icon.icon"></i>
                                <span class="tooltiptext">{{icon.name}}</span>
                            </span>
                            
                        </div>
                    </div>
                </span>
                <span class="col s12 noPadding" ng-if="nowTimestamp < season_start">
                    <a class="center-align waves-effect waves-light btn-large extras-button noPadding"
                       ng-class="isExtraBet(team.id, extrasType, team.division) ? 'grey lighten-1' : 'grey lighten-3'"
                       style="margin:10px;"
                       href=""
                       ng-repeat="team in listTeams | filter:{conference:extrasType}:true | filter:{division:'North'}:true | orderBy:['name']"
                       ng-click="saveExtraBet(team.id, extrasType, team.division)">
                        <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                    </a>
                </span>
            </div>
        </div>
        <p class="col s12 divider">&nbsp;</p>
        <div class="col l6 s12"
             style="border-right: 1px solid lightgrey">
            <p class="center-align"><b>{{extrasType}} South</b></p>
            <div class="center-align noPadding">
                <span class="col s12 noPadding" ng-if="nowTimestamp >= season_start">
                    <div class="extras-bets noPadding shadowed left-align"
                         style="margin:5px;"
                         ng-repeat="team in listTeams | filter:{conference:extrasType}:true | filter:{division:'South'}:true | orderBy:['name']">
                        <div>
                            <div class="left waves-effect waves-light btn-large extras-button noPadding noLink"
                               ng-class="isExtraBet(team.id, extrasType, team.division) ? 'grey lighten-1' : 'grey lighten-3'">
                                <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                            </div>
                        </div>
                        <div class="extra-bets-content">
                            <span class="tooltip" 
                                  ng-repeat="icon in listAllExtraBets | extraBetsFilter:extrasType+'_South' | filter:{id_team:team.id}:true">
                                <i style="color: {{icon.color}}; padding:2px;"
                                   ng-class="icon.icon"></i>
                                <span class="tooltiptext">{{icon.name}}</span>
                            </span>
                            
                        </div>
                    </div>
                </span>
                <span class="col s12 noPadding" ng-if="nowTimestamp < season_start">
                    <a class="center-align waves-effect waves-light btn-large extras-button noPadding"
                       ng-class="isExtraBet(team.id, extrasType, team.division) ? 'grey lighten-1' : 'grey lighten-3'"
                       style="margin:10px;"
                       href=""
                       ng-repeat="team in listTeams | filter:{conference:extrasType}:true | filter:{division:'South'}:true | orderBy:['name']"
                       ng-click="saveExtraBet(team.id, extrasType, team.division)">
                        <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                    </a>
                </span>

            </div>
        </div>
        <div class="col l6 s12">
            <p class="center-align"><b>{{extrasType}} West</b></p>
            <div class="center-align noPadding">
                <span class="col s12 noPadding" ng-if="nowTimestamp >= season_start">
                    <div class="extras-bets noPadding shadowed left-align"
                         style="margin:5px;"
                         ng-repeat="team in listTeams | filter:{conference:extrasType}:true | filter:{division:'West'}:true | orderBy:['name']">
                        <div>
                            <div class="left waves-effect waves-light btn-large extras-button noPadding noLink"
                               ng-class="isExtraBet(team.id, extrasType, team.division) ? 'grey lighten-1' : 'grey lighten-3'">
                                <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                            </div>
                        </div>
                        <div class="extra-bets-content">
                            <span class="tooltip" 
                                  ng-repeat="icon in listAllExtraBets | extraBetsFilter:extrasType+'_West' | filter:{id_team:team.id}:true">
                                <i style="color: {{icon.color}}; padding:2px;"
                                   ng-class="icon.icon"></i>
                                <span class="tooltiptext">{{icon.name}}</span>
                            </span>
                            
                        </div>
                    </div>
                </span>
                <span class="col s12 noPadding" ng-if="nowTimestamp < season_start">
                    <a class="center-align waves-effect waves-light btn-large extras-button noPadding"
                       ng-class="isExtraBet(team.id, extrasType, team.division) ? 'grey lighten-1' : 'grey lighten-3'"
                       style="margin:10px;"
                       href=""
                       ng-repeat="team in listTeams | filter:{conference:extrasType}:true | filter:{division:'West'}:true | orderBy:['name']"
                       ng-click="saveExtraBet(team.id, extrasType, team.division)">
                        <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                    </a>
                </span>
            </div>
        </div>

        <p class="col s12 divider">&nbsp;</p>
        
        <div class="col s12 m10 offset-m1">
            <p class="center-align"><b>{{extrasType}} Wild Card</b></p>
            <div class="center-align noPadding">
                <span class="col s12 noPadding" ng-if="nowTimestamp >= season_start">
                    <div class="extras-bets noPadding shadowed left-align"
                         style="margin:5px;"
                         ng-repeat="team in listTeams | filter:{conference:extrasType}:true | orderBy:['name']"
                         ng-if="team.show_wc">
                        <div>
                            <div class="left waves-effect waves-light btn-large extras-button noPadding noLink"
                               ng-class="isExtraBet(team.id, extrasType, 'wildcard') ? 'grey lighten-1' : 'grey lighten-3'">
                                <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                            </div>
                        </div>
                        <div class="extra-bets-content">
                            <span class="tooltip" 
                                  ng-repeat="icon in listAllExtraBets | extraBetsFilter:extrasType+'_wildcard' | filter:{id_team:team.id}:true">
                                <i style="color: {{icon.color}}; padding:2px;"
                                   ng-class="icon.icon"></i>
                                <span class="tooltiptext">{{icon.name}}</span>
                            </span>
                            
                        </div>
                    </div>
                </span>
                <span class="col s12 noPadding" ng-if="nowTimestamp < season_start">
                    <a class="center-align waves-effect waves-light btn-large extras-button noPadding"
                       ng-class="isExtraBet(team.id, extrasType, 'wildcard') ? 'grey lighten-1' : 'grey lighten-3'"
                       style="margin:10px;"
                       href=""
                       ng-repeat="team in listTeams | filter:{conference:extrasType}:true | orderBy:['name']"
                       ng-click="saveExtraBet(team.id, extrasType, 'wildcard')">
                        <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                    </a>

                </span>
            </div>
        </div>
            
        <p class="col s12 divider">&nbsp;</p>        
        <div class="col s12 m10 offset-m1">
            <p class="center-align"><b>{{extrasType}} Champion</b></p>
            <div class="center-align noPadding">
                <span class="col s12 noPadding" ng-if="nowTimestamp >= season_start">
                    <div class="extras-bets noPadding shadowed left-align"
                         style="margin:5px;"
                         ng-repeat="team in listTeams | filter:{conference:extrasType}:true | orderBy:['name']"
                         ng-if="team.show_conf">
                        <div>
                            <div class="left waves-effect waves-light btn-large extras-button noPadding noLink"
                               ng-class="isExtraBet(team.id, extrasType) ? 'grey lighten-1' : 'grey lighten-3'">
                                <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                            </div>
                        </div>
                        <div class="extra-bets-content">
                            <span class="tooltip" 
                                  ng-repeat="icon in listAllExtraBets | extraBetsFilter:extrasType | filter:{id_team:team.id}:true">
                                <i style="color: {{icon.color}}; padding:2px;"
                                   ng-class="icon.icon"></i>
                                <span class="tooltiptext">{{icon.name}}</span>
                            </span>
                            
                        </div>
                    </div>
                </span>
                <span class="col s12 noPadding" ng-if="nowTimestamp < season_start">
                    <a class="center-align waves-effect waves-light btn-large extras-button noPadding"
                       ng-class="isExtraBet(team.id, extrasType) ? 'grey lighten-1' : 'grey lighten-3'"
                       style="margin:10px"
                       href=""
                       ng-repeat="team in listTeams | filter:{conference:extrasType}:true | orderBy:['name']"
                       ng-click="saveExtraBet(team.id, extrasType)">
                        <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                    </a>
                </span>
            </div>
        </div>
    </span>
    <span ng-show="extrasType == 'Superbowl'">
        <div class="col s12 m10 offset-m1">
            <p class="center-align"><b>{{extrasType}}</b></p>
            <div class="center-align noPadding">
                <span class="col s12 noPadding" ng-if="nowTimestamp >= season_start">
                    <div class="extras-bets noPadding shadowed left-align"
                         style="margin:5px;"
                         ng-repeat="team in listTeams | orderBy:['name']"
                         ng-if="team.show_sb">
                        <div>
                            <div class="left waves-effect waves-light btn-large extras-button noPadding noLink"
                               ng-class="isExtraBet(team.id, extrasType) ? 'grey lighten-1' : 'grey lighten-3'">
                                <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                            </div>
                        </div>
                        <div class="extra-bets-content">
                            <span class="tooltip" 
                                  ng-repeat="icon in listAllExtraBets | extraBetsFilter:extrasType | filter:{id_team:team.id}:true">
                                <i style="color: {{icon.color}}; padding:2px;"
                                   ng-class="icon.icon"></i>
                                <span class="tooltiptext">{{icon.name}}</span>
                            </span>
                            
                        </div>
                    </div>
                </span>
                <span class="col s12 noPadding" ng-if="nowTimestamp < season_start">
                    <a class="center-align waves-effect waves-light btn-large extras-button noPadding"
                       style="margin:10px"
                       ng-class="isExtraBet(team.id, extrasType) ? 'grey lighten-1' : 'grey lighten-3'"
                       href=""
                       ng-repeat="team in listTeams | orderBy:['name']"
                       ng-click="saveExtraBet(team.id, extrasType)">
                        <img ng-src="img/nfl_logos/{{team.alias}}.gif" class="img-valign nfl-logo">
                    </a>

                </span>

                
                
            </div>
        </div>
    </span>
    <p class="col s12">&nbsp;</p>
</div>

<div class="center-align col s12"
     ng-show="loggedUser.length == 0 && nowTimestamp < season_start">
    <h5>
        Você precisa estar logado para apostar.
    </h5>
</div>