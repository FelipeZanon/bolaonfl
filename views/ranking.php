<div id="ranking" class="shadowed">
    <div class="row noMargin">
        <div class="col s12 noPadding">
            <ul id="tabsRanking" 
                class="tabs ranking-selector-header">
                <li class="tab col s6 nfl-dark-blue">
                    <a href="" 
                       ng-class="{'activeRound' : rankingType == 'general'}"
                       ng-click="rankingType = 'general'">
                        Geral
                    </a>
                </li>
                <li class="tab col s6 nfl-dark-blue">
                    <a href="" 
                       ng-class="{'activeRound' : rankingType == 'weekly'}"
                       ng-click="rankingType = 'weekly'">
                        Semanal
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div ng-show="rankingType === 'weekly'"
        class="col s12 noMargin noPadding">
        <ul id="tabsBets" 
            class="tabs">
            <?php require("weekSelector.php"); ?>
        </ul>
    </div>

    <table class="striped centered">
        <thead>
            <tr class="valign-wrapper ranking-header">
                <th class="col s1"></th>
                <th class="col s1"></th>
                <th class="col s3" id="sidebarText">
                    Nome
                </th>
                <th id="sidebarText" class="tooltip tooltip-mobile col s2">
                    Pts
                    <span class="tooltiptext tooltipBottomLeft">Pontos</span>
                </th>
                <th id="sidebarText" class="green-text tooltip tooltip-mobile col s1">
                    M
                    <span class="tooltiptext tooltipBottomLeft">Acertos na Mosca</span>
                </th>
                <th id="sidebarText" class="blue-text tooltip tooltip-mobile col s1">
                    V
                    <span class="tooltiptext tooltipBottomLeft">Vencedores Corretos</span>
                </th>
                <th id="sidebarText" class="tooltip tooltip-mobile col s2">
                    %
                    <span class="tooltiptext tooltipBottomLeft">Aproveitamento de Pontos</span>
                </th>
                <th id="sidebarText" class="purple-text tooltip tooltip-mobile col s1">
                    PE
                    <span class="tooltiptext tooltipBottomLeft">Pontos Extras</span>
                </th>
            </tr>
        </thead>
    </table>
    <div class="ranking-content">
        <table class="striped centered" ng-show="rankingType == 'general' && !loadingRanking">
            <tbody>
                <tr>
                    <td class="input-field col s12 ranking-search">
                        <input id="rankingSearch" 
                               class="center-align"
                               ng-model="searchText"
                               placeholder="Busque um participante">
                    </td>
                </tr>
<!--                General Ranking-->
                <tr class="ranking-row"
                    ng-repeat="user in listRanking | filter:searchText | orderBy:['position']"
                    ng-show="user.id">
                    <td id="sidebarText"
                        class="col s1"
                        ng-class="user.id == loggedUser[0].id ? 'active-user' : (user.online ? 'online-user' : '')">
                        {{user.position}}
                    </td>
                    <th class="col s1 center-align">
                        <i class="{{user.icon}}"
                           style="color: {{user.color}}"></i>
                    </th>

                    <td class="col s3 truncate" id="sidebarText">{{user.name}}</td>
                    <td class="col s2" id="sidebarText">{{user.points_with_extras}}</td>
                    <td class="col s1" id="sidebarText">{{user.tens}}</td>
                    <td class="col s1" id="sidebarText">{{user.fives}}</td>
                    <td class="col s2" id="sidebarText">{{user.percentage | number : 1}}</td>
                    <td class="col s1" id="sidebarText">{{user.extra_points}}</td>
                </tr>
            </tbody>
        </table>
        
        <table class="striped centered" ng-show="rankingType == 'weekly' && !loadingRanking">
            <tbody>
                <tr>
                    <td class="input-field col s12 ranking-search">
                        <input id="rankingSearch" 
                               class="center-align"
                               ng-model="searchText"
                               placeholder="Busque um participante">
                    </td>
                </tr>
<!--                Weekly Ranking-->
                <tr class="ranking-row"
                    ng-repeat="user in listWeeklyRanking | filter:searchText | orderBy:['position']"
                    ng-show="user.id">
                    <td id="sidebarText"
                        class="col s1"
                        ng-class="user.id == loggedUser[0].id ? 'active-user' : (user.online ? 'online-user' : '')">
                        {{user.position}}
                    </td>
                    <th class="col s1 center-align">
                        <i class="{{user.icon}}"
                           style="color: {{user.color}}"></i>
                    </th>
                    
                    <td class="col s3 truncate" id="sidebarText">{{user.name}}</td>
                    <td class="col s2" id="sidebarText">{{user.points}}</td>
                    <td class="col s1" id="sidebarText">{{user.tens}}</td>
                    <td class="col s1" id="sidebarText">{{user.fives}}</td>
                    <td class="col s2" id="sidebarText">{{user.percentage | number : 1}}</td>
                    <td class="col s1" id="sidebarText">&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <div ng-show="loadingRanking">
            <br><br>
            <p class="center-align"><img src="img/loading.gif" width=60px></p>
            <br><br>
        </div>
    </div>
</div>