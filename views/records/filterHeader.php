<div class="col s12">
    <div class="col s2 noPadding">
        Resultados
    </div>
    <div class="col s4">
        Usuário
    </div>
    <div class="col s3">
        Temporada
    </div>
    <div class="col s3 noPadding" ng-show="selectedTypeRecords == 1">
        Semana
    </div>
    <div class="col s3 noPadding purple-text tooltip" ng-show="selectedTypeRecords == 2">
        PE
        <span class="tooltiptext tooltipTopLeft">Pontos Extras</span>
    </div>

</div>
<form class="col s12" action="">
    <div class="col s2 noPadding input-field">
        <input id="results" 
               class="center-align" 
               type="number" 
               ng-model="numberOfResults">
    </div>
    <div class="col s4 input-field">
        <input id="rankingSearch" 
               class="center-align"
               ng-model="searchTextRecords">
    </div>
    <div class="input-field col s3">
        <select class="browser-default" ng-model="searchSeason">
            <option value="" selected>Todas</option>
            <option ng-repeat="season in seasons" value="{{season.text}}">{{season.text}}</option>
        </select>
    </div>
    <div class="input-field col s3 noPadding" ng-show="selectedTypeRecords == 1">
        <select class="browser-default" ng-model="searchWeek">
            <option value="" selected>Todas</option>
            <option ng-repeat="week in weeks" value="{{week.number}}" ng-show="week.number <= 17">{{week.text}}</option>
        </select>
    </div>
    <div class="input-field col s3 noPadding" ng-show="selectedTypeRecords == 2">
        <p>
            <label>
                <input type="checkbox" class="filled-in" ng-model="searchPE"/>
                <span>Extras?</span>
            </label>
        </p>
    </div>
</form>
<p class="col s12 divider">&nbsp;</p>
<div class="col s12"
     style="margin: 10px 0 10px 0">
    <div class="switch">
        <label>
            Melhores
            <input type="checkbox" ng-model="searchOrder">
            <span class="lever"></span>
            Piores
        </label>
    </div>
</div>
