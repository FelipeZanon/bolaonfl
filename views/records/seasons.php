<div class="col s12">
    <div class="col s12 noPadding" style="margin: 20px 0;">
        <a class="waves-effect waves-light btn"
           ng-class="$parent.selectedSeason == season.id ? 'teal white-text' : 'white black-text'"
           style="margin: 5px;"
           ng-click="$parent.selectedSeason = season.id"
           ng-repeat="season in seasons">{{season.text}}</a>
    </div>
    <div class="col s12 noPadding"
         style="margin-top:20px;">
        <table class="striped centered">
            <thead class="table-text">
                <tr>
                    <th class="col s1 pointer" ng-click="rankingOrder = defaultRankingOrder">#</th>
                    <th class="col s1">&nbsp;</th>
                    <th class="col s3">Nome</th>
                    <th class="col s2 noPadding tooltip pointer" ng-click="rankingOrder = 'percentage'">
                        %
                        <span class="tooltiptext">Aproveitamento de Pontos</span>
                    </th>
                    <th class="col s2 noPadding tooltip pointer" ng-click="rankingOrder = defaultRankingOrder">
                        Pts
                        <span class="tooltiptext">Pontos</span>
                    </th>
                    <th class="col s1 green-text tooltip pointer" ng-click="rankingOrder = 'tens'">
                        M
                        <span class="tooltiptext">Acertos na Mosca</span>
                    </th>
                    <th class="col s1 blue-text tooltip pointer" ng-click="rankingOrder = 'fives'">
                        V
                        <span class="tooltiptext">Vencedores Corretos</span>
                    </th>
                    <th class="col s1 purple-text tooltip pointer" ng-click="rankingOrder = 'extra_points'">
                        PE
                        <span class="tooltiptext topLeft">Pontos Extras</span>
                    </th>
                </tr>
            </thead>

            <tbody class="table-text">
                <tr style="line-height: 52px;"
                    class="noPadding noMargin"
                    ng-repeat="userScore in seasonsRankings | filter:{season_id: selectedSeason}:true | orderBy:rankingOrder:true">
                    <td class="noPadding noMargin col s1">{{userScore.position}}</td>
                    <td class="noPadding noMargin col s1">
                        <i class="{{userScore.icon}}"
                           style="color: {{userScore.color}}"></i>
                    </td>
                    <td class="noPadding noMargin col s3">{{userScore.name}}</td>
                    <td class="noPadding noMargin col s2">{{userScore.percentage | number:2}}</td>
                    <td class="noPadding noMargin col s2">{{userScore.points_with_extras}}</td>
                    <td class="noPadding noMargin col s1">{{userScore.tens}}</td>
                    <td class="noPadding noMargin col s1">{{userScore.fives}}</td>
                    <td class="noPadding noMargin col s1">{{userScore.extra_points}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<p class="col s12 divider">&nbsp;</p>
<div class="col s12 left-align"
     style="margin-left: 10px;">
    <p ng-show="selectedSeason == 1 || selectedSeason == 2">* Esta temporada <b>não</b> possuía apostas extras.</p>
    <p ng-show="selectedSeason == 1 || selectedSeason == 2">** Esta temporada <b>não</b> possuía pontuação progressiva nos playoffs. A pontuação final mostrada nessa seção leva em conta o padrão <b>atual</b> de pontos, ou seja, o ranking final disposto aqui pode ser diferente do ranking final da temporada.</p>
</div>
