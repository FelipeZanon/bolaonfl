<div class="row" style="margin:10px 0 0 0;">
    <div class="col s12 noPadding noMargin">
        <div class="col s6">
            <a class="waves-effect waves-light btn-small"
               ng-class="selectedTypeRecords == 1 ? 'teal white-text' : 'white black-text'"
               ng-click="selectedTypeRecords = 1">Semanas</a>
        </div>
        <div class="col s6">
            <a class="waves-effect waves-light btn-small"
               ng-class="selectedTypeRecords == 2 ? 'teal white-text' : 'white black-text'"
               ng-click="selectedTypeRecords = 2">Temporadas</a>
        </div>
        <p class="col s12 divider">&nbsp;</p>
        <?php require("records/filterHeader.php"); ?>
        <p class="col s12 divider">&nbsp;</p>
        
        <span ng-show="selectedTypeRecords == 1">
            <table class="striped centered">
                <thead class="table-text">
                    <tr>
                        <th class="col s1 noPadding">&nbsp;</th>
                        <th class="col s1 noPadding">&nbsp;</th>
                        <th class="col s2 noPadding">Nome</th>
                        <th class="col s2 noPadding hide-on-med-and-down">Temporada</th>
                        <th class="col s2 noPadding hide-on-large-only tooltip tooltip-mobile">
                            T
                            <span class="tooltiptext tooltipTop">Temporada</span>
                        </th>
                        <th class="col s1 noPadding tooltip tooltip-mobile">
                            S
                            <span class="tooltiptext tooltipTop">Semana</span>
                        </th>
                        <th class="col s1 noPadding tooltip tooltip-mobile">
                            J
                            <span class="tooltiptext tooltipTop">Jogos (Apostas)</span>
                        </th>
                        <th class="col s1 noPadding tooltip tooltip-mobile">
                            %
                            <span class="tooltiptext tooltipTop">Aproveitamento de Pontos</span>
                        </th>
                        <th class="col s1 noPadding tooltip tooltip-mobile">
                            Pts
                            <span class="tooltiptext tooltipTop">Pontos</span>
                        </th>
                        <th class="col s1 noPadding green-text tooltip tooltip-mobile">
                            M
                            <span class="tooltiptext tooltipTop">Acertos na Mosca</span>
                        </th>
                        <th class="col s1 noPadding blue-text tooltip tooltip-mobile">
                            V
                            <span class="tooltiptext tooltipTopLeft">Vencedores Corretos</span>
                        </th>
                    </tr>
                </thead>

                <tbody class="table-text">
                    <tr style="line-height: 52px;"
                        class="noPadding noMargin"
                        ng-repeat="userScore in weeklyRankings | 
                                   orderBy:['percentage', 'tens', 'fives']:!searchOrder | 
                                   filter:{name: searchTextRecords}:false | 
                                   filter:{season: searchSeason}:myComparator | 
                                   filter:{week_string: searchWeek}:myComparator | 
                                   startFrom:0 | 
                                   objLimitTo:numberOfResults 
                                   track by $index">
                        <td class="col s1 noPadding">{{$index + 1}}</td>
                        <td class="col s1 noPadding">
                            <i class="{{userScore.icon}}"
                               style="color: {{userScore.color}}"></i>
                        </td>
                        <td class="col s2 noPadding">{{userScore.name}}</td>
                        <td class="col s2 noPadding">{{userScore.season_mini}}</td>
                        <td class="col s1 noPadding">{{userScore.week}} {{searchWeekNumber}}</td>
                        <td class="col s1 noPadding hide-on-med-and-down">{{userScore.matches}} ({{userScore.bets_number}})</td>
                        <td class="col s1 noPadding hide-on-large-only">{{userScore.matches}}</td>
                        <td class="col s1 noPadding">{{userScore.percentage | number:2}}</td>
                        <td class="col s1 noPadding">{{userScore.points}}</td>
                        <td class="col s1 noPadding">{{userScore.tens}}</td>
                        <td class="col s1 noPadding">{{userScore.fives}}</td>
                    </tr>
                </tbody>
            </table>
        </span>
        <span ng-show="selectedTypeRecords == 2">
            <table class="striped centered">
                <thead class="table-text">
                    <tr>
                        <th class="col s1 noPadding">&nbsp;</th>
                        <th class="col s1 noPadding">&nbsp;</th>
                        <th class="col s2 noPadding">Nome</th>
                        <th class="col s2 noPadding">Temporada</th>
                        <th class="col s1 noPadding tooltip">
                            PF
                            <span class="tooltiptext tooltipTop">Posição Final</span>
                        </th>
                        <th class="col s1 noPadding tooltip">
                            %
                            <span class="tooltiptext tooltipTop">Aproveitamento de Pontos</span>
                        </th>
                        <th class="col s1 noPadding tooltip">
                            Pts
                            <span class="tooltiptext tooltipTop">Pontos</span>
                        </th>
                        <th class="col s1 noPadding green-text tooltip">
                            M
                            <span class="tooltiptext tooltipTop">Acertos na Mosca</span>
                        </th>
                        <th class="col s1 noPadding blue-text tooltip">
                            V
                            <span class="tooltiptext tooltipTopLeft">Vencedores Corretos</span>
                        </th>
                        <th class="col s1 purple-text tooltip">
                            PE
                            <span class="tooltiptext tooltipTopLeft">Pontos Extras</span>
                        </th>

                    </tr>
                </thead>

                <tbody class="table-text">
                    <tr style="line-height: 52px;"
                        class="noPadding noMargin"
                        ng-repeat="userScore in seasonsRankings | 
                                   orderBy:[orderByPoints, 'tens', 'fives']:!searchOrder | 
                                   filter:{name: searchTextRecords}:false | 
                                   filter:{season: searchSeason}:myComparator | 
                                   startFrom:0 | 
                                   objLimitTo:numberOfResults 
                                   track by $index">
                        <td class="col s1 noPadding">{{$index + 1}}</td>
                        <td class="col s1 noPadding">
                            <i class="{{userScore.icon}}"
                               style="color: {{userScore.color}}"></i>
                        </td>
                        <td class="col s2 noPadding">{{userScore.name}}</td>
                        <td class="col s2 noPadding">{{userScore.season_mini}}</td>
                        <td class="col s1 noPadding">{{userScore.position}}</td>
                        <td class="col s1 noPadding">{{userScore.percentage | number:2}}</td>
                        <td class="col s1 noPadding" ng-show="!searchPE">{{userScore.points}}</td>
                        <td class="col s1 noPadding" ng-show="searchPE">{{userScore.points_with_extras}}</td>
                        <td class="col s1 noPadding">{{userScore.tens}}</td>
                        <td class="col s1 noPadding">{{userScore.fives}}</td>
                        <td class="col s1 noPadding">{{userScore.extra_points}}</td>
                    </tr>
                </tbody>
            </table>            
        </span>
    </div>
</div>