<div class="col s12 noMargin noPadding">
    <ul id="tabsRecords" 
        class="tabs">
        <?php require("weekSelector.php"); ?>
    </ul>
</div>

<div class="col s12 noPadding" 
     style="margin-top:20px;">
    <div class="col s12 noPadding noMargin">
        <table class="striped centered">
            <thead class="table-text">
                <tr>
                    <th class="col s1 noPadding">&nbsp;</th>
                    <th class="col s1 noPadding">&nbsp;</th>
                    <th class="col s3 noPadding">Nome</th>
                    <th class="col s3 noPadding">Temporada</th>                    
                    <th class="col s1 noPadding tooltip">
                        %
                        <span class="tooltiptext">Aproveitamento de Pontos</span>
                    </th>
                    <th class="col s1 noPadding tooltip">
                        Pts
                        <span class="tooltiptext">Pontos</span>
                    </th>
                    <th class="col s1 noPadding green-text tooltip">
                        M
                        <span class="tooltiptext">Acertos na Mosca</span>
                    </th>
                    <th class="col s1 noPadding blue-text tooltip">
                        V
                        <span class="tooltiptext topLeft">Vencedores Corretos</span>
                    </th>
                </tr>
            </thead>

            <tbody class="table-text">
                <tr style="line-height: 52px;"
                    class="noPadding noMargin"
                    ng-repeat="userScore in checkpointRankings | filter:{week: selectedRound}:true | orderBy:['accumulated_percentage', 'accumulated_tens', 'accumulated_fives']:true | startFrom:0 | objLimitTo:10 track by $index">
                    <td class="col s1 noPadding">{{$index + 1}}</td>
                    <td class="col s1 noPadding">
                        <i class="{{userScore.icon}}"
                           style="color: {{userScore.color}}"></i>
                    </td>
                    <td class="col s3 noPadding">{{userScore.name}}</td>
                    <td class="col s3 noPadding">{{userScore.season_mini}}</td>                    
                    <td class="col s1 noPadding">{{userScore.accumulated_percentage | number:2}}</td>
                    <td class="col s1 noPadding">{{userScore.accumulated_points}}</td>
                    <td class="col s1 noPadding">{{userScore.accumulated_tens}}</td>
                    <td class="col s1 noPadding">{{userScore.accumulated_fives}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>