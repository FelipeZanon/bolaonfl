<div class="row noPadding noMargin">
    <div class="col s12 noPadding noMargin">
        <ul id="tabsStats" class="tabs tabs-fixed-width" style="background-image: url(img/menu_bg_inner.jpg);">
            <li class="tab" ng-click="selectedStats = 1"><a ng-class="{'activeStats' : selectedStats == 1}" href="">
                <span ng-show="clientLanguage == 'pt-BR'">Ataques/Defesas</span>
                <span ng-show="clientLanguage != 'pt-BR'">Offences/Defences</span>
            </a></li>
            <li class="tab" ng-click="selectedStats = 2"><a ng-class="{'activeStats' : selectedStats == 2}" href="">
                <span ng-show="clientLanguage == 'pt-BR'">Artilharia</span>
                <span ng-show="clientLanguage != 'pt-BR'">Top Scorers</span>
            </a></li>
        </ul>
    </div>
</div>
<div ng-show="loadingMatches">
   	<br><br>
	<p class="center-align"><img src="img/loading.gif" width=100px></p>
</div>
<div ng-show="!loadingMatches">
    <div id="hotsiteInsideContent" class="col s12 hotsiteInsideContent noPadding" ng-show="selectedStats == 1">
        <?php require("offdef.php"); ?>
    </div>
    <div id="hotsiteInsideContent" class="col s12 hotsiteInsideContent noPadding" ng-show="selectedStats == 2">
        <?php require("scorers.php"); ?>
    </div>
</div>