<div ng-show="internalAdminMenu == 'teams'" class="col l11 s12 scrollableInside">
	<div class="row" ng-repeat="team in listTeams" style="margin:0; border-bottom: 1px solid black;">
		<div class="col l2">
			<p>
				<input type="checkbox" id="removeTeam{{$index}}" class="filled-in" ng-model="team.remove">
				<label for="removeTeam{{$index}}" class="removeCheckbox">Remover?</label>
			</p>
		</div>
		<div class="col l3">
			<p><img src="img/flags_min/{{team.prefix_fifa | lowercase}}.png" height="20px" style="vertical-align:middle">&nbsp;{{team.prefix}}</p>
		</div>
		<div class="col l7">
			<p>{{team.name}}</p>
		</div>
	</div>
</div>

<div ng-show="internalAdminMenu == 'players'" class="col l11 s12 scrollableInside">
	<div class="row" ng-repeat="player in listPlayers" style="margin:0; border-bottom: 1px solid black;">
		<div class="col l2">
			<p>
				<input type="checkbox" id="removePlayer{{$index}}" class="filled-in" ng-model="player.remove">
				<label for="removePlayer{{$index}}" class="removeCheckbox">Remover?</label>
			</p>
		</div>
		<div class="col l3">
			<p>{{player.name}}</p>
		</div>
		<div class="col l2">
			<p>{{player.number}}</p>
		</div>
		<div class="col l2">
			<p>{{player.position}}</p>
		</div>
		<div class="col l3">
			<p>{{player.team}}</p>
		</div>
	</div>
</div>

<div ng-show="internalAdminMenu == 'stadiums'" class="col l11 s12 scrollableInside">
	<div class="row" ng-repeat="stadium in listStadiums" style="margin:0; border-bottom: 1px solid black;">
		<div class="col l2">
			<p>
				<input type="checkbox" id="removeStadium{{$index}}" class="filled-in" ng-model="stadium.remove">
				<label for="removeStadium{{$index}}" class="removeCheckbox">Remover?</label>
			</p>
		</div>
		<div class="col l4">
			<p><img src="img/stadiums/{{stadium.name | lowercase}}.png" height="20px" style="vertical-align:middle">&nbsp;{{stadium.name}}</p>
		</div>
		<div class="col l2">
			<p>{{stadium.capacity}}</p>
		</div>
		<div class="col l2">
			<p>{{stadium.capacity}}</p>
		</div>
		<div class="col l1">
			<p>{{stadium.geo_latitude}}</p>
		</div>
		<div class="col l1">
			<p>{{stadium.geo_longitude}}</p>
		</div>
	</div>
</div>

<div ng-show="internalAdminMenu == 'matches'" class="col l11 s12 scrollableInside">
	<div class="row" ng-repeat="match in listMatches" style="margin:0; border-bottom: 1px solid black;">
		<div class="col s2">
			<p>
				<input type="checkbox" id="removeMatch{{$index}}" class="filled-in" ng-model="match.remove">
				<label for="removeMatch{{$index}}" class="removeCheckbox">Remover?</label>
			</p>
		</div>
		<div class="col l2">
			<!--{{singleGame.gamedate | date : "EEE, dd'/'MM"}}<br><b>{{singleGame.gamedate | date : "HH'h'mm"}}-->
			<p>{{match.date | date : "dd MMM - HH'h'mm"}}</p>
		</div>
		<div class="col l2">
			<p><img src="img/flags_min/{{match.prefix_fifa_home | lowercase}}.png" height="20px" style="vertical-align:middle">&nbsp;{{match.team_home}}</p>
		</div>
		<div class="col l1">
			<p>{{match.goals_home}} x {{match.goals_visitor}}</p>
		</div>			

		<div class="col l2">
			<p style="text-align: right">{{match.team_visitor}}&nbsp;<img src="img/flags_min/{{match.prefix_fifa_visitor | lowercase}}.png" height="20px" style="vertical-align:middle">&nbsp;</p>
		</div>
		<div class="col l3">
			<p style="text-align: right">{{match.stadium}}&nbsp;<img src="img/stadiums/{{match.stadium | lowercase}}.png" height="20px" style="vertical-align:middle"></p>
		</div>
	</div>
</div>


<div class="col l1 right hide-on-med-and-down commandMenu">
	<a id="tooltipRemove" class="tooltipped" href="#" ng-click="deleteInfo()" data-position="top" data-delay="50" data-tooltip="Clique para ZERAR TUDO"><i class="large material-icons red-text text-accent-4">delete_forever</i></a>
</div>
