<div class="page-header records-header col s12 teal-text">
    Records <i class="icon-class fas fa-trophy"></i>
</div>
<div class="row noPadding noMargin">
    <div class="col s12 noPadding noMargin">
        <ul id="tabsRecords" class="tabs tabs-fixed-width">
            <li class="tab" ng-click="selectedRecords = 1">
                <a ng-class="{'activeRecords' : selectedRecords == 1}" href="">Records</a>
            </li>
            <li class="tab" ng-click="selectedRecords = 3">
                <a ng-class="{'activeRecords' : selectedRecords == 3}" href="">Checkpoints</a>
            </li>
            <li class="tab" ng-click="selectedRecords = 4">
                <a ng-class="{'activeRecords' : selectedRecords == 4}" href="">Temporadas</a>
            </li>
        </ul>
    </div>
</div>

<div ng-show="!loadingRecords">
    <div id="content" class="col s12 noPadding" ng-show="selectedRecords == 1">
        <?php require("records/records.php"); ?>
    </div>
    <div id="content" class="col s12 noPadding" ng-show="selectedRecords == 3">
        <?php require("records/checkpoints.php"); ?>
    </div>
    <div id="content" class="col s12 noPadding" ng-show="selectedRecords == 4">
        <?php require("records/seasons.php"); ?>
    </div>
</div>

<div ng-show="loadingRecords">
    <br><br>
    <p class="center-align"><img src="img/loading.gif" width=60px></p>
    <br><br>
</div>
