<div class="row" style="margin-top:10px;">
	<div id="pontuacao" class="col s12">
		<div style="margin-left:20px;">
			<b>Vitória fácil</b>: diferença de <span style="font-variant: small-caps;"><b>mais</b></span> de 7 pontos.<br>
			<b>Vitória difícil</b>: diferença de 7 pontos ou <span style="font-variant: small-caps;"><b>menos</b></span>.
		</div>
		<br>
		<table class="striped">
			<tbody>
				<tr>
					<th data-field="awayTeam" class="center"><img src="img/nfl_logos/Raiders.gif" style="width:45px;"></th>
					<th data-field="awayPoints" class="center">35</th>
					<th data-field="at" class="center">@</th>					
					<th data-field="homePoints" class="center">31</th>					
					<th data-field="homeTeam" class="center"><img src="img/nfl_logos/Buccaneers.gif" style="width:45px;"></th>
				</tr>
			</tbody>
		</table>
		Vitória difícil para os Raiders pois 35 - 31 = 4.<br><br><br>
		
		<table class="striped">
			<tbody>
				<tr>
					<th data-field="awayTeam" class="center"><img src="img/nfl_logos/Raiders.gif" style="width:45px;"></th>
					<th data-field="awayPoints" class="center">28</th>
					<th data-field="at" class="center">@</th>					
					<th data-field="homePoints" class="center">35</th>					
					<th data-field="homeTeam" class="center"><img src="img/nfl_logos/Buccaneers.gif" style="width:45px;"></th>
				</tr>
			</tbody>
		</table>
		Vitória difícil para os Buccaneers pois 35 - 28 = 7.<br><br><br>
		
		<table class="striped">
			<tbody>
				<tr>
					<th data-field="awayTeam" class="center"><img src="img/nfl_logos/Raiders.gif" style="width:45px;"></th>
					<th data-field="awayPoints" class="center">21</th>
					<th data-field="at" class="center">@</th>					
					<th data-field="homePoints" class="center">7</th>					
					<th data-field="homeTeam" class="center"><img src="img/nfl_logos/Buccaneers.gif" style="width:45px;"></th>
				</tr>
			</tbody>
		</table>
		Vitória fácil para os Raiders pois 21 - 7 = 14.<br><br>
		
		
		<div style="margin:20px 0 0 20px;">
			</span>Para marcar <span class="green-text" style="font-weight: bold;">10</span> pontos: acertar o <span style="font-variant: small-caps;"><b>vencedor</b></span> e também a <span style="font-variant: small-caps;"><b>margem</b></span> de vitória (acerto na mosca).<br>
			Para marcar <span class="blue-text" style="font-weight: bold;">05</span> pontos: acertar o <span style="font-variant: small-caps;"><b>vencedor</b></span>, porém errar a margem.<br>
			Caso haja <span style="font-variant: small-caps;"><b>empate</b></span>, todos que marcaram vit&oacute;ria <span style="font-variant: small-caps;"><b>difícil</b></span> recebem <span class="blue-text" style="font-weight: bold;">05</span> pontos.<br>
			Errar o vencedor resulta em <span class="red-text" style="font-weight: bold;">0</span> pontos ao apostador.<br><br>
			<span class="red-text" style="font-weight: bold;">ATENÇÃO!</span><br>As pontuações serão modificadas de acordo com a seguinte tabela:
		</div>
		
		<table class="striped">
			<tbody>
				<tr>
					<th data-field="awayPoints" class="center">Temporada regular</th>
					<th data-field="homePoints" class="center">
						<span class="green-text" style="font-weight: bold;">10</span> 
						| <span class="blue-text" style="font-weight: bold;">05</span>					
						| <span class="red-text" style="font-weight: bold;">00</span>
					</th>
				</tr>
				<tr>
					<th data-field="awayPoints" class="center">Wild Card e Divisional Round</th>
					<th data-field="homePoints" class="center">
						<span class="green-text" style="font-weight: bold;">20</span> 
						| <span class="blue-text" style="font-weight: bold;">10</span>					
						| <span class="red-text" style="font-weight: bold;">00</span>
					</th>
				</tr>
				<tr>
					<th data-field="awayPoints" class="center">Conference Championships</th>
					<th data-field="homePoints" class="center">
						<span class="green-text" style="font-weight: bold;">40</span> 
						| <span class="blue-text" style="font-weight: bold;">20</span>					
						| <span class="red-text" style="font-weight: bold;">00</span>
					</th>
				</tr>
				<tr>
					<th data-field="awayPoints" class="center">Superbowl</th>
					<th data-field="homePoints" class="center">
						<span class="green-text" style="font-weight: bold;">80</span> 
						| <span class="blue-text" style="font-weight: bold;">40</span>					
						| <span class="red-text" style="font-weight: bold;">00</span>
					</th>
				</tr>
			</tbody>
		</table>
	</div>
</div>