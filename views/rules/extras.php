<div class="row" style="margin-top:10px;">
	<div id="extras" class="col s12">
		<div>
			<a href="#/apostarExtra/" style="font-size:10px;" class="brown-text text-darken-3">
				<button class="waves-effect waves-light btn white orange-text text-darken-3"><b>Apostas Extras</b></button>
			</a>
			<div style="margin:20px;">
				Apostas específicas sobre o campeão de cada divisão, de cada conferência e o vencedor do Super Bowl. Essas apostas ficarão habilitadas até <b>13/09</b> às <b>13:00</b> (horário de Brasília).<br>
			</div>
			
			As pontuações para acerto das apostas extras são as seguintes:			
			<table class="striped centered">
				<tbody>
					<tr>
						<th data-field="awayTeam" class="center">Wild Card</th>
						<th data-field="at" class="center"><span class="purple-text">10</span> pontos</th>					
					</tr>
					<tr>
						<th data-field="awayTeam" class="center">Division Champion</th>
						<th data-field="at" class="center"><span class="purple-text">20</span> pontos</th>					
					</tr>
					<tr>
						<th data-field="awayTeam" class="center">Conference Champion</th>
						<th data-field="at" class="center"><span class="purple-text">50</span> pontos</th>					
					</tr>
					<tr>
						<th data-field="awayTeam" class="center">Super Bowl Winner</th>
						<th data-field="at" class="center"><span class="purple-text">100</span> pontos</th>					
					</tr>
				</tbody>
			</table>
			<div style="margin:10px;">
				Os pontos serão computados após cada categoria ser decidida.
			</div>
		</div>
	</div>
</div>