<div class="row" style="margin-top:10px;">
	<div id="inscricao" class="col s12">
		<b>A temporada 2020/21 não terá premiações, portanto não será cobrado valor de inscrição.</b><br>
		Se você quiser participar apenas pela diversão, contate um dos administradores:<br>
		<a href="mailto:sharpion.k@gmail.com?Subject=Bolão NFL" target="_top">Felipe</a><br>
		<a href="mailto:ngm.motta@gmail.com?Subject=Bolão NFL" target="_top">Motta</a><br>
<!--		O valor da inscrição é de <b>R$50,00</b>.<br>
		O valor deve ser pago <span style="font-variant: small-caps;"><b>antes</b></span> do início da temporada (05/09).<br>
		Avisem quando tiverem depositado!<br><br>
		
		<div class="col s6 m4 offset-m2 ">
			<b>Banco do Brasil</b><br>
			Agência: 0982-2<br>
			C/C: 47930-6<br>
			Felipe Zanon do Nascimento<br>
			CPF: 050.143.229-99
		</div>
		
        <div class="col s6 m4">
			<b>NuBank (Cód. 260)</b><br>
			Agência: 0001<br>
			C/C: 426087-4<br>
			Felipe Zanon do Nascimento<br>
			CPF: 050.143.229-99
		</div>
		
		<div class="col s12">
			<br>
			Também incluímos a possibilidade de pagar com o <b>PicPay</b>!<br>
			Basta usar o nome de usuário <b>@felipezn</b> e enviar o pagamento.
		</div>
		<div class="col s12">
			<span style="font-variant: small-caps;" class="center red-text"><br><b>NOVIDADE!</b></span><br>
			Os participantes que indicarem 5 novos inscritos no bolão para essa temporada ficarão isentos da taxa de inscrição!
		</div>
		<div class="col s12">
			
			<span style="font-variant: small-caps;"><br><b>Situação dos Pagamentos</b></span>
			<table class="striped centered">
				<thead>
				  <tr>
					  <th class="center"></th>
				  </tr>
				</thead>		
				<tbody>
					<tr>
						<th data-field="nome" class="center">A sua mãe</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Acauã</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">andrezits</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Bruno</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">cussolin</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Douglas</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Duplo Maravilha</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Fabian</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Felipe</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Jessi</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">joaodiehl</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">João Gaudeda</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Koreano</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Larissa</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Llama</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">LNRD</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Motta</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Nastari</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Phetor</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Renata</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Renan</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">RoPensO</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Sisamek</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Thigo</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Viking</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">Vinebas</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">viniciuslf</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
					<tr>
						<th data-field="nome" class="center">vuintrieri</th>
						<th data-field="checkPagamento" class="center green-text"><i class="material-icons">done</i></th>
					</tr>
				</tbody>
			</table>
		</div>
-->
	</div>
</div>