<div class="row" style="margin-top:10px;">
	<div id="apostas" class="col s12">
		<a href="#/apostar/" style="font-size:10px;" class="brown-text text-darken-3">
			<button class="waves-effect waves-light btn white nfl-blue-text"><b>Apostar</b></button>
		</a>
		<br /><br />

		<div style="margin-left:20px; text-align: center;">
			As apostas poderão ser feitas até o kickoff de cada jogo. O bloqueio será automático.<br><br>
			<b>Importante!<br></b>
			Caso alguém esteja sem acesso a internet ou não consiga acessar o site, aceitaremos apostas por <a href="mailto:sharpion.k@gmail.com?Subject=Apostas NFL" target="_top">e-mail</a>.<br>
			<span style="font-size:13px">Obs.: Se não entendermos suas apostas, elas serão ignoradas. <br>Seja como for, deixe organizado para entendermos e inserirmos corretamente.</span>
		</div>
	</div>
</div>