<li class="tab col s1">
    <a class="waves-effect waves-light btn noPadding" 
       style="background-color: white;" 
       ng-disabled="selectedRound == weeks[0].number" 
       ng-click="selectRound(selectedRound - 1)">
        <i class="material-icons" 
           style="line-height: 48px">arrow_back</i>
    </a>
</li>
<li class="tab col s3" ng-show="selectedRound == weeks[0].number">&nbsp;</li>
<li class="tab col s2" ng-show="selectedRound == weeks[1].number">&nbsp;</li>
<li class="tab col s1" ng-show="selectedRound == weeks[2].number">&nbsp;</li>

<li class="tab col"
    ng-class="$parent.selectedRound == week.number ? 's4' : 's1'"
    ng-repeat="week in weeks"
    ng-click="selectRound(week.number)"
    ng-show="week.number >= $parent.selectedRound - 3 && week.number <= $parent.selectedRound + 3">
    <!-- $parent is used here because ng-repeat creates a child scope for each loop, so we have to change the original (parent) variable -->
    <a ng-class="{'activeRound' : $parent.selectedRound == week.number}" 
       href="">
        <span ng-show="$parent.selectedRound == week.number"><b>{{week.text}}</b></span>
        <span ng-hide="$parent.selectedRound == week.number">{{week.number}}</span>
    </a>
</li>
<li class="tab col s3" ng-show="selectedRound == weeks[weeks.length - 1].number">&nbsp;</li>
<li class="tab col s2" ng-show="selectedRound == weeks[weeks.length - 2].number">&nbsp;</li>
<li class="tab col s1" ng-show="selectedRound == weeks[weeks.length - 3].number">&nbsp;</li>

<li class="tab col s1">
    <a class="waves-effect waves-light btn noPadding" 
       style="background-color: white;" 
       ng-disabled="selectedRound == weeks.length"
       ng-click="selectRound(selectedRound + 1)">
        <i class="material-icons" style="line-height: 48px">arrow_forward</i>
    </a>
</li>