<div id="modalLoginMobile" class="modal bottom-sheet">
    <span ng-show="loggedUser.length == 0">
        <div class="modal-content noPadding">
            <div class="row noMargin noPadding">
                <div class="col s12 noPadding">
                    <div class="col s6 center-align noPadding noMargin preferences-tab"
                        ng-class="loginTab == 1 ? 'active-tab' : ''"
                        ng-click="loginTab = 1">
                        <a class="btn-flat btn-large white-text"
                           style="width: 100%">
                            Login
                        </a>
                    </div>

                    <div class="col s6 center-align noPadding noMargin preferences-tab"
                        ng-class="loginTab == 2 ? 'active-tab' : ''"
                        ng-click="loginTab = 2">
                        <a class="btn-flat btn-large white-text"
                           style="width: 100%">
                            Cadastre-se
                        </a>
                    </div>

                </div>
                <div class="col s12"
                     ng-show="loginTab == 1"
                     style="padding:10px;">

                    <form class="col s12">
                        <div class="input-field col s12">
                            <input id="emailMobile" 
                                   type="text" 
                                   class="validate nfl-white"
                                   ng-model="emailLogin"
                                   ng-keyup="$event.keyCode == 13 && manualLogin()"
                                   Placeholder="E-mail"
                                   autocomplete="email"
                                   autofocus>
                        </div>
                        <div class="input-field col s12 center-align">
                            <input id="passwordMobile" 
                                   type="password" 
                                   class="validate nfl-white"
                                   ng-model="passwordLogin"
                                   ng-keyup="$event.keyCode == 13 && manualLogin()"
                                   placeholder="Password"
                                   autocomplete="current-password">
                        </div>
                    </form>
                    <div class="col s12 center-align">
                        <a class="waves-effect waves-light btn-small btn-flat white-text green modal-close"
                           style="width: 100%;"
                           ng-click="manualLogin()">
                            <i class="small-icon-class fas fa-sign-in-alt"></i>
                        </a>
                    </div>
                </div>
                <div class="col s12"
                     ng-show="loginTab == 2"
                     style="padding:10px;">
                    
                    
                    
                <div class="col s12 center-align">
                    <b>Todos</b> os campos são obrigatórios.<br>
                    <span ng-show="formSignup.passwordSignup.$invalid">Sua senha deve ter no mínimo <b>6 caracteres</b>.</span>
                    <span ng-show="!formSignup.passwordSignup.$invalid"><br></span>
                </div>
                <form name="formSignup"
                      class="col s12"
                      style="margin-top: 10px;">
                    <div class="input-field col s12 center-align">
                        <input id="emailSignupMobile"
                               name="emailSignupMobile"
                               type="email" 
                               class="validate modal-input animation"
                               placeholder="E-mail (usado no login)"
                               ng-model="emailSignup"
                               autocomplete="email"
                               ng-keyup="$event.keyCode == 13 && formSignup.$valid && signup()"
                               autofocus
                               required>
                    </div>
                    <div class="input-field col s12 center-align">
                        <input id="passwordSignupMobile"
                               name="passwordSignupMobile"
                               type="password" 
                               class="validate modal-input"
                               placeholder="Senha"
                               ng-model="passwordSignup"
                               autocomplete="new-password"
                               minlength="6"
                               ng-keyup="$event.keyCode == 13 && formSignup.$valid && signup()"
                               required>
                    </div>
                    <div class="input-field col s12 center-align">
                        <input id="userSignupMobile"
                               name="userSignupMobile"
                               type="text" 
                               class="validate modal-input"
                               placeholder="Usuário (exibição no ranking)"
                               ng-model="userSignup"
                               autocomplete="nickname"
                               ng-keyup="$event.keyCode == 13 && formSignup.$valid && signup()"
                               required>
                    </div>
                </form>
                <div class="col s12 center-align">
                    <a href="" 
                       class="waves-effect waves-light btn-flat red white-text modal-button"
                       ng-click="cleanForm()">
                        Limpar
                    </a>
                    <a href="" 
                       class="waves-effect waves-light btn-flat green white-text modal-button"
                       ng-click="formSignup.$valid && signup()">Cadastrar</a>
                </div>
                    
                    
                    
                </div>
            </div>
        </div>
    </span>
</div>