<div id="modalPreferencesMobile" class="modal bottom-sheet">
    <div class="modal-content noPadding">
        <div class="row">
            <div class="col s12 noPadding">
                <div class="col s6 center-align noPadding noMargin preferences-tab"
                    ng-class="preferencesTab == 1 ? 'active-tab' : ''"
                    ng-click="preferencesTab = 1">
                    <a class="btn-flat btn-large white-text"
                       style="width: 100%">
                        Minha Conta
                    </a>
                </div>
                <div class="col s6 center-align noPadding noMargin preferences-tab"
                    ng-class="preferencesTab == 2 ? 'active-tab' : ''"
                    ng-click="preferencesTab = 2">
                    <a class="btn-flat btn-large white-text"
                       style="width: 100%">
                        Meu Perfil
                    </a>
                </div>
            </div>
            <div class="col s12"
                 ng-show="preferencesTab == 1">
                <div class="col m10 offset-m1 s12 center-align">
                    <form name="updateUserAccountForm"
                          class="col s12"
                          style="margin-top: 10px;">
                        <div class="col s12 left-align">Nome Completo</div>
                        <div class="input-field col s11 center-align">
                            <input id="fullnamePreferencesMobile"
                                   class="modal-input"
                                   ng-value=loggedUser[0].full_name
                                   ng-model="newFullname"
                                   ng-disabled="!editFullName">
                        </div>
                        <div class="col s1"
                             style="line-height: 50px;"
                             ng-click="editFullName = !editFullName"
                             set-focus="fullnamePreferences">
                            <a href=""><i class="icon-class fas fa-pen"></i></a>
                        </div>
                        
                        <div class="col s12 left-align">Nome</div>
                        <div class="input-field col s11 center-align">
                            <input class="modal-input"
                                   ng-value=loggedUser[0].name
                                   ng-model="newName"
                                   ng-disabled="!editName">
                        </div>
                        <div class="col s1"
                             style="line-height: 50px;"
                             ng-click="editName = !editName">
                            <a href=""><i class="icon-class fas fa-pen"></i></a>
                        </div>
                        
                        <div class="col s12 left-align">E-mail</div>
                        <div class="input-field col s11 center-align">
                            <input class="modal-input"
                                   ng-value=loggedUser[0].login
                                   ng-model="newLogin"
                                   ng-disabled="!editEmail">
                        </div>
                        <div class="col s1"
                             style="line-height: 50px;"
                             ng-click="editEmail = !editEmail">
                            <a href=""><i class="icon-class fas fa-pen"></i></a>
                        </div>
                        
                        <span ng-show="editPassword">
                            <div class="col s12 left-align">Nova senha</div>
                            <div class="input-field col s11 center-align">
                                <input id="newPasswordMobile"
                                       name="newPasswordMobile"
                                       type="password"
                                       class="modal-input"
                                       ng-model="newPassword"
                                       minlength="6"
                                       ng-keyup="$event.keyCode == 13 && formSignup.$valid && signup()"
                                       placeholder="Digite sua nova senha">

                            </div>
                            <div class="col s1"
                                 style="line-height: 50px;"
                                 ng-click="editPassword = false">
                                <a href=""><i class="icon-class fas fa-times"></i></a>
                            </div>

                        </span>
                        <span class="col s12"
                              ng-show="!editPassword"
                              ng-click="editPassword = true">
                            <a href="">Deseja alterar sua senha?</a>
                        </span>

                        <div class="col s12 left-align">Senha atual</div>
                        <div class="input-field col s11 center-align">
                            <input id="passwordConfirmMobile"
                                   name="passwordConfirmMobile"
                                   type="password" 
                                   class="validate modal-input"
                                   ng-model="passwordConfirm"
                                   autocomplete="new-password"
                                   minlength="6"
                                   placeholder="Senha atual"
                                   ng-keyup="$event.keyCode == 13 && updateUserAccountForm.$valid && updateUserAccount()"
                                   required>
                        </div>
                        <p>Digite sua senha atual para confirmar as mudanças.</p>
                    </form>
                    <div class="col s12 center-align">
                        <div class="col s6">
                            <a href="" 
                               class="waves-effect waves-light btn-flat red white-text modal-close">
                                Cancelar
                            </a>
                        </div>
                        <div class="col s6">
                            <a href="" 
                               class="waves-effect waves-light btn-flat green white-text"
                               ng-disabled="!updateUserAccountForm.$valid"
                               ng-click="updateUserAccountForm.$valid && updateUserAccount()">Salvar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12"
                 ng-show="preferencesTab == 2">
                <div class="col m3 s12">
                    <p class="center-align">Escolha a cor do seu ícone.</p>
                    <div class="center-align noPadding">
                        <a class="center-align waves-effect waves-light btn-small extras-button noPadding"
                           style="background-color: {{iconColor}}"
                           href=""
                           ng-repeat="iconColor in iconColors"
                           ng-click="loggedUser[0].color = iconColor; updateUserProfile(icon, iconColor)">
                            &nbsp;
                        </a>
                    </div>
                </div>
                <div class="col m9 s12 center-align">
                    <p class="center-align">Escolha um ícone para representar suas apostas na seção "Tabela"</p>
                    <a class="waves-effect waves-light btn-small white extras-button"
                       style="color:{{loggedUser[0].color}}"
                       ng-click="$parent.chosenIcon = icon; updateUserProfile(icon, color)"
                       ng-repeat="icon in icons | startFrom:0+((iconsCurrentPage-1)*iconsLimit) | objLimitTo:iconsLimit+iconsStart">
                        <i class="{{icon}}"></i>
                    </a>
                    <div class="col s12">
                        <div class="col s5 left-align">
                            <a href="" 
                               class="waves-effect waves-light btn-flat blue darken-3 white-text"
                               ng-click="iconsCurrentPage = iconsCurrentPage - 1"
                               ng-disabled="iconsCurrentPage == 1">
                                <i class="fas fa-arrow-left"></i>
                            </a>
                        </div>
                        <div class="col s2 center-align"
                             style="line-height: 36px;">
                            {{iconsCurrentPage}}/{{iconsPages}}
                        </div>
                        <div class="col s5 right-align">
                            <a href="" 
                               class="waves-effect waves-light btn-flat blue darken-3 white-text"
                               ng-click="iconsCurrentPage = iconsCurrentPage + 1"
                               ng-disabled="iconsCurrentPage == iconsPages">
                                <i class="fas fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>