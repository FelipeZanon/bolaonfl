<div id="modalSignup" 
     class="modal bottom-sheet">
    <div class="modal-content">
        <div class="row">
            <div id="modalSignup-cadastro"
                 class="col m6 s12">
                <h5 class="center-align noPadding noMargin">Cadastro</h5>
                <div class="col s12 center-align">
                    <b>Todos</b> os campos são obrigatórios.<br>
                    <span ng-show="formSignup.passwordSignup.$invalid">Sua senha deve ter no mínimo <b>6 caracteres</b>.</span>
                    <span ng-show="!formSignup.passwordSignup.$invalid"><br></span>
                </div>
                <form name="formSignup"
                      class="col s12"
                      style="margin-top: 10px;">
                    <div class="input-field col m12 center-align">
                        <input id="emailSignup"
                               name="emailSignup"
                               type="email" 
                               class="validate modal-input animation"
                               placeholder="E-mail (usado no login)"
                               ng-model="emailSignup"
                               autocomplete="email"
                               ng-keyup="$event.keyCode == 13 && formSignup.$valid && signup()"
                               autofocus
                               required>
                    </div>
                    <div class="input-field col m12 center-align">
                        <input id="passwordSignup"
                               name="passwordSignup"
                               type="password" 
                               class="validate modal-input"
                               placeholder="Senha"
                               ng-model="passwordSignup"
                               autocomplete="new-password"
                               minlength="6"
                               ng-keyup="$event.keyCode == 13 && formSignup.$valid && signup()"
                               required>
                    </div>
                    <div class="input-field col m12 center-align">
                        <input id="userSignup"
                               name="userSignup"
                               type="text" 
                               class="validate modal-input"
                               placeholder="Usuário (exibição no ranking)"
                               ng-model="userSignup"
                               autocomplete="nickname"
                               ng-keyup="$event.keyCode == 13 && formSignup.$valid && signup()"
                               required>
                    </div>
                </form>
                <div class="col s12 center-align">
                    <a href="" 
                       class="waves-effect waves-light btn-flat red white-text modal-button"
                       ng-click="cleanForm()">
                        Limpar
                    </a>
                    <a href="" 
                       class="waves-effect waves-light btn-flat green white-text modal-button"
                       ng-click="formSignup.$valid && signup()">Cadastrar</a>
                </div>
            </div>
            <div id="modalSignup-info"
                 class="col m6 s12">
                <h5 class="center-align noPadding noMargin">Inscrição</h5><br>
                <div id="inscricao" class="col s12 center">
                    O valor da inscrição é de <b>R$50,00</b>.<br>
                    O valor deve ser pago <span style="font-variant: small-caps;"><b>antes</b></span> do início da temporada (06/09).<br>
                    Avisem quando tiverem depositado!<br><br>
                    
                    <div class="col s4 offset-s1">
                        <b>Banco do Brasil</b><br>
                        Agência: 0982-2<br>
                        C/C: 47930-6<br>
                        Felipe Zanon do Nascimento<br>
                        CPF: 050.143.229-99
                    </div>
                    
                    <div class="col s4">
                        <b>NuBank (Cód. 260)</b><br>
                        Agência: 0001<br>
                        C/C: 426087-4<br>
                        Felipe Zanon do Nascimento<br>
                        CPF: 050.143.229-99
                    </div>
                    
                    <div class="col s1">
                        <b>PicPay</b>: @felipezn
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>