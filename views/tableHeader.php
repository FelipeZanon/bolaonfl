<thead>
        <tr class="valign-wrapper match-head hide-on-small-only">
                <th class="col s2"><i class="material-icons">access_time</i></th>
                <th class="col s1">Fácil</th>
                <th class="col s1">Difícil</th>
                <th class="col s2">Away</th>
                <th class="col s2">Placar</th>
                <th class="col s2">Home</th>
                <th class="col s1">Difícil</th>
                <th class="col s1">Fácil</th>                    
        </tr>
        <tr class="valign-wrapper match-head hide-on-med-and-up">
            <th class="col s2 tooltip tooltip-mobile">
                F
                <span class="tooltiptext tooltipTopRight">Fácil</span>
            </th>
            <th class="col s2 tooltip tooltip-mobile">
                D
                <span class="tooltiptext tooltipTop">Difícil</span>
            </th>
            <th class="col s1">&nbsp;</th>
            <th class="col s2">&nbsp;</th>
            <th class="col s1">&nbsp;</th>
            <th class="col s2 tooltip tooltip-mobile">
                D
                <span class="tooltiptext tooltipTop">Difícil</span>
            </th>
            <th class="col s2 tooltip tooltip-mobile">
                F
                <span class="tooltiptext tooltipTopLeft">Fácil</span>
            </th>                    
        </tr>
</thead>
