<nav id="mobileHeader"
     class="nfl-dark-blue">
    <div class="nav-wrapper">
        <a class="sidenav-trigger"
           href="#" 
           data-target="mobile-sidebar" >
            <i class="material-icons">menu</i>
        </a>
        <ul class="right">
            <li>                
                <a class="waves-effect waves-light btn modal-trigger btn-small btn-flat"
                   ng-class="loggedUser.length > 0 ? 'white green-text' : 'nfl-dark-blue-text white'"
                   ng-href="{{loggedUser.length > 0 ? '#modalPreferencesMobile' : '#modalLoginMobile'}}">
                    <span ng-show="loggedUser.length == 0">Login</span>
                    <span ng-show="loggedUser.length > 0">{{loggedUser[0].name}}</span>
                    <i class="icon-class fas fa-user right"></i>
                </a>
            </li>
        </ul>
    </div>
</nav>
<?php require("views/sidebar/sidebarMobile.php"); ?>
<?php require("views/modals/loginMobile.php"); ?>
<?php require("views/modals/preferencesMobile.php"); ?>