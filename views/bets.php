<div class="page-header bets-header col s12 nfl-blue-text">
    Apostar <i class="icon-class far fa-dot-circle"></i>
</div>
<div class="col s12 noMargin noPadding">
    <ul id="tabsBets" 
        class="tabs">
        <?php require("weekSelector.php"); ?>
    </ul>
</div>
<div ng-show="loggedUser.length > 0">
    <span ng-show="!loadingBets">
        <table class="centered" ng-show="selectedRound == 0"><tr><td><span class="red-text"><b>ATENÇÃO</b></span>: os resultados da preseason <b>NÃO</b> contam para a pontuação oficial do bolão.</td></tr></table>
        <table id="match-table" class="striped highlight centered">
            <form action="#">
                <?php require("tableHeader.php"); ?>
                <tbody>
                <tr class="valign-wrapper match-body table-line"
                    ng-class="match.userBets == null ? 'no-bets' : ''"
                    ng-repeat="match in listMatches | filter:{week:selectedRound}:true | orderBy:['timestamp']">
                    <td class="col s2 hide-on-small-only"
                        style="font-size: 14px; line-height: 14px;">                
                        <span style="font-variant: small-caps"><b>{{match.timestamp * 1000| date:"EEE"}}, {{match.timestamp * 1000| date:"dd MMM"}}</b></span><br>
                        {{match.timestamp * 1000| date:"HH'h'mm"}}
                    </td>
                    <td class="col s2 m1 noPadding noMargin center-align table-column">
                        <p class="noPadding noMargin center-align">
                            <label>
                                <input id="{{match.id + 'AwayEasy'}}"
                                       name="group{{match.id}}"
                                       class="with-gap" 
                                       type="radio"
                                       ng-value="0"
                                       ng-model=match.userBets
                                       ng-disabled="nowTimestamp > match.timestamp"
                                       ng-click="saveBet(match.id, match.userBets)"/>
                                <span ></span>
                            </label>
                        </p>
                    </td>
                    <td class="col s2 m1 noPadding noMargin center-align table-column">
                        <p class="noPadding noMargin center-align">
                            <label>
                                <input id="{{match.id + 'AwayHard'}}"
                                       name="group{{match.id}}" 
                                       class="with-gap" 
                                       type="radio"
                                       ng-value="1"
                                       ng-model=match.userBets
                                       ng-disabled="nowTimestamp > match.timestamp"
                                       ng-click="saveBet(match.id, match.userBets)"/>
                                <span ></span>
                            </label>
                        </p>
                    </td>
                    <td class="col s1 m2 tooltip noMargin noPadding table-column">
                        <img ng-src="img/nfl_logos/{{match.team_away_alias}}.gif" class="img-valign nfl-logo">
                        <span class="tooltiptext tooltipTop">{{match.team_away}}</span>
                    </td>
                    <td class="col s2 noMargin noPadding table-column"
                        ng-value="betColor = colorCode(match)">
                        <span class="hide-on-small-only">
                            <span ng-class="betColor"
                                  ng-show="nowTimestamp > match.timestamp">
                                <b>{{match.away_points}} @ {{match.home_points}}</b>
                            </span>
                            <span ng-show="nowTimestamp <= match.timestamp">
                                @
                            </span>
                        </span>
                        <span class="hide-on-med-and-up">
                            <span ng-class="betColor"
                                  ng-show="nowTimestamp > match.timestamp"><b>{{match.away_points}} @ {{match.home_points}}</b></span>
                            <span ng-show="nowTimestamp <= match.timestamp"
                                  style="font-size: 10px; line-height: 12px;">
                                <span style="font-variant: small-caps"><b>{{match.timestamp * 1000| date:"dd MMM"}}</b></span><br>
                                {{match.timestamp * 1000| date:"HH'h'mm"}}
                            </span>
                        </span>
                    </td>
                    <td class="col s1 m2 tooltip noMargin noPadding table-column">
                        <img ng-src="img/nfl_logos/{{match.team_home_alias}}.gif" class="img-valign nfl-logo">
                        <span class="tooltiptext tooltipTop">{{match.team_home}}</span>
                    </td>
                    <td class="col s2 m1 noPadding noMargin center-align table-column">
                        <p class="noPadding noMargin center-align">
                            <label>
                                <input id="{{match.id + 'HomeHard'}}"
                                       name="group{{match.id}}" 
                                       class="with-gap" 
                                       type="radio"
                                       ng-value="2"
                                       ng-model=match.userBets
                                       ng-disabled="nowTimestamp > match.timestamp"
                                       ng-click="saveBet(match.id, match.userBets)"/>
                                <span></span>
                            </label>
                        </p>
                    </td>
                    <td class="col s2 m1 noPadding noMargin center-align table-column">
                        <p class="noPadding noMargin center-align">
                            <label>
                                <input id="{{match.id + 'HomeEasy'}}"
                                       name="group{{match.id}}" 
                                       class="with-gap" 
                                       type="radio"
                                       ng-value="3"
                                       ng-model=match.userBets
                                       ng-disabled="nowTimestamp > match.timestamp"
                                       ng-click="saveBet(match.id, match.userBets)"/>
                                <span></span>
                            </label>
                        </p>
                    </td>
                </tr>
            </tbody>
            </form>
            <div class="col s12" 
                 ng-show="loadinBets">
                <br><br>
                <p class="center-align"><img src="img/loading.gif" width=60px></p>
            </div>
        </table>
            <p class="col s12 center-align"
                 style="margin-top: 0; padding-top: 10px; box-shadow: inset 0px 1px 0px 0px darkgrey;">
                    <b>Bye Week</b>
            </p>
            <p class="col s12 center-align noPadding"
                 style="margin-top: 0">
                <span class="tooltip"
                            style="width: 55px;"
                            ng-repeat="team in byeWeeks | filter:{bye_week:selectedRound}:true">
                        <img ng-src="img/nfl_logos/{{team.alias}}.gif" 
                                 class="img-valign nfl-logo">
                        <span class="tooltiptext tooltipTop">{{team.name}}</span>
                </span>
            </p>

    </span>
    <div ng-show="loadingBets">
        <br><br>
        <p class="center-align"><img src="img/loading.gif" width=60px></p>
        <br><br>
    </div>
</div>
<div class="center-align col s12"
     ng-show="loggedUser.length == 0">
    <h5>
        Você precisa estar logado para apostar.
    </h5>
</div>