<div class="page-header table-header col s12 nfl-red-text">
    Tabela <i class="icon-class fas fa-table"></i>
</div>
<div class="col s12 noMargin noPadding">
    <ul id="tabsTables" 
        class="tabs">
        <?php require("weekSelector.php"); ?>
    </ul>
</div>
<div class="progress">
    <div class="determinate" ng-style="{'width': 100*(nowTimestamp - lastGetResults)/30 + '%'}"></div>
</div>  

<table class="centered" ng-show="selectedRound == 0"><tr><td><span class="red-text"><b>ATENÇÃO</b></span>: os resultados da preseason <b>NÃO</b> contam para a pontuação oficial do bolão.</td></tr></table>
<span ng-show="!loadingMatches">
    <table id="match-table" 
            class="striped highlight centered">
            <?php require("tableHeader.php"); ?>
            <tbody>
                    <tr class="valign-wrapper match-body table-line"
                            ng-repeat="match in listMatches | filter:{week:selectedRound}:true | orderBy:['timestamp']"
                            ng-value="betMargin = returnBetMargin(match.away_points, match.home_points)">
                            <td class="col s2 hide-on-small-only"
                                    style="font-size: 14px; line-height: 14px;">  
                                    <span ng-show="match.status == 'P'">
                                            <span style="font-variant: small-caps"><b>{{match.timestamp * 1000| date:"EEE"}}, {{match.timestamp * 1000| date:"dd MMM"}}</b></span><br>
                                            {{match.timestamp * 1000| date:"HH'h'mm"}}
                                    </span>
                                    <span style="font-variant: small-caps; text-transform: capitalize; font-weight: bold"
                                                ng-show="match.status != 'P'">
                                            {{match.status}}
                                    </span>
                            </td>
                            <td class="col s2 m1 noMargin noPadding table-column"
                                    ng-class="{'blue-grey lighten-4': nowTimestamp > match.timestamp && betMargin == 0}">
                                    <span class="tooltip bet-span"
                                                style="color: {{bet.color}};"
                                                ng-repeat="bet in match.bets | filter:{id_bet:0}:true">
                                            <i class="{{bet.icon}} table-icon"></i>
                                            <span class="tooltiptext tooltipTopRight">{{bet.name}}</span>
                                    </span>
                            </td>
                            <td class="col s2 m1 noMargin noPadding table-column"
                                    ng-class="{'blue-grey lighten-4': nowTimestamp > match.timestamp && (betMargin == 1 || betMargin == 4)}">
                                    <div class="tooltip bet-span"
                                                style="color: {{bet.color}}"
                                                ng-repeat="bet in match.bets | filter:{id_bet:1}:true">
                                            <i class="{{bet.icon}} table-icon"></i>
                                            <span class="tooltiptext tooltipTop">{{bet.name}}</span>
                                    </div>
                            </td>
                            <td class="col s1 m2 tooltip noMargin noPadding table-column">
                                    <img ng-src="img/nfl_logos/{{match.team_away_alias}}.gif" class="img-valign nfl-logo">
                                    <span class="tooltiptext tooltipTop">{{match.team_away}}</span>
                            </td>
                            <td class="col s2 noMargin noPadding" ng-value="betColor = colorCode(match)">
                                    <span class="hide-on-small-only">
                                            <span ng-class="betColor"
                                                        ng-show="nowTimestamp > match.timestamp">
                                                    <i class="fas fa-football-ball fa-xs brown-text" ng-class="match.status != 'final' && match.status != 'final/ot' && match.possession == 'home' ? '' : 'no-opacity'"></i>
                                                    <b>{{match.away_points}} @ {{match.home_points}}</b>
                                                    <i class="fas fa-football-ball fa-xs brown-text" ng-class="match.status != 'final' && match.status != 'final/ot' && match.possession == 'away' ? '' : 'no-opacity'"></i>
                                            </span>
                                            <span ng-show="nowTimestamp <= match.timestamp">
                                                    @
                                            </span>
                                    </span>
                                    <span class="hide-on-med-and-up">
                                            <span ng-class="betColor"
                                                        ng-show="nowTimestamp > match.timestamp"><b>{{match.away_points}} @ {{match.home_points}}</b></span>
                                            <span ng-show="nowTimestamp <= match.timestamp"
                                                        style="font-size: 10px; line-height: 12px;">
                                                    <span style="font-variant: small-caps"><b>{{match.timestamp * 1000| date:"dd MMM"}}</b></span><br>
                                                    {{match.timestamp * 1000| date:"HH'h'mm"}}
                                            </span>
                                    </span>
                            </td>
                            <td class="col s1 m2 tooltip noMargin noPadding table-column"
                                    ng-class="match.status != 'final' && match.status != 'final/ot' && match.status != 'P' && match.possession == 'home' ? 'mobile-possession' : ''">
                                    <img ng-src="img/nfl_logos/{{match.team_home_alias}}.gif" class="img-valign nfl-logo">
                                    <span class="tooltiptext tooltipTop">{{match.team_home}}</span>
                            </td>
                            <td class="col s2 m1 noMargin noPadding table-column"
                                    ng-class="{'blue-grey lighten-4': nowTimestamp > match.timestamp && (betMargin == 2 || betMargin == 4)}">
                                    <span class="tooltip bet-span"
                                                style="color: {{bet.color}}"
                                                ng-repeat="bet in match.bets | filter:{id_bet:2}:true">
                                            <i class="{{bet.icon}} table-icon"></i>
                                            <span class="tooltiptext tooltipTop">{{bet.name}}</span>
                                    </span>
                            </td>
                            <td class="col s2 m1 noMargin noPadding table-column"
                                    ng-class="{'blue-grey lighten-4': nowTimestamp > match.timestamp && betMargin == 3}">
                                    <span class="tooltip bet-span"
                                                style="color: {{bet.color}}"
                                                ng-repeat="bet in match.bets | filter:{id_bet:3}:true">
                                            <i class="{{bet.icon}} table-icon"></i>
                                            <span class="tooltiptext tooltipTopLeft">{{bet.name}}</span>
                                    </span>
                            </td>
                    </tr>
            </tbody>
    </table>
    <p class="col s12 center-align"
         style="margin-top: 0; padding-top: 10px; box-shadow: inset 0px 1px 0px 0px darkgrey;">
            <b>Bye Week</b>
    </p>
    <p class="col s12 center-align noPadding"
         style="margin-top: 0">
            <span class="tooltip"
                        style="width: 55px;"
                        ng-repeat="team in byeWeeks | filter:{bye_week:selectedRound}:true">
                    <img ng-src="img/nfl_logos/{{team.alias}}.gif" 
                             class="img-valign nfl-logo">
                    <span class="tooltiptext tooltipTop">{{team.name}}</span>
            </span>
    </p>
</span>
<div ng-show="loadingMatches">
    <br><br><br><br>
    <p class="center-align"><img src="img/loading.gif" width=60px></p>
    <br><br>
</div>
