app.service('SaveInfoService', function($rootScope, $q, SQLFactory, DataFactory, ToasterService) {
    this.bets = function (id_match, id_bet) {
        SQLFactory.saveBets("regular", id_match, id_bet).then(function(){
            ToasterService.printToast("success", 100);
        },function(error) {
            ToasterService.printToast("error", 101);
        });
    },
    this.extraBets = function (type, id_bet) {
        SQLFactory.saveBets("extra", type, id_bet).then(function(){
            ToasterService.printToast("success", 100);
            $rootScope.$broadcast("extraBetsSaved");
        }, function(error) {
            ToasterService.printToast("error", 101);
        });
    },
    this.onlineTimestamp = function () {
        SQLFactory.updateOnlineTimestamp();
    }
});
