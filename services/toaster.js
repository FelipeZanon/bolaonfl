app.service('ToasterService', function($rootScope, $q, DataFactory) {
    this.returnMessageByCode = function (code) {
        switch (code) {
            case 100:
                return "Aposta salva com sucesso!";
            case 101:
                return "Aposta NÃO foi salva. Por favor, atualize a página e tente novamente.";
            case 10: {
                var user = DataFactory.getInfo("loggedUser");
                return "Bem-vindo, " + user[0].name;
            }
            case 11: 
                return "Login e/ou senha não conferem.";
            case 20:
                return "Logout feito com sucesso.";
            case 900:
                return "Cadastro criado com sucesso!"
            case 901:
                return "Todos os campos são obrigatórios.";
            case 902:
                return "Sua senha deve conter pelo menos 6 caracteres.";
            case 903:
                return "E-mail já cadastrado. Escolha outro.";
            case 904:
                return "Nome já cadastrado. Escolha outro.";
            case 905:
                return "Senha não confere.";
            case 910:
                return "Cadastro alterado com sucesso!";
            case 911:
                return "Erro na alteração do cadastro. Tente novamente.";
        }
    },
    this.printToast = function (type, code) {
        
        var toastText = this.returnMessageByCode(code);
        if (!toastText)
            return;
        
        if (type == "error")
            M.toast({html:toastText, displayLength: '6000'});
        else if (type == "success")
            M.toast({html:toastText, displayLength: '2000'});
    }
});