app.service('GetInfoService', function($rootScope, $q, SQLFactory, DataFactory) {
    this.teams = function () {
        var deferred = $q.defer();
        SQLFactory.loadTeams().then(function(){
            $rootScope.$broadcast("teamsSet");
            deferred.resolve();
        });
        return deferred.promise;
    },
    this.matches = function (week) {
        var deferred = $q.defer();
        SQLFactory.loadMatches(week).then(function(){
            $rootScope.$broadcast("matchesSet");
            deferred.resolve();
        });
        return deferred.promise;
    },
    this.bets = function (type, week) {
        var deferred = $q.defer();
        SQLFactory.loadBets(type, week).then(function(){
            if (type == "regular")
                $rootScope.$broadcast("betsSet");
            else if (type == "extras")
                $rootScope.$broadcast("allExtraBetsSet");
            deferred.resolve();
        });
        return deferred.promise;
    },
    this.ranking = function (week) {
        var season = DataFactory.getInfo("season");
        var deferred = $q.defer();
        SQLFactory.loadRanking(week, season).then(function(){
            $rootScope.$broadcast("rankingSet");
            deferred.resolve();
        });
        return deferred.promise;
    },
    this.allSeasonsRecords = function () {
        var deferred = $q.defer();
        SQLFactory.loadAllSeasonsRecords().then(function(dataReturn){
            deferred.resolve(dataReturn);
        });
        return deferred.promise;
    },
    this.singleSeasonRecords = function (season) {
        var deferred = $q.defer();
        SQLFactory.loadSingleSeasonsRecords(season).then(function(dataReturn){
            deferred.resolve(dataReturn);
        });
        return deferred.promise;
    },

    this.recordsRanking = function (week, season) {
        var deferred = $q.defer();
        SQLFactory.loadRanking(week, season).then(function(dataReturn){
            deferred.resolve(dataReturn);
        });
        return deferred.promise;
    },
    this.userBets = function () {
        var deferred = $q.defer();
        SQLFactory.loadUserBets("regular").then(function(){
            $rootScope.$broadcast("userBetsSet");
            deferred.resolve();
        });
        return deferred.promise;
    },
    this.userExtraBets = function () {
        var deferred = $q.defer();
        SQLFactory.loadUserBets("extra").then(function(){
            $rootScope.$broadcast("userExtraBetsSet");
            deferred.resolve();
        });
        return deferred.promise;
    },
    this.user = function () {
        var deferred = $q.defer();
        SQLFactory.loadUser().then(function(){
            deferred.resolve();
        });
        return deferred.promise;
    },
		this.byeWeeks = function () {
        var season = DataFactory.getInfo("season");

				var deferred = $q.defer();
				SQLFactory.loadByeWeeks(season).then(function() {
						deferred.resolve();
				});
				return deferred.promise;
		}

});