app.service('LoginService', function($rootScope, $q, UtilitiesService, ToasterService, SQLFactory, DataFactory, LoginFactory) {
    this.manualLogin = function (login, password) {
        const encryptedPass = UtilitiesService.encryptSHA(password);
    
        LoginFactory.manualLogin(login, encryptedPass).then(function() {
            const loggedUser = DataFactory.getInfo("loggedUser");
            if (loggedUser.length > 0) {
                $rootScope.$broadcast("userLoggedIn");
            }
        });
    },
    this.cookiesLogin = function () {
        let loggedUser = DataFactory.getInfo("loggedUser");
        if (loggedUser.length == 0) {
            LoginFactory.cookiesLogin().then(function(){
                loggedUser = DataFactory.getInfo("loggedUser");
                if (loggedUser.length > 0) {
                    $rootScope.$broadcast("userLoggedIn");
                    UtilitiesService.renewCookies();
                }
            });
        }
    },
    this.logout = function () {    
        const bolaoCookies = UtilitiesService.returnBrowserCookies();

        if (bolaoCookies) {
            allCookies = bolaoCookies.split('-');
            let singleCookie = allCookies[0].trim();
            singleCookie = singleCookie.substring(13);
            SQLFactory.deleteCookies(singleCookie).then(function(){
                SQLFactory.destroySession();
            });            
        }
        
        DataFactory.setLoggedUser("");
        $rootScope.$broadcast("userLoggedOut");
        ToasterService.printToast('success', 20);
    },
    this.signup = function (email, password, user) {
        if (!email) {
            ToasterService.printToast('error', 901);
        } else if (!password) {
            ToasterService.printToast('error', 901);
        } else if (!user) {
            ToasterService.printToast('error', 901);
        } else if (password.length < 6) {
            ToasterService.printToast('error', 902);
        } else {
            LoginFactory.signup(email, UtilitiesService.encryptSHA(password), user).then(function(){
                ToasterService.printToast('success', 900);
                $rootScope.$broadcast("userSignup");
            }, function (error) {
                ToasterService.printToast('error', error);
            });
        }

    },
    this.updateAccount = function (fullname, name, login, newpass, pass) {
        const loggedUser = DataFactory.getInfo("loggedUser");

        if ((!fullname || fullname == loggedUser[0].full_name) && 
            (!name || name == loggedUser[0].name) && 
            (!login || login == loggedUser[0].login) && 
            !newpass)
            return;

        
        if (newpass)
            newpass = UtilitiesService.encryptSHA(newpass);
        
        LoginFactory.updateUser(fullname, name, login, newpass, UtilitiesService.encryptSHA(pass)).then(function() {
            $rootScope.$broadcast("userUpdate");
            ToasterService.printToast('success', 910);
        }, function(error) {
            ToasterService.printToast('error', error);
        });
    },
    this.updateProfile = function (icon, color) {
        LoginFactory.updateUserProfile(icon, color).then(function() {
            $rootScope.$broadcast("userUpdate");
            ToasterService.printToast('success', 910);
        }, function(error) {
            ToasterService.printToast('error', error);
        });
    }
});