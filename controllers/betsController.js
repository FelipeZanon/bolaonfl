app.controller('betsController', function($rootScope, $scope, $q, $http, $location, $window, $timeout, SQLFactory, DataFactory, GetInfoService, SaveInfoService) {
    var loadingBets = true;
    $scope.testval = 2;
    $scope.extrasType = "AFC";
    
	$(document).ready(function(){
        GetInfoService.ranking($scope.selectedRound);
        if ($scope.loggedUser.length > 0) {
            $scope.loadingBets = 1;
            GetInfoService.userBets();
            GetInfoService.userExtraBets();
            GetInfoService.bets("extras");
        }            
	});
    
    var userBetsListener = $scope.$on("userBetsSet", function () {
        $scope.listUserBets = DataFactory.getInfo("userBets");
        mergeUserBetsIntoMatches();
    });
    
    var userLoggedInListener = $scope.$on("userLoggedIn", function () {
        $scope.loadingBets = 1;
        GetInfoService.userBets();
        GetInfoService.userExtraBets();
        GetInfoService.bets("extras");
    });

    var extraBetsSavedListener = $scope.$on("extraBetsSaved", function () {
        GetInfoService.userExtraBets();
    });
    
    var allExtraBetsSetListener = $scope.$on("allExtraBetsSet", function () {
        $scope.listAllExtraBets = DataFactory.getInfo("allExtraBets");
        for (var i = 0; i < $scope.listTeams.length; i++) {
            $scope.listTeams[i].show_wc = false;
            $scope.listTeams[i].show_conf = false;
            $scope.listTeams[i].show_sb = false;
            for (var j = 0; j < $scope.listAllExtraBets.length; j++) {
                if ($scope.listTeams[i].id == $scope.listAllExtraBets[j].id_team) {
                    if ($scope.listAllExtraBets[j].id_type == 12 ||
                        $scope.listAllExtraBets[j].id_type == 13) {
                        $scope.listTeams[i].show_wc = true;
                    } else if ($scope.listAllExtraBets[j].id_type == 2 ||
                               $scope.listAllExtraBets[j].id_type == 7) {
                        $scope.listTeams[i].show_conf = true;
                    } else if ($scope.listAllExtraBets[j].id_type == 1) {
                        $scope.listTeams[i].show_sb = true;
                    }
                }
            }            
        }
    });

    
    
    $scope.saveBet = function (id_match, id_bet) {
        SaveInfoService.bets(id_match, id_bet);
    }
    
    $scope.saveExtraBet = function (id_team, conference, division) {        
        conference = conference.toLowerCase();
        if (division != undefined) {
            division = "_" + division.toLowerCase();
        } else {
            division = '';
        }

        SaveInfoService.extraBets(conference+division, id_team);
    }
    
    function mergeUserBetsIntoMatches () {
        for (var i = 0; i < $scope.listMatches.length; i++) {
            for (var j = 0; j < $scope.listUserBets.length; j++) {
                if ($scope.listMatches[i].id == $scope.listUserBets[j].id_match) {
                    $scope.listMatches[i].userBets = $scope.listUserBets[j].id_bet;
                }
            }
        }
        $scope.loadingBets = 0;
    }
    
    $scope.$on('$destroy', function () {
        userBetsListener();
        userLoggedInListener();
        extraBetsSavedListener();
        allExtraBetsSetListener();
    });


});
