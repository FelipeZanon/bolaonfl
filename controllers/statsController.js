app.controller('statsController', function($rootScope, $scope, $q, $http, $location, $window, SQLFactory, DataFactory) {
	$scope.loadingMatches = true;
	$scope.selectedStats = 1;
	
	$(document).ready(function(){
		SQLFactory.getScorers().then(function(){
			$scope.listScorers = DataFactory.getInfo("scorers");
		});
		SQLFactory.getOwnGoals().then(function(){
			$scope.listOwnGoals = DataFactory.getInfo("ownGoals");
		});
	});
	
	$scope.$watch('listMatches.length', function(length) {
		if (length > 0) {
			$scope.totalGoals = 0;
			$scope.finishedMatches = 0;
			for (var i = 0; i < $scope.listMatches.length; i++) {
				for (var j = 0; j < $scope.listTeams.length; j++) {
					if (i == 0) {
						$scope.listTeams[j].goals_scored = 0;
						$scope.listTeams[j].goals_conceded = 0;
					}
					
					if ($scope.listMatches[i].round <= 3) {					
						if ($scope.listMatches[i].team_home_id == $scope.listTeams[j].id) {
							$scope.listTeams[j].goals_scored = $scope.listTeams[j].goals_scored + $scope.listMatches[i].goals_home;
							$scope.listTeams[j].goals_conceded = $scope.listTeams[j].goals_conceded + $scope.listMatches[i].goals_visitor;
						} else if ($scope.listMatches[i].team_visitor_id == $scope.listTeams[j].id) {
							$scope.listTeams[j].goals_scored = $scope.listTeams[j].goals_scored + $scope.listMatches[i].goals_visitor;
							$scope.listTeams[j].goals_conceded = $scope.listTeams[j].goals_conceded + $scope.listMatches[i].goals_home;
						}
					}
				}
				$scope.totalGoals = $scope.totalGoals + $scope.listMatches[i].goals_home + $scope.listMatches[i].goals_visitor;
				if ($scope.listMatches[i].status != 'future') {
					$scope.finishedMatches++;
				}
			}
			if ($scope.finishedMatches == 0) {
				$scope.averageGoals = 0;
			} else {
				$scope.averageGoals = $scope.totalGoals / $scope.finishedMatches;
			}
			$scope.loadingMatches = false;
		}
	});

});