app.controller('recordsController', function($rootScope, $scope, $q, $http, $location, $window, $timeout, 
                                            SQLFactory, DataFactory, GetInfoService) {
//    GetInfoService.recordsRanking(semana escolhida, id da temporada escolhida, flag de extra bets, flag de pontos acumulados);
    
    $scope.loadingRecords = true;
    $scope.bestIndividualResults = [];
    $scope.bestWeekResults = [];    
    $scope.worstIndividualResults = [];
    $scope.worstWeekResults = [];
    $scope.extraBetsFlag = 'false';
    $scope.selectedRecords = 1;
    $scope.selectedTypeRecords = 1;
    $scope.selectedSeason = 1;
    $scope.numberOfResults = 10;
    $scope.searchOrder = false;
    $scope.defaultRankingOrder = ['points_with_extras', 'tens', 'fives'];
    $scope.rankingOrder = $scope.defaultRankingOrder;
  
    $scope.finalRankings = [];
    $scope.weeklyRankings = [];
    $scope.seasonsRankings = [];
    $scope.checkpointRankings = [];
    
    var score, week, matches, season;

	$(document).ready(function(){
        GetInfoService.allSeasonsRecords().then(function (data){
            for(let i = 0; i < data.length; i++) {
                for (let j = 0; j < data[i].length; j++) {
                    if (j === 0) {
                        season = data[i][j].season;
                        season_mini = data[i][j].season_mini;
                        season_id = data[i][j].season_id;
                        matches = data[i][j].matches;
                    } else {
                        score = {
                            season: season,
                            season_mini: season_mini,
                            season_id: season_id,
                            matches: matches,
                            icon: data[i][j].icon,
                            color: data[i][j].color,
                            name: data[i][j].name,
                            points: data[i][j].points,                            
                            points_with_extras: data[i][j].points_with_extras,
                            percentage: data[i][j].percentage,
                            accumulated_percentage: data[i][j].accumulated_percentage,
                            accumulated_points: data[i][j].accumulated_points,
                            accumulated_tens: data[i][j].accumulated_tens,
                            accumulated_fives: data[i][j].accumulated_fives,
                            tens: data[i][j].tens,
                            fives: data[i][j].fives,                            
                            extra_points: data[i][j].extra_points,
                            position: data[i][j].position
                        }

                        $scope.seasonsRankings.push(score);
                    }
                }
            }
        });

        var promises = [];
        for (let i = 0; i < $scope.seasons.length; i++) {
            promises.push(GetInfoService.singleSeasonRecords($scope.seasons[i].id));
        }

        $q.all(promises).then(function(result){
            // For the length of seasons
            for (let i = 0; i < result.length; i++) {
                // For the length of weeks
                for (let j = 0; j < result[i].length; j++) {
                    // For the length of users
                    for (let k = 0; k < result[i][j].length; k++) {
                        if (k == 0) {
                            season = result[i][j][k].season;
                            season_mini = result[i][j][k].season_mini;
                            season_id = result[i][j][k].season_id;
                            matches = result[i][j][k].matches;
                            week = result[i][j][k].week;                                                                        
                        } else {
                            score = {
                                season: season,
                                season_mini: season_mini,
                                season_id: season_id,
                                week: week,
                                week_string: week.toString(),
                                matches: matches,
                                icon: result[i][j][k].icon,
                                color: result[i][j][k].color,
                                name: result[i][j][k].name,
                                points: result[i][j][k].points,                            
                                points_with_extras: result[i][j][k].points_with_extras,
                                percentage: result[i][j][k].percentage,
                                accumulated_percentage: result[i][j][k].accumulated_percentage,
                                accumulated_points: result[i][j][k].accumulated_points,
                                accumulated_tens: result[i][j][k].accumulated_tens,
                                accumulated_fives: result[i][j][k].accumulated_fives,
                                tens: result[i][j][k].tens,
                                fives: result[i][j][k].fives,                            
                                extra_points: result[i][j][k].extra_points,
                                position: result[i][j][k].position,
                                bets_number: result[i][j][k].bets_number
                            }
                            
                            if (result[i][j][k].bets_number >= matches - 2) {
                                if (week <= 17)
                                    $scope.weeklyRankings.push(score);
                                $scope.checkpointRankings.push(score);
                            }
                        }
                    }
                }
            }
            $scope.loadingRecords = false;
        }, function(error) {
            $scope.loadingRecords = false;
        });
	});
    
    $scope.myComparator = function(actual, expected){
        return !expected || actual === expected;
    };
    
    $scope.orderByPoints = function (result) {
        if ($scope.searchPE) {
            return result.points_with_extras;
        } else {
            return result.points;
        }
    };

});