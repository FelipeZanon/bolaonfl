app.controller('tableController', function($rootScope, $scope, $routeParams, $q, $http, $location, $window, $filter, $interval, DataFactory, SQLFactory, GetInfoService) {
	$scope.gamedate = "";
	$scope.collapsibleIndex = "-1";
	$scope.expandedGame = false;
	$scope.expandedBets = false;
	$scope.expandedMatchId = '';
	$scope.weather = [];
	$scope.matchEvents = [];
	$scope.yourBet = [];
	$scope.liveMatch = [];
	
	$(document).ready(function(){
	});
    
    $scope.returnBetMargin = function (away_points, home_points) {
        if (away_points - home_points > 7)
            return 0; //Away easy
        else if (away_points - home_points > 0 && away_points - home_points <= 7)
            return 1; //Away hard
        else if (home_points - away_points > 0 && home_points - away_points <= 7)
            return 2; //Home hard 
        else if (home_points - away_points > 7)
            return 3; //Home easy
        else if (home_points == away_points)
            return 4; //Tie
    }
	
	//30s
//	$interval(updateEvents, 30000, 0, true);


});
