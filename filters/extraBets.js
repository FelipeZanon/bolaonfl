app.filter('extraBetsFilter', [function(){
    return function(obj, filter){
        if (obj && obj.length > 0) {
            var ret = [];
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].description == filter.toLowerCase())
                    ret.push(obj[i]);
            }
            return ret;
        }
    };
}]);