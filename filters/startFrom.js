app.filter('startFrom', function() {
    return function(input, start) {
        if (input && input.length > 0) {
            start = +start; //parse to int
            return input.slice(start);    
        }        
    }
});
