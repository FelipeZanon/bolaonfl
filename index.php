<?php
	@session_start();
	$_SESSION['openAPI'] = true;
	$_SESSION['saveBets'] = false;
	$_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
?>

<!doctype html>
<html ng-app="bolaoApp" style="font-family: 'Roboto', sans-serif;" class="background-color">
	<header>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119267009-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
		  
			gtag('config', 'UA-119267009-1');
		</script>

		
		<title>Bolão NFL 2020/21</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<meta property="og:url" content="https://www.felipe.zanon.tk/bolaonfl/"/>
		<meta property="og:title" content="Bolão NFL 2020/21"/>
		<meta property="og:image" content="https://www.felipe.zanon.tk/bolaonfl/img/nfl_misc/NFL.gif"/>
		<meta property="og:description" content="Bolão da temporada 2020/21 da NFL" />
		<meta property="og:type" content="website"/>

		<link rel="shortcut icon" href="img/favicon.png?012"/>

		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

		<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script async src="https://code.jquery.com/ui/jquery-ui-git.js"></script>		
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

		<script src="js/moment.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
		<script src="js/angular-locale.js"></script>
		<script src="js/angular-route.min.js"></script>
        
		<script src="app/main.js?v=1.0"></script>
		<script src="controllers/betsController.js?v=1.0"></script>
		<script src="controllers/rulesController.js?v=1.0"></script>
		<script src="controllers/statsController.js?v=1.0"></script>
		<script src="controllers/tableController.js?v=1.0"></script>
        <script src="controllers/recordsController.js"></script>
        <script src="services/constants.js?v=1.0"></script>
        <script src="services/utilities.js?v=1.0"></script>
        <script src="services/saveInfo.js?v=1.0"></script>
        <script src="services/getInfo.js?v=1.0"></script>
        <script src="services/login.js?v=1.0"></script>
        <script src="services/toaster.js?v=1.0"></script>
		<script src="factories/dataFactory.js?v=1.0"></script>
		<script src="factories/loginFactory.js?v=1.0"></script>
		<script src="factories/sqlFactory.js?v=1.0"></script>
        <script src="directives/validation.js?v=1.0"></script>
        <script src="directives/inputFocus.js?v=1.0"></script>
        <script src="filters/startFrom.js?v=1.0"></script>
        <script src="filters/obectLimitTo.js?v=1.0"></script>
        <script src="filters/extraBets.js?v=1.0"></script>
		<link type="text/css" rel="stylesheet" href="css/weather-icons.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

		<link type="text/css" rel="stylesheet" href="css/main.css?v=1.1">
	</header>
	<main>
		<div ng-controller="mainController">

            <span class="hide-on-small-only"><?php require("views/sidebar/sidebar.php"); ?></span>
            <span class="hide-on-med-and-up"><?php require("views/headerMobile.php"); ?></span>

			<div class="row">				
				<div id="allContent" class="col s12 animation"
                     ng-class="openedSidebar ? 'l10 offset-l2' : 'l11 offset-l1'">

					<div class="row" style="position:relative;">
						<div id="mainContent" class="shadowed col l7 s12 noPadding">
                            
							<div ng-view></div>
						</div>
						<div id="rightContent" class="col l5 hide-on-med-and-down">
							<?php require("views/ranking.php"); ?>
						</div>
					</div>
				</div>
			</div>
            <div class="nfl-old" ng-show="false"></div>
		</div>
	</main>
</html>
