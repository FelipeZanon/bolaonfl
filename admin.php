<!doctype html>
<html ng-app="bolaoAdminApp" style="overflow-y: hidden;">
	<head>
		<title>Bola&#771;o Copa 2018</title>
		<!--<link rel="shortcut icon" href="favicon.ico"/>-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="shortcut icon" href="img/favicon.png"/>

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script async src="http://code.jquery.com/ui/jquery-ui-git.js"></script>		
		<script src="materialize/js/materialize.js"></script>		
		<script src="js/moment.js"></script>		
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
		<script src="app/mainAdmin.js"></script>
		<script src="factories/dataFactory.js"></script>
		<script src="factories/loginFactory.js"></script>
		<script src="factories/sqlFactory.js"></script>
		
		<link type="text/css" rel="stylesheet" href="materialize/css/materialize.css"  media="screen,projection"/>
		<link type="text/css" rel="stylesheet" href="css/admin.css">
	</head>
	<body>
		<div ng-controller="adminController" style="overflow-y: hidden;">
			<div class="row">
				<div id="loginPage" class="loginPage col l4 offset-l4 s12" ng-hide="loggedUser.length > 0">
					<h5>Login</h5>
					<div class="row">
						<form class="col l8 offset-l2 s10 offset-s1">
							<div class="row">
								<div class="input-field col s12">
									<input placeholder="Login" id="loginCopa2018" type="text" class="centerText" ng-model="userLogin">
									<!--<label for="first_name">Login</label>-->
								</div>
								<div class="input-field col s12">
									<input placeholder="Password" id="passwordCopa2018" type="password" class="centerText" ng-model="userPass">
								</div>
								<div class="center">
									<p class="centerText">
										<input type="checkbox" class="filled-in" id="filled-in-box" checked="checked" ng-model="userRemember" />
										<label for="filled-in-box" class="black-text">Lembre-se de mim</label>
									</p>

									<button class="btn waves-effect waves-light black beigeText" type="submit" name="action" ng-click="login()">OK
										<i class="material-icons right">send</i>
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="mainPage" class="col l10 offset-l1 s12" ng-if="loggedUser.length > 0">
				<nav>
					<div class="nav-wrapper">
					  <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
					  <ul class="left hide-on-med-and-down">
						<li><a href="sass.html">Sass</a></li>
						<li><a href="badges.html">Components</a></li>
						<li><a href="collapsible.html">Javascript</a></li>
						<li><a href="mobile.html">Mobile</a></li>
					  </ul>
					  <ul class="side-nav" id="mobile-demo">
						<li><a href="#" ng-click="updateInternalPage('teams')"><i class="material-icons">add_circle_outline flag</i>&nbsp;Inserir Seleções</a></li>
						<li><a href="#" ng-click="updateInternalPage('players')"><i class="material-icons">add_circle_outline person</i>&nbsp;Inserir Jogadores</a></li>
					  </ul>
					</div>
				  </nav>
					<div id="content">
						<div id="adminMenu" class="col l3 adminMenu hide-on-med-and-down">
							<div class="collection">
							  <a href="#!" class="collection-item" ng-class="{'active': selectedAdminMenu == 1}" ng-click="selectedAdminMenu = 1; updateSelectsTimeout()">
								<i class="material-icons">add_circle_outline</i>&nbsp;Inserir
							  </a>
							  <div ng-show="selectedAdminMenu == 1">
									<a href="#" ng-click="updateInternalPage('teams')">
										<div class="adminMenuOptions" ng-class="{'activeInternalMenu': internalAdminMenu == 'teams'}"><i class="material-icons">flag</i>&nbsp;Seleções</div>
									</a>
									<a href="#" ng-click="updateInternalPage('players')">
										<div class="adminMenuOptions" ng-class="{'activeInternalMenu': internalAdminMenu == 'players'}"><i class="material-icons">person</i>&nbsp;Jogadores</div>
									</a>
									<a href="#" ng-click="updateInternalPage('stadiums')">
										<div class="adminMenuOptions" ng-class="{'activeInternalMenu': internalAdminMenu == 'stadiums'}"><i class="material-icons">location_on</i>&nbsp;Estádios</div>
									</a>
									<a href="#" ng-click="updateInternalPage('matches')">
										<div class="adminMenuOptions" ng-class="{'activeInternalMenu': internalAdminMenu == 'matches'}"><i class="material-icons">adjust</i>&nbsp;Partidas</div>
									</a>

							  </div>
						  
							  <a href="#!" class="collection-item" ng-class="{'active': selectedAdminMenu == 2}" ng-click="selectedAdminMenu = 2; updateSelectsTimeout()">
								<i class="material-icons">edit</i>&nbsp;Editar
							  </a>
							  <div ng-show="selectedAdminMenu == 2">
								  <div class="adminMenuOptions"><i class="material-icons">flag</i>&nbsp;Seleções</div>
								  <div class="adminMenuOptions"><i class="material-icons">person</i>&nbsp;Jogadores</div>
							  </div>
							  
							  <a href="#!" class="collection-item" ng-class="{'active': selectedAdminMenu == 3}" ng-click="selectedAdminMenu = 3; updateSelectsTimeout()">
								<i class="material-icons">remove_circle_outline</i>&nbsp;Remover
							  </a>
							  <div ng-show="selectedAdminMenu == 3">
									<a href="#" ng-click="updateInternalPage('teams')">
										<div class="adminMenuOptions" ng-class="{'activeInternalMenu': internalAdminMenu == 'teams'}"><i class="material-icons">flag</i>&nbsp;Seleções</div>
									</a>
									<a href="#" ng-click="updateInternalPage('players')">
										<div class="adminMenuOptions" ng-class="{'activeInternalMenu': internalAdminMenu == 'players'}"><i class="material-icons">person</i>&nbsp;Jogadores</div>
									</a>
									<a href="#" ng-click="updateInternalPage('stadiums')">
										<div class="adminMenuOptions" ng-class="{'activeInternalMenu': internalAdminMenu == 'stadiums'}"><i class="material-icons">location_on</i>&nbsp;Estádios</div>
									</a>
									<a href="#" ng-click="updateInternalPage('matches')">
										<div class="adminMenuOptions" ng-class="{'activeInternalMenu': internalAdminMenu == 'matches'}"><i class="material-icons">adjust</i>&nbsp;Partidas</div>
									</a>
							  </div>
							</div>
						</div>
						<div id="adminMainContent" class="adminMainContent col l9 s12">
							<div id="internalContent" ng-if="selectedAdminMenu == 1">
								<?php require("views/insert.php"); ?>
							</div>
							<div id="internalContent" ng-if="selectedAdminMenu == 3">
								<?php require("views/remove.php"); ?>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>